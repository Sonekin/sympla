<?php

use App\Http\Controllers\_TestController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;

use App\Http\Controllers\EventRegistrationController;
use App\Http\Controllers\TicketOfficeController;

use App\Http\Controllers\WebHook\WebHookContaPayController;
use App\Http\Controllers\WebHook\WebHookPagarMeController;
use App\Http\Controllers\Organizer\MyEventsController;
use App\Http\Controllers\Organizer\EventTicketsController;
use App\Http\Controllers\Organizer\EventPanelController;
use App\Http\Controllers\Organizer\TicketPanelController;
use App\Http\Controllers\Organizer\ParticipantPanelController;
use App\Http\Controllers\Organizer\CheckInController;

use App\Http\Controllers\App\UserTicketsController;
use App\Http\Controllers\ConsumerArea\MyProfileController;

use App\Http\Controllers\WebHook\WebHookWhatsappController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return Inertia::render('Home', [
        'event' => 'a-festa-do-ano',
        'status' => Auth::check()
    ]);
});

Route::get('/', [AuthenticatedSessionController::class, 'create']);

Route::prefix('webhook')->name('webhook.')->group(function () {
    Route::post('/conta-pay', WebHookContaPayController::class)->name('contapay');
    Route::post('/pagar-me', WebHookPagarMeController::class)->name('pagarme');

});

Route::prefix( 'whatsapp' )->name('whatsapp.')->group( function() {
    Route::get('test', [ WebHookWhatsappController::class, 'test' ])->name('test');
    Route::get('log', [ WebHookWhatsappController::class, 'log' ])->name('log');
    Route::get('clearlog', [ WebHookWhatsappController::class, 'clearLog' ])->name('clearLog');
});

Route::prefix('evento')->name('ticketOffice.')->group(function () {
    Route::get('/{slug}', TicketOfficeController::class)->name('show');
    Route::prefix('/{slug}')->name('registration.')->group(function () {
        Route::get('/inscricao', [EventRegistrationController::class, 'create'])->name('create');
        Route::get('/checkout', [EventRegistrationController::class, 'checkout'])->name('checkout');
        Route::post('/inscricao', [EventRegistrationController::class, 'store'])->name('store');
        Route::post('/inscricao-pix', [EventRegistrationController::class, 'storePix'])->name('store.pix');
    });
});

Route::middleware('auth:sanctum')->group(function () {

    Route::prefix('/checkin')->name('checkin.')->group(function () {
        Route::post('/{eventId}', [CheckInController::class, 'check'])->name('check');    
    });
    /**
     * ********************************************************
     * * in the future these routes can and should be removed *
     * ********************************************************
     */
    if (config('app.env') !== 'production') {
        Route::get('/endpoints', function () {
            return Inertia::render('Endpoints', []);
        });

        Route::prefix('test')->name('test.')->group(function () {
            Route::get('/pay/document', [_TestController::class, 'paydocument'])->name('pay.document');
        });

        // Generate a user token to be used as Authorization header (on Postman)
        Route::get('/tokens/create', function (Illuminate\Http\Request $request) {
            $token = $request->user()->createToken($request->token_name, ['server:update']);
            return ['token' => $token->plainTextToken];
        });
    }
    /**
     * ********************************************************
     * *                  END TEST ROUTES                     *
     * ********************************************************
     */

    Route::get('/fale-conosco', function () {
        return Inertia::render('FaleConosco', []);
    });


    Route::get('/carne', function () {
        return Inertia::render('Orders/CarnePayment', []);
    });

    Route::name('myprofile.')->group(function () {
        Route::get('/meus-dados', [MyProfileController::class, 'index'])->name('index');
        Route::put('/meus-dados', [MyProfileController::class, 'update'])->name('update');

        Route::get('/meus-pedidos', [MyProfileController::class, 'orders'])->name('orders');
    });

    Route::get('/order-info', function () {
        return Inertia::render('Orders/OrderInfo', []);
    });

    Route::get('/cancelamentos', function () {
        return Inertia::render('CancelRules', []);
    });

    Route::get('/politica-privacidade', function () {
        return Inertia::render('PoliticaPrivacidade', []);
    });

    Route::get('/seu-lugar', function () {
        return Inertia::render('YourPlace', []);
    });

    Route::prefix('organizador')->name('organizer.')->group(function () {
        Route::get('/{eventId}/ingressos/filtro', [TicketPanelController::class, 'filter'])->name('filter');
        Route::get('/{eventId}/participantes/filtro', [ParticipantPanelController::class, 'filter'])->name('filter');
        Route::post('/{companyId}/enderecos-eventos', [MyEventsController::class, 'storeEventAddress'])->name('store.eventaddress');

        Route::prefix('meus-eventos')->name('myevents.')->group(function () {
            Route::get('/', [MyEventsController::class, 'index'])->name('index');
            Route::get('/criar-evento', [MyEventsController::class, 'create'])->name('create');
            Route::get('/{eventId}/editar-evento', [MyEventsController::class, 'edit'])->name('edit');

            Route::post('/', [MyEventsController::class, 'store'])->name('store');
            Route::put('/{eventId}', [MyEventsController::class, 'update'])->name('update');
            Route::delete('/{eventId}', [MyEventsController::class, 'destroy'])->name('destroy');

            Route::get('/filtro', [MyEventsController::class, 'filter'])->name('filter');
            Route::get('/{eventId}/report', [CheckInController::class, 'report'])->name('report');
            Route::post('/{eventId}/checar', [CheckInController::class, 'check'])->name('check');

        });

        Route::prefix('ingressos')->name('tickets.')->group(function () {
            Route::post('/{eventId}', [EventTicketsController::class, 'store'])->name('store');
            Route::put('/{ticketId}', [EventTicketsController::class, 'update'])->name('update');
            Route::delete('/{ticketId}', [EventTicketsController::class, 'destroy'])->name('destroy');
        });

        Route::prefix('painel-do-evento')->name('eventpanel.')->group(function () {
            Route::get('/{eventId}', [EventPanelController::class, 'index'])->name('index');
        });

        Route::prefix('ingressos-dashboard')->name('ticketpanel.')->group(function () {
            Route::get('/{eventId}', [TicketPanelController::class, 'index'])->name('index');
        });

        Route::prefix('participantes-dashboard')->name('participantpanel.')->group(function () {
            Route::get('/{eventId}', [ParticipantPanelController::class, 'index'])->name('index');
        });
    });
});

/* I have created this routes to only work with them in the front-end */
Route::inertia('/checkout', 'Checkout');
Route::inertia('/order-placed', 'OrderPlaced');




Route::get('/ticket/{eventId}', [UserTicketsController::class, 'index']);

require __DIR__.'/auth.php';
