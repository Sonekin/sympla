FORMAT: 1A

# Passaporte Digital API
Essa API foi desenvolvida de modo a funcionar com o [Inertia](https://inertiajs.com/).

OBSERVAÇÃO: Esse documento ainda **está sendo escrito**.


# 1. EVENTOS
Essa seção descreve as páginas e endpoints referentes à gestão de Eventos no Painel Administrativo do Organizador.

### 1.1 Página Meus Eventos [/organizador/meus-eventos]
Busca as informações necessárias para o carregamento inicial da página.
Retorna uma lista com os eventos pertencentes às companhias das quais o organizador autenticado faz parte.

+ Props (Vue.js)

    ```js
    {
        "events": [
            {
                "id": 1,
                "name": "A festa do ano",
                "slug": "a-festa-do-ano",
                "event_status": "draft",
                "description": "Teremos 50 atrações",
                "date": "2022-08-16 22:12:01",
                "flyer_image": "uma imagem",
                "total_tickets": 400,
                "sold_tickets": 3,
                "sold_value": 91.5
            },
            {
                "id": 2,
                "name": "Forrozinho bom",
                "slug": "forrozinho-bom",
                "event_status": "draft",
                "description": "Muita música boa e cervejinha.",
                "date": "2022-08-20 22:12:01",
                "flyer_image": "imagem sensacional",
                "total_tickets": 100,
                "sold_tickets": 1,
                "sold_value": 50
            }
        ]
    }
    ```

### 1.2 Página Painel do Evento [/organizador/painel-do-evento/{eventId}]
Busca as informações necessárias para o carregamento inicial da página.
Retorna o evento, os tipos de ingressos associados e uma lista com a quantidade de ingressos vendidos por data.

+ Parâmetros

    + eventId (integer) - ID do evento que será exibido no Painel.
    
+ Props (Vue.js)

    ```js
    {
        "event": {
            "id": 1,
            "name": "A festa do ano",
            "slug": "a-festa-do-ano",
            "event_status": "draft",
            "description": "Teremos 50 atrações",
            "date": "2022-08-16 22:12:01",
            "flyer_image": "uma imagem",
            "total_tickets": 400,
            "sold_tickets": 3,
            "sold_value": 86
        },
        "ticketTypes": [
            "Promocional",
            "1º lote | Meia entrada",
            "2º lote | Meia entrada",
            "2º lote | Inteira"
        ],
        "soldTickets": [
            {
                "sale_date": "2022-07-21",
                "sold": 1
            },
            {
                "sale_date": "2022-07-28",
                "sold": 2
            }
        ]
    }
    ```

### 1.3 Página Criar Novo Evento [/organizador/meus-eventos/criar-evento]
Busca as informações necessárias para o carregamento inicial da página.
Retorna os endereços de evento disponíveis e os assuntos e categorias de eventos.
    
+ Props (Vue.js)

    ```js
    {
        "eventAddressList": [
            {
                "id": 4,
                "company_id": 1,
                "name": "Casa do Zezinho",
                "street": "Rua Hermes de Paula",
                "number": "146",
                "complement": "Bloco 7 Ap 506",
                "district": "Vila Brasília",
                "postal_code": "39401-138",
                "city": "Montes Claros",
                "state": "MG"
            },
            {
                "id": 12,
                "company_id": 1,
                "name": "Escola do Zezinho",
                "street": "Rua dos Sabiás",
                "number": "256",
                "complement": "Bloco 1 Ap 505",
                "district": "Vila Atlântida",
                "postal_code": "39401-150",
                "city": "São Paulo",
                "state": "SP"
            }
        ],
        "subjectList": {
            "1": "Acadêmico e científico",
            "2": "Artesanato",
            "3": "Casa e estilo de vida",
            "4": "Cinema, fotografia",
            "5": "Desenvolvimento pessoal",
            "6": "Design, métricas e produtos digitais",
            "7": "Direito e legislação",
            "8": "Empreendedorismo, negócios e inovação",
            "9": "Esportes",
            "10": "Games e geek",
            "11": "Gastronomia, comidas e bebidas",
            "12": "Governo e política",
            "13": "Informática, tecnologia e programação",
            "14": "Marketing e vendas",
            "15": "Moda e beleza",
            "16": "Música",
            "17": "Outro",
            "18": "Religião, espiritualidade",
            "19": "Saúde, nutrição e bem-estar",
            "20": "Sociedade e cultura",
            "21": "Teatro, stand up e dança"
        },
        "categoryList": {
            "1": "Competição ou torneio",
            "2": "Corrida",
            "3": "Curso, aula, treinamento ou workshop",
            "4": "Drive-in",
            "5": "Espetáculos",
            "6": "Feira, festival ou exposição",
            "7": "Festa, festival ou show",
            "8": "Meetup ou evento de networking",
            "9": "Missa ou culto",
            "10": "Outro",
            "11": "Palestra, congresso ou seminário",
            "12": "Passeios, excursões ou tour",
            "13": "Retiro ou acampamento"
        }
    }
    ```

### 1.4 Página Editar Evento [/organizador/meus-eventos/{eventId}/editar-evento]
Busca as informações necessárias para o carregamento inicial da página.
Retorna o evento sendo editado, endereços de evento disponíveis e os assuntos e categorias de eventos.

+ Parâmetros

    + eventId (integer) - ID do evento que será editado.

+ Props (Vue.js)

    ```js
    {
        "event": {
            "id": 7,
            "name": "Aniversário do Zezinho",
            "slug": "aniversario-do-zezinho",
            "event_status": "draft",
            "subject": "Games e geek",
            "category": "Festa, festival ou show",
            "city": "São Paulo",
            "state": "SP",
            "description": "<h1>Vai ser uma festinha espetacular!</h1>",
            "date": "2022-08-16 16:00:00",
            "flyer_image": null,
            "total_tickets": null,
            "sold_tickets": null,
            "sold_value": null
        },
        "eventAddress": {
            "id": 4,
            "company_id": 1,
            "name": "Casa do Zezinho",
            "street": "Rua Hermes de Paula",
            "number": "146",
            "complement": "Bloco 7 Ap 506",
            "district": "Vila Brasília",
            "postal_code": "39401-138",
            "city": "Montes Claros",
            "state": "MG"
        },
        "eventAddressList": [
            {
                "id": 4,
                "company_id": 1,
                "name": "Casa do Zezinho",
                "street": "Rua Hermes de Paula",
                "number": "146",
                "complement": "Bloco 7 Ap 506",
                "district": "Vila Brasília",
                "postal_code": "39401-138",
                "city": "Montes Claros",
                "state": "MG"
            },
            {
                "id": 12,
                "company_id": 1,
                "name": "Escola do Zezinho",
                "street": "Rua dos Sabiás",
                "number": "256",
                "complement": "Bloco 1 Ap 505",
                "district": "Vila Atlântida",
                "postal_code": "39401-150",
                "city": "São Paulo",
                "state": "SP"
            }
        ],
        "subjectList": {
            "1": "Acadêmico e científico",
            "2": "Artesanato",
            "3": "Casa e estilo de vida",
            "4": "Cinema, fotografia",
            "5": "Desenvolvimento pessoal",
            "6": "Design, métricas e produtos digitais",
            "7": "Direito e legislação",
            "8": "Empreendedorismo, negócios e inovação",
            "9": "Esportes",
            "10": "Games e geek",
            "11": "Gastronomia, comidas e bebidas",
            "12": "Governo e política",
            "13": "Informática, tecnologia e programação",
            "14": "Marketing e vendas",
            "15": "Moda e beleza",
            "16": "Música",
            "17": "Outro",
            "18": "Religião, espiritualidade",
            "19": "Saúde, nutrição e bem-estar",
            "20": "Sociedade e cultura",
            "21": "Teatro, stand up e dança"
        },
        "categoryList": {
            "1": "Competição ou torneio",
            "2": "Corrida",
            "3": "Curso, aula, treinamento ou workshop",
            "4": "Drive-in",
            "5": "Espetáculos",
            "6": "Feira, festival ou exposição",
            "7": "Festa, festival ou show",
            "8": "Meetup ou evento de networking",
            "9": "Missa ou culto",
            "10": "Outro",
            "11": "Palestra, congresso ou seminário",
            "12": "Passeios, excursões ou tour",
            "13": "Retiro ou acampamento"
        }
    }
    ```

### 1.5 Endpoint filtrar Meus Eventos [GET] [/organizador/meus-eventos/filtro?event_status={event_status}&name={name}]
Retorna um JSON para uma chamada tipo ajax com os eventos pertencentes às companhias das quais o organizador autenticado faz parte
de acordo com os filtros passados por parâmetro.

+ Parâmetros

    + event_status (string, opcional) - O slug do status dos eventos filtrados.

    + name (string, opcional) - Termo para buscar por eventos que o contenham no nome.

+ Resposta 200 (application/json)

    ```js
    {
        "events": [
            {
                "id": 1,
                "name": "A festa do ano",
                "slug": "a-festa-do-ano",
                "event_status": "draft",
                "description": "Teremos 50 atrações",
                "date": "2022-08-16 22:12:01",
                "flyer_image": "uma imagem",
                "total_tickets": 400,
                "sold_tickets": 3,
                "sold_value": 91.5
            }
        ]
    }
    ```

### 1.6 Endpoint salvar Endereço de Evento de uma Companhia [POST] [/organizador/{companyId}/enderecos-eventos]
Insere um novo Endereço de Evento associado à companhia. O ID da companhia e o nome do endereço associado a ela são um conjunto único.
Retorna o endereço inserido em caso de sucesso.

+ Parâmetros

    + companyId (integer) - ID da companhia ao qual pertence o endereço de eventos.

    + address_name (string) - Nome do novo endereço.

    + street (string) -  Nome da rua ou avenida.

    + district (string) -  Nome do bairro.

    + number (string, opcional) -  Número do endereço do local.

    + complement (string, opcional) -  Complemento do endereço do local.

    + postal_code (string) -  Código postal ou CEP.

    + city (string) -  Cidade em que ocorrerá o evento.

    + state (string) -  Estado em que ocorrerá o evento.

+ Resposta 200 (application/json)

    ```js
    {
        "eventAddress": {
            "company_id": 1,
            "name": "Casa do Zezinho",
            "street": "Rua Hermes de Paula",
            "number": "146",
            "complement": "Bloco 7 Ap 506",
            "district": "Vila Brasília",
            "postal_code": "39401-138",
            "city": "Montes Claros",
            "state": "MG"
        }
    }
    ```

### 1.7 Endpoint Cadastrar Evento [POST] [/organizador/meus-eventos]
Insere um novo evento com a possibilidade de inserir também um novo endereço em que ele ocorrerá.
Retorna o evento e o endereço associado em caso de sucesso.

+ Parâmetros

    + address_id (integer, opcional) - ID de local de realização de eventos já salvo para a companhia. Nesse caso, os campos referentes ao endereço são desnecessários.

    + save_address (bit, opcional) - 0 para somente salvar o evento de modo que local "ainda será definido". 1 para criar um novo endereço (nesse caso, o campo address_id é desnecessário e os campos de endereço descritos no endpoint "1.4 Endpoint salvar Endereço de Evento de uma Companhia", exceto companyId, devem ser passados junto à requisição).

    + name (string) - Nome do evento.

    + event_status (string, opcional) - O slug do status do novo evento.

    + subject (integer, opcional) - ID do tipo de assunto tratado no evento.

    + category (integer, opcional) - ID da categoria do evento.

    + description (string, opcional) - Descrição do evento.

    + date (string, Y-m-d H:i) - Data e horário de início do evento.

    + end_date (string, Y-m-d H:i, opcional) - Data e horário de término do evento.

    + visible (bit, opcional) - 0 se visibilidade do evento for Privado. 1 se visibilidade do evento for Público.

+ Resposta 200 (application/json)

    ```js
    {
         "event": {
            "id": 7,
            "name": "Aniversário do Zezinho",
            "slug": "aniversario-do-zezinho",
            "event_status": "draft",
            "subject": "Games e geek",
            "category": "Festa, festival ou show",
            "city": "São Paulo",
            "state": "SP",
            "description": "<h1>Vai ser uma festinha espetacular!</h1>",
            "date": "2022-08-15 16:00",
            "flyer_image": null,
            "total_tickets": null,
            "sold_tickets": null,
            "sold_value": null
        },
        "eventAddress": {
            "company_id": 1,
            "name": "Escola do Zezinho",
            "street": "Rua dos Sabiás",
            "number": "256",
            "complement": "Bloco 1 Ap 505",
            "district": "Vila Atlântida",
            "postal_code": "39401-150",
            "city": "São Paulo",
            "state": "SP"
        }
    }
    ```

### 1.8 Endpoint Editar Evento [PUT] [/organizador/meus-eventos/{eventId}]
Atualiza as informações do evento cujo ID foi passado via parâmetro. Há também a possibilidade de inserir um novo endereço em que ele ocorrerá.
Retorna o evento e o endereço associado com as informações atualizadas em caso de sucesso.

+ Parâmetros

    + eventId (integer) - ID do evento que será editado.

    + address_id (integer, opcional) - ID de local de realização de eventos já salvo para a companhia. Nesse caso, os campos referentes ao endereço são desnecessários.

    + save_address (bit, opcional) - 0 para somente salvar o evento de modo que local "ainda será definido". 1 para criar um novo endereço (nesse caso, o campo address_id é desnecessário e os campos de endereço descritos no endpoint "1.4 Endpoint salvar Endereço de Evento de uma Companhia", exceto companyId, devem ser passados junto à requisição).

    + name (string) - Nome do evento.

    + event_status (string, opcional) - O slug do status do evento.

    + subject (integer, opcional) - ID do tipo de assunto tratado no evento.

    + category (integer, opcional) - ID da categoria do evento.

    + description (string, opcional) - Descrição do evento.

    + date (string, Y-m-d H:i) - Data e horário de início do evento.

    + end_date (string, Y-m-d H:i, opcional) - Data e horário de término do evento.

    + visible (bit, opcional) - 0 se visibilidade do evento for Privado. 1 se visibilidade do evento for Público.

+ Resposta 200 (application/json)

    ```js
    {
        "event": {
            "id": 7,
            "name": "Aniversário do Zezinho",
            "slug": "aniversario-do-zezinho",
            "event_status": "draft",
            "subject": "Games e geek",
            "category": "Festa, festival ou show",
            "city": "São Paulo",
            "state": "SP",
            "description": "<h1>Vai ser uma festinha espetacular!</h1>",
            "date": "2022-08-16 16:00",
            "flyer_image": null,
            "total_tickets": null,
            "sold_tickets": null,
            "sold_value": null
        },
        "eventAddress": {
            "id": 4,
            "company_id": 1,
            "name": "Casa do Zezinho",
            "street": "Rua Hermes de Paula",
            "number": "146",
            "complement": "Bloco 7 Ap 506",
            "district": "Vila Brasília",
            "postal_code": "39401-138",
            "city": "Montes Claros",
            "state": "MG"
        }
    }
    ```

### 1.9 Endpoint Excluir Evento [DELETE] [/organizador/meus-eventos/{eventId}]
Exclui da aplicação o evento passado via ID.

+ Parâmetros

    + eventId (integer) - ID do evento que será excluído.

+ Resposta 200 (application/json)

    ```js
    {
        {
            "success": true
        }
    }
    ```


# 2. INGRESSOS
Essa seção descreve as páginas e endpoints referentes à gestão de Ingressos no Painel Administrativo do Organizador.

### 2.1 Página Painel de Ingressos [/organizador/ingressos-dashboard/{eventId}]
Busca as informações necessárias para o carregamento inicial da página.
Retorna o evento, os tipos de ingressos associados, uma lista com a quantidade de ingressos vendidos por data e uma lista com detalhes de todos
os ingressos pertencentes ao evento.

+ Parâmetros

    + eventId (integer) - ID do evento a que pertencem os Ingressos.
    
+ Props (Vue.js)

    ```js
    {
        "event": {
            "id": 1,
            "name": "A festa do ano",
            "slug": "a-festa-do-ano",
            "event_status": "draft",
            "description": "Teremos 50 atrações",
            "date": "2022-08-26 19:23:57",
            "flyer_image": "uma imagem",
            "total_tickets": 400,
            "sold_tickets": 3,
            "sold_value": 110
        },
        "tickets": [
            {
                "id": 1,
                "description": "Promocional",
                "price": 15,
                "fee": 1.5,
                "quantity": 100,
                "start_sales": "2022-07-21 19:23:57",
                "end_sales": "2022-08-05 19:23:57",
                "sold": 0
            },
            {
                "id": 2,
                "description": "1º lote | Meia entrada",
                "price": 25,
                "fee": 2.5,
                "quantity": 100,
                "start_sales": "2022-08-05 19:23:57",
                "end_sales": "2022-08-20 19:23:57",
                "sold": 0
            },
            {
                "id": 3,
                "description": "2º lote | Meia entrada",
                "price": 30,
                "fee": 3,
                "quantity": 100,
                "start_sales": "2022-08-05 19:23:57",
                "end_sales": "2022-08-20 19:23:57",
                "sold": 2
            },
            {
                "id": 4,
                "description": "1º lote | Inteira",
                "price": 50,
                "fee": 5,
                "quantity": 100,
                "start_sales": "2022-08-05 19:23:57",
                "end_sales": "2022-08-20 19:23:57",
                "sold": 1
            }
        ],
        "ticketTypes": [
            "Promocional",
            "1º lote | Meia entrada",
            "2º lote | Meia entrada",
            "1º lote | Inteira"
        ],
        "soldTickets": [
            {
                "sale_date": "2022-08-01",
                "sold": 1
            },
            {
                "sale_date": "2022-08-06",
                "sold": 2
            }
        ]
    }
    ```

### 2.2 Endpoint filtrar Ingressos do Evento [GET] [/organizador/{eventId}/ingressos/filtro?since_days_ago={since_days_ago}&paid_ticket={paid_ticket}&ticket_type={ticket_type}]
Retorna um JSON para uma chamada tipo ajax com os ingressos vendidos pertencentes ao evento de acordo com os filtros passados por parâmetro.

+ Parâmetros

    + eventId (integer) - ID do evento ao qual os ingressos pertencem.

    + since_days_ago (integer, opcional) - Quantidade limite de dias atrás em que tenha ocorrido a data da venda. (Por exemplo, vendas nos últimos 7 dias)

    + paid_ticket (bit, opcional) - 0 para somente ingressos gratuitos, 1 para somente ingressos pagos.

    + ticket_type (string, opcional) - Descrição do tipo de ingresso.

+ Resposta 200 (application/json)

    ```js
    {
        "soldTickets": [
            {
                "sale_date": "2022-07-21",
                "sold": 1
            },
            {
                "sale_date": "2022-07-28",
                "sold": 2
            }
        ]
    }
    ```

### 2.3 Endpoint Cadastrar Ingresso do Evento [POST] [/organizador/ingressos/{eventId}]
Insere um novo ingresso vinculado ao ID de evento passado via parâmetro.
Retorna o ingresso que foi cadastrado.

+ Parâmetros

    + eventId (integer) - ID do evento ao qual o ingresso pertence.

    + description (string) - Título do ingresso.

    + status (bit, opcional) - 0 para desativado e invisível. 1 para ativo e visível.

    + price (float) - Preço do ingresso.

    + quantity (integer) - Quantidade de ingressos disponíveis.

    + start_sales (string, Y-m-d H:i) - Data e horário de início das vendas.

    + end_sales (string, Y-m-d H:i) - Data e horário de término das vendas.

    + detailed_description (string, opcional) - Descrição textual com detalhes sobre o ingresso.

    + min_purchase_quantity (integer, opcional) - Quantidade mínima de ingressos por compra.

    + max_purchase_quantity (integer, opcional) - Quantidade máxima de ingressos por compra.
 
    + availability (integer, opcional) - Disponibilidade do ingresso:
        - 1 : Para todo o público,
        - 2 : Convites pessoais por e-mail,
        - 3 : Link único pra compartilhar,
        - 4 : Para ser adicionado manualmente.

+ Resposta 200 (application/json)

    ```js
    {
        "ticket": {
            "id": 9,
            "event_id": 7,
            "description": "Ingresso VIP",
            "detailed_description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
            "price": "75.25",
            "fee": 2.5,
            "quantity": "25",
            "status": "1",
            "availability": "Para ser adicionado manualmente",
            "min_purchase_quantity": "2",
            "max_purchase_quantity": "3",
            "start_sales": "2022-08-30 15:00",
            "end_sales": "2022-08-31 15:00",
            "sold": 0
        }
    }
    ```

### 2.4 Endpoint Editar Ingresso [PUT] [/organizador/ingressos/{ticketId}]
Atualiza as informações do ingresso cujo ID foi passado via parâmetro.
Retorna o ingresso com as informações atualizadas em caso de sucesso.

+ Parâmetros

    + ticketId (integer) - ID do ingresso a editar.

    + description (string) - Título do ingresso.

    + status (bit, opcional) - 0 para desativado e invisível. 1 para ativo e visível.

    + price (float) - Preço do ingresso.

    + quantity (integer) - Quantidade de ingressos disponíveis.

    + start_sales (string, Y-m-d H:i) - Data e horário de início das vendas.

    + end_sales (string, Y-m-d H:i) - Data e horário de término das vendas.

    + detailed_description (string, opcional) - Descrição textual com detalhes sobre o ingresso.

    + min_purchase_quantity (integer, opcional) - Quantidade mínima de ingressos por compra.

    + max_purchase_quantity (integer, opcional) - Quantidade máxima de ingressos por compra.
 
    + availability (integer, opcional) - Disponibilidade do ingresso:
        - 1 : Para todo o público,
        - 2 : Convites pessoais por e-mail,
        - 3 : Link único pra compartilhar,
        - 4 : Para ser adicionado manualmente.

+ Resposta 200 (application/json)

    ```js
    {
        "ticket": {
            "id": 9,
            "event_id": 7,
            "description": "Ingresso VIP",
            "detailed_description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
            "price": 75.25,
            "fee": 2.5,
            "quantity": 25,
            "status": 1,
            "availability": "Para ser adicionado manualmente",
            "min_purchase_quantity": 2,
            "max_purchase_quantity": 3,
            "start_sales": "2022-08-30 15:00:00",
            "end_sales": "2022-09-30 19:00:00",
            "sold": 0
        }
    }
    ```

### 2.5 Endpoint Excluir Ingresso [DELETE] [/organizador/ingressos/{ticketId}]
Exclui da aplicação o ingresso passado via ID.

+ Parâmetros

    + ticketId (integer) - ID do ingresso que será excluído.

+ Resposta 200 (application/json)

    ```js
    {
        {
            "success": true
        }
    }
    ```


# 3. PARTICIPANTES
Essa seção descreve as páginas e endpoints referentes à gestão de Participantes no Painel Administrativo do Organizador.

### 3.1 Página Administrar Participantes [/organizador/participantes-dashboard/{eventId}]
Busca as informações necessárias para o carregamento inicial da página.
Retorna o evento e uma lista com detalhes de seus participantes e visão geral sobre os pagamentos.

+ Parâmetros

    + eventId (integer) - ID do evento ao qual irão os participantes.
    
+ Props (Vue.js)

    ```js
    {
        "event": {
            "id": 1,
            "name": "A festa do ano",
            "slug": "a-festa-do-ano",
            "event_status": "draft",
            "subject": null,
            "category": null,
            "city": null,
            "state": null,
            "description": "Teremos 50 atrações",
            "date": "2022-08-31 22:13:57",
            "flyer_image": "uma imagem",
            "total_tickets": null,
            "sold_tickets": null,
            "sold_value": null
        },
        "ticketTypes": [
            "Promocional",
            "1º lote | Meia entrada",
            "2º lote | Meia entrada",
            "2º lote | Inteira"
        ],
        "participants": [
            {
                "status": "paid",
                "name": "Maria Joana",
                "ticket_number": "1Y8H84Y8ZYT",
                "ticket_type": "1º lote | Inteira",
                "buyer_name": "Daniel Oliveira Paixão",
                "purchase_date": "2022-08-28 19:08:50"
            },
            {
                "status": "paid",
                "name": "João Pedro",
                "ticket_number": "1Y8H84Y8ZYT",
                "ticket_type": "1º lote | Inteira",
                "buyer_name": "Daniel Oliveira Paixão",
                "purchase_date": "2022-08-28 19:08:50"
            },
            {
                "status": "pending",
                "name": "Zezinho Santos",
                "ticket_number": "SLDB-Z1-U4MF",
                "ticket_type": "2º lote | Inteira",
                "buyer_name": "Consumer Teste",
                "purchase_date": "2022-08-28 19:10:50"
            }
        ],
    }
    ```

### 3.2 Endpoint filtrar Participantes do Evento [GET] [/organizador/{eventId}/participantes/filtro?search={search}&status={status}&ticket_type={ticket_type}]
Retorna um JSON para uma chamada tipo ajax com os participantes de um evento de acordo com os filtros passados por parâmetro.

+ Parâmetros

    + eventId (integer) - ID do evento ao qual irão os participantes.

    + search (string, opcional) - Busca por nome de participante, e-mail ou número do ingresso.
    
    + status (string, opcional)
        - "paid" para Pagamento Confirmado. 
        - "pending" para Pagamento Pendente. 

    + ticket_type (string, opcional) - Descrição do tipo de ingresso.

+ Resposta 200 (application/json)

    ```js
    {
        "event": {
            "id": 1,
            "name": "A festa do ano",
            "slug": "a-festa-do-ano",
            "event_status": "draft",
            "subject": null,
            "category": null,
            "city": null,
            "state": null,
            "description": "Teremos 50 atrações",
            "date": "2022-08-31 22:13:57",
            "flyer_image": "uma imagem",
            "total_tickets": null,
            "sold_tickets": null,
            "sold_value": null
        },
        "participants": [
            {
                "status": "pending",
                "name": "Zezinho Santos",
                "ticket_number": "SLDB-Z1-U4MF",
                "ticket_type": "2º lote | Inteira",
                "buyer_name": "Consumer Teste",
                "purchase_date": "2022-08-28 19:10:50"
            }
        ]
    }
    ```
