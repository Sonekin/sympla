<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title inertia>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{ mix('css/tailwind.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- Styles -->
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
            text-align: left;
        }

        .container {
            margin: 0 auto;
        }

        #ticket {
            margin-top: 5%;
        }

        .header {
            background: #172cc1;
            color: #fff;
            border-bottom: 4px dashed #fff;
            border-radius: 4px;
        }

        .padding-20 {
            padding: 20px;
        }

        h1 {
            margin: 20px 0;
        }

        h4 {
            font-weight: 400;
        }

        .logo {
            max-width: 250px;
            text-align: center;
            justify-content: center;
            align-items: center;
            margin: 0 auto;
        }

        .qrcode {
            text-align: center;
            justify-content: center;
            align-items: center;
        }

        .box-info {
            border: .5px solid #fff;
            border-radius: 5px;
            max-width: 60%;
            text-align: center;
            padding: 15px 0;
        }

        .box-content {
            border: 1px solid #172cc1;
        }

        .border-divider {
            border-top: 1px solid #172cc1
        }

        .code {
            text-transform: uppercase;
            font-size: 20px;
            text-align: center;;
        }

        .qrcode-mobile { display:none; justify-content: center; align-items: center; }

        @media (max-width: 600px)
        {
            #ticket {
                margin-top: 0;
            }
            .qrcode-mobile
            {
                display: block;
            }
        }
    </style>
</head>

<body>

    <div class="container text-center">
        <div id="ticket">
            <div class="row header">
                <div class="col col-8 padding-20">

                    <div class="row">
                        <h1>{{ $evento->name }}</h1>
                    </div>

                    <div class="row">

                        <div class="col text-center">
                            <div class="box-info">
                                <span class="icon clearfix"><i class="fa-sharp fa-solid fa-calendar"></i></span>
                                <span class="label-icons">1 de Outubro</span>
                            </div>
                        </div>

                        <div class="col text-center">
                            <div class="box-info">
                                <span class="icon clearfix"><i class="fa-sharp fa-solid fa-clock"></i></span>
                                <span class="label-icons">17 horas</span>
                            </div>
                        </div>

                        <div class="col text-center">
                            <div class="box-info">
                                <span class="icon clearfix"><i class="fa-sharp fa-solid fa-location-pin"></i></span>
                                <span class="label-icons">Montes Claros</span>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-4 padding-20 text-center logo flex">
                    <img src="{{asset('images/logo-branca.png')}}" class="img-responsive" alt="Seu Passaporte Digital" />
                </div>
            </div>

            <div class="row border-divider qrcode-mobile padding-20">
                @if($qrcode)
                <div class="col qrcode flex">
                    {!! $qrcode !!}
                </div>
                @endif
                <label class="code text-center">{{ $ticket_pass->code }}</label>
            </div>

            <div class="row box-content">
                <div class="col col-8 padding-20">

                    <div class="row">
                        <div class="row">
                            <h4 class="text-muted">Ingresso</h4>
                            <h3>{{ $ticket->description }}</h3>
                        </div>

                        <div class="row ">
                            <h4 class="text-muted">Valor</h4>
                            <h3>R$ {{ number_format($order->amount, 2) }}</h3>
                        </div>

                        <div class="row">
                            <h4 class="text-muted">Local</h4>
                            <p>Parque de Exposições João Alencar Athaide - Montes Claros MG</p>
                        </div>

                        @if($transaction && !is_null($transaction->payment_date))
                        <div class="row">
                            <p class="text-muted">Comprado em {{ date('d/m/Y', strtotime($transaction->payment_date)) }}</p>
                        </div>
                        @endif
                    </div>
                    <div class="row">
                        <h4 class="text-muted">Nome</h4>
                        <h3>{{ $ticket_holder->name }}</h3>
                    </div>
                </div>

                <div class="col col-4 padding-20 text-center">
                    @if($qrcode)
                    <div class="row qrcode">
                        {!! $qrcode !!}
                    </div>
                    @endif

                    <label class="code">{{ $ticket_pass->code }}</label>
                </div>

            </div>
        </div>
    </div>

</body>

</html>