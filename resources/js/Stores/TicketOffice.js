import { defineStore } from 'pinia'
import { formatDecimal } from "@/Common/Helpers";

export const useTicketOfficeStore = defineStore({
  id: 'ticketOffice',
  state: () => {
    return {
      eventInfo: {},
      tickets: [],
      buyer: {
        data: {},
        payment: {}
      },
      holders: {
        data: []
      }
    };
  },
  getters: {
    cart: (state) => {
      return {
        items: () => {
          const items = [];
          state.tickets.forEach(item => {
            items.push(item);
          });
          return items;
        },
        getQuantity: () => {
          return (ticketId) => {
            if (state.tickets[ticketId]) {
              return state.tickets[ticketId].quantity;
            }
            return 0;
          }
        },
        amountCart: () => {
          return formatDecimal(state.tickets.reduce(
            (amount, ticket) => {
              return amount + (ticket.quantity * (ticket.price + ticket.fee));
            },
            0
          ));
        },
        countTotalItems: () => {
          let total = 0;
          state.tickets.forEach(item => {
            total += item.quantity;
          });
          return total;
        }
      }
    },
    event: (state) => {
      return {
        get: () => {
          return state.eventInfo.event;
        }
      }
    },
    buyer: (state) => {
      return {
        get: () => {
          return state.buyer.data;
        },
        getPayment: () => {
          return state.buyer.payment;
        }
      }
    },
    holders: (state) => {
      return {
        get: () => {
          return state.holders.data;
        }
      }
    }
  },
  actions: {
    cartActions() {
      const tickets = this.tickets;
      return {
        addItem(ticketId, price, fee, ticket) {
          if (tickets[ticketId]) {
            tickets[ticketId].quantity++;
            return;
          }

          tickets[ticketId] = {
            'ticketId': ticketId,
            'quantity': 1,
            'price': price,
            'fee': fee,
            'ticket': ticket,
          };
        },
        removeItem(ticketId) {
          if (! tickets[ticketId]){
            return;
          }

          if (tickets[ticketId].quantity == 1) {
            delete tickets[ticketId];
            return;
          }

          tickets[ticketId].quantity--;
        },
      }
    },
    eventActions() {
      let eventInfo = this.eventInfo;
      return {
        store(event) {
          eventInfo.event = event;
        }
      }
    },
    buyerActions() {
      let buyer = this.buyer;
      return {
        store(b) {
          buyer.data = b;
        }
      }
    },
    holdersActions() {
      let holders = this.holders;
      return {
        store(h) {
          holders.data = h;
        }
      }
    }
  },
})
