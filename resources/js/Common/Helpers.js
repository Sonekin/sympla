import moment from 'moment'
import 'moment/locale/pt-br'
moment.locale('pt-br')
import { cpf } from 'cpf-cnpj-validator';

export function formatDecimal(value, fixed = 2) {
  return parseFloat(value).toFixed(fixed);
}

export function formatDate(date, from="YYYY-MM-DD", to="DD/MM/YYYY") {
  return moment(
    date,
    from
   ).format(to);
}

export function cpfValidate(value) {
  return cpf.isValid(value);
}
