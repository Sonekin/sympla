export default function eventException(message, name = null) {
    this.message = message;
    this.name = name ? name : "eventException";
 }