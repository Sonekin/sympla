import locale_pt_br from "./pt-br";

export const pt_br = locale_pt_br;

export default {
  pt_br
};
