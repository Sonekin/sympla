import './bootstrap';

import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';
import { createPinia } from 'pinia'

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel';

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => {
      const page = require(`./Pages/${name}.vue`)
      return page
    },
    setup({ el, app, props, plugin }) {
      const VueApp = createApp({ render: () => h(app, props) });

      VueApp.use(plugin)
        .use(createPinia())
        .mixin({ methods: { route } })
        .mount(el);
    },
});

InertiaProgress.init({ color: '#4B5563' });
