import * as yup from 'yup';
import { pt_br } from '@/Common/yup/locale'
import { cpfValidate } from "@/Common/Helpers";

yup.setLocale(pt_br);
const rulesValidation = yup.object().shape({
  registrations: yup.array().of(
    yup.object().shape({
      name: yup.string().required(),
      email: yup.string().required().email(),
      phone: yup.string().required(),
      ticket_id: yup.string().required(),
    })
  ),
  buyer: yup.object().shape({
    cpf: yup.string().required().test(
      'test-invalid-cpf',
      'CPF inválido',
      (cpf) =>  cpfValidate(cpf)
    ),
    name: yup.string().required(),
    email: yup.string().required().email(),
    phone: yup.string().required(),
  }),
});

export default rulesValidation;
