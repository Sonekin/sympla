import * as yup from 'yup';
import { pt_br } from '@/Common/yup/locale'

yup.setLocale(pt_br);
const rulesValidation = yup.object().shape({
  paymentMethod: yup.string().required().oneOf(['payment_book', 'invoice', 'credit_card', 'pix']),
});

export default rulesValidation;
