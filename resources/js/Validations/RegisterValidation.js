import * as yup from 'yup';
import { pt_br } from '@/Common/yup/locale'
import { cpfValidate } from "@/Common/Helpers";

yup.setLocale(pt_br);
const rulesValidation = yup.object().shape({
  cpf: yup.string().required().test(
    'test-invalid-cpf',
    'CPF inválido',
    (cpf) =>  cpfValidate(cpf)
  ),
  name: yup.string().required(),
  email: yup.string().required().email(),
  phone: yup.string().required(),
  password: yup.string().required().min(8),
  password_confirmation: yup.string().required().min(8).oneOf([yup.ref("password")], "As senhas informadas são diferentes."),
});

export default rulesValidation;
