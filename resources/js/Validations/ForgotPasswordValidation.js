import * as yup from 'yup';
import { pt_br } from '@/Common/yup/locale'

yup.setLocale(pt_br);
const rulesValidation = yup.object().shape({
  email: yup.string().required().email(),
});

export default rulesValidation;
