import * as yup from 'yup';
import { pt_br } from '@/Common/yup/locale'

yup.setLocale(pt_br);
const rulesValidation = yup.object().shape({
  email: yup.string().required().email(),
  password: yup.string().required().min(8),
  password_confirmation: yup.string().required().min(8).oneOf([yup.ref("password")], "As senhas informadas são diferentes."),
});

export default rulesValidation;
