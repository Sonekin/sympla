import * as yup from 'yup';
import { pt_br } from '@/Common/yup/locale'

yup.setLocale(pt_br);
const rulesValidation = yup.object().shape({
  email: yup.string().required().email(),
  password: yup.string().required().min(8),
});

export default rulesValidation;
