FORMAT: 1A

# Passaporte Digital API
Essa API foi desenvolvida de modo a funcionar com o [Inertia](https://inertiajs.com/).

OBSERVAÇÃO: Esse documento ainda **está sendo escrito**.


# 1. DADOS
Essa seção descreve as páginas e endpoints referentes à gestão de dados de cliente logado na Área do Cliente.

### 1.1 Página Meus Dados [/meus-dados]
Busca as informações necessárias para o carregamento inicial da página.
Retorna informações básicas sobre o cliente autenticado.

+ Props (Vue.js)

    ```js
    {
        "consumer": {
            "id": 1,
            "name": "Consumer Teste",
            "email": "ct@email.com",
            "phone": "(38) 95585-4521",
            "cpf": "853.507.320-56"
        }
    }
    ```

### 1.2 Endpoint Editar Meus Dados [PUT] [/meus-dados]
Atualiza os dados do usuário logado no sistema.
Retorna o cliente atualizado em caso de sucesso.

+ Parâmetros

    + name (string, opcional) - Novo nome de usuário.

    + email (string, opcional) - Novo e-mail de usuário.

    + phone (string, opcional) - Novo número de telefone.

    + cpf (string, opcional) - Atualiza o CPF do usuário.

    + password (string, opcional) - Atualiza a senha do usuário. Ao passar valor vazio, a senha atual é mantida.

+ Resposta 200 (application/json)

    ```js
    {
        "consumer": {
            "id": 1,
            "name": "Consumer Teste",
            "email": "ct@email.com",
            "phone": "(38) 95585-4521",
            "cpf": "853.507.320-56"
        }
    }
    ```

# 2. PEDIDOS
Essa seção descreve as páginas e endpoints referentes à gestão de pedidos do cliente logado na Área do Cliente.

### 1.1 Página Meus Pedidos [/meus-pedidos]
Busca as informações necessárias para o carregamento inicial da página.
Retorna informações básicas sobre os pedidos realizados pelo cliente.

+ Props (Vue.js)

    ```js
    {
        "orders": [
            {
                "id": 1,
                "created_at": "2022-09-23T02:06:00.000000Z",
                "event_name": "A festa do ano",
                "city": "Montes Claros"
            },
            {
                "id": 2,
                "created_at": "2022-09-23T02:08:44.000000Z",
                "event_name": null,
                "city": null
            }
        ]
    }
    ```