<?php

namespace App\Http\Resources\Organizer;

use Illuminate\Http\Resources\Json\JsonResource;

class ParticipantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'status' => $this->status,
            'name' => $this->name,
            'ticket_number' => $this->ticket_number,
            'ticket_type' => $this->ticket_type,
            'buyer_name' => $this->buyer_name,
            'purchase_date' => $this->purchase_date,
        ];
    }
}
