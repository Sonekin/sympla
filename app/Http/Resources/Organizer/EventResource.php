<?php

namespace App\Http\Resources\Organizer;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'event_status' => $this->event_status,
            'subject' => $this->subject,
            'category' => $this->category,
            'city' => $this->city,
            'state' => $this->state,
            'description' => $this->description,
            'date' => $this->date,
            'flyer_image' => $this->flyer_image,
            'total_tickets' => $this->total_tickets,
            'sold_tickets' => $this->sold_tickets,
            'sold_value' => $this->sold_value,
        ];
    }
}