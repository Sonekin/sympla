<?php

namespace App\Http\Resources\Organizer;

use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'event_id' => $this->event_id,
            'description' => $this->description,
            'detailed_description' => $this->detailed_description,
            'price' => $this->price,
            'fee' => $this->fee,
            'quantity' => $this->quantity,
            'status' => $this->status,
            'availability' => $this->availability,
            'min_purchase_quantity' => $this->min_purchase_quantity,
            'max_purchase_quantity' => $this->max_purchase_quantity,
            'start_sales' => $this->start_sales,
            'end_sales' => $this->end_sales,
            'sold' => $this->sold ?? 0,
        ];
    }
}
