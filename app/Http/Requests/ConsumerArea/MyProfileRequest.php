<?php

namespace App\Http\Requests\ConsumerArea;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MyProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'nullable',
                'string',
                'max:255'
            ],
            'email' => [
                'nullable',
                'string',
                'max:255',
                Rule::unique('users')->ignore(auth()->user()->id),
            ],
            'phone' => [
                'nullable',
                'string',
                'max:20',
                Rule::unique('users')->ignore(auth()->user()->id),
            ],
            'cpf' => [
                'nullable',
                'string',
                'max:14',
                Rule::unique('users')->ignore(auth()->user()->id),
            ],
            'password' => [
                'nullable',
                'string',
                'max:255'
            ],
        ];
    }
}
