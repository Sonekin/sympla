<?php

namespace App\Http\Requests;

use App\Models\Order;
use App\Rules\CPF;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EventRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge([
            'registrations.*.email' => [
                'required',
                'string',
                'email',
                'max:255',
                'distinct',
            ],
            'registrations.*.name' => [
                'required',
                'string',
                'max:255'
            ],
            'registrations.*.phone' => [
                'required',
                'string'
            ],
            'buyer.cpf' => [
                'required',
                'string',
                new CPF,
                Rule::unique('users', 'cpf')->ignore($this->input('buyer.email'), 'email')
            ],
            'buyer.email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique('users', 'email')->ignore($this->input('buyer.cpf'), 'cpf')
            ],
            'buyer.name' => [
                'required',
                'string',
                'max:255'
            ],
            'buyer.phone' => [
                'required',
                'string',
                Rule::unique('users', 'email')->ignore($this->input('buyer.cpf'), 'cpf')
            ],
            'payment.payment_method' => [
                'required',
                'string',
                Rule::in([
                    Order::PAYMENT_BOOK,
                    Order::INVOICE,
                    Order::CREDIT_CARD,
                    Order::PIX
                ])
            ],
            'payment.amount' => [
                'required',
                'numeric',
            ],
            'payment.installments' => [
                'required_if:payment.payment_method,' . Order::CREDIT_CARD .','. Order::PAYMENT_BOOK,
                'numeric',
                'min:1',
            ],
            'payment.card.number' => [
                'required_if:payment.payment_method,' . Order::CREDIT_CARD,
                'string',
                'min:15',
            ],
            'payment.card.holder_name' => [
                'required_if:payment.payment_method,' . Order::CREDIT_CARD,
                'string',
            ],
            'payment.card.expiration_date' => [
                'required_if:payment.payment_method,' . Order::CREDIT_CARD,
                'string',
                'date_format:m/y',
                'after_or_equal:' . Carbon::now()->firstOfMonth()
            ],
            'payment.card.cvv' => [
                'required_if:payment.payment_method,' . Order::CREDIT_CARD,
                'string',
            ]
        ]);
    }
}
