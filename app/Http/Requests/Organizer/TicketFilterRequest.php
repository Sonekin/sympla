<?php

namespace App\Http\Requests\Organizer;

use Illuminate\Foundation\Http\FormRequest;

class TicketFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'since_days_ago' => [
                'nullable',
                'integer',
            ],
            'paid_ticket' => [
                'nullable',
                'boolean',
            ],
            'ticket_type' => [
                'nullable',
                'string',
                'max:255'
            ],
        ];
    }
}
