<?php

namespace App\Http\Requests\Organizer;

use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            $this->has('save_address') && !empty($this->save_address) ? with(new EventAddressRequest)->rules() : [],
            [
                'address_id' => [
                    'nullable',
                    'integer',
                ],
                'name' => [
                    'required',
                    'string',
                    'max:255'
                ],
                'event_status' => [
                    'nullable',
                    'string',
                    'max:45'
                ],
                'subject' => [
                    'nullable',
                    'integer',
                ],
                'category' => [
                    'nullable',
                    'integer',
                ],
                'city' => [
                    'nullable',
                    'string',
                    'max:255'
                ],
                'state' => [
                    'nullable',
                    'string',
                    'max:45'
                ],
                'description' => [
                    'nullable',
                    'string',
                ],
                'date' => [
                    'required',
                    'date_format:Y-m-d H:i',
                ],
                'end_date' => [
                    'nullable',
                    'date_format:Y-m-d H:i',
                ],
                'flyer_image' => [
                    'nullable',
                    'string',
                ],
                'visible' => [
                    'nullable',
                    'boolean',
                ],
                'save_address' => [
                    'nullable',
                    'boolean',
                ],
            ],
        );
    }
}
