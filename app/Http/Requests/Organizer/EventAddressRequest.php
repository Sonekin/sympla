<?php

namespace App\Http\Requests\Organizer;

use Illuminate\Foundation\Http\FormRequest;

class EventAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address_name' => [
                'required',
                'string',
                'max:255'
            ],
            'street' => [
                'required',
                'string',
                'max:255'
            ],
            'number' => [
                'nullable',
                'string',
                'max:45'
            ],
            'complement' => [
                'nullable',
                'string',
                'max:255'
            ],
            'district' => [
                'required',
                'string',
                'max:255'
            ],
            'postal_code' => [
                'required',
                'string',
                'max:255'
            ],
            'city' => [
                'required',
                'string',
                'max:255'
            ],
            'state' => [
                'required',
                'string',
                'max:45'
            ],
        ];
    }
}
