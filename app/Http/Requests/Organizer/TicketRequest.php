<?php

namespace App\Http\Requests\Organizer;

use Illuminate\Foundation\Http\FormRequest;

class TicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => [
                'required',
                'string',
                'max:255'
            ],
            'price' => [
                'required',
                'numeric',
                'min:0'
            ],
            'detailed_description' => [
                'nullable',
                'string',
                'max:10000'
            ],
            'quantity' => [
                'required',
                'integer',
                'min:0',
            ],
            'status' => [
                'nullable',
                'boolean',
            ],
            'availability' => [
                'nullable',
                'integer',
                'min:1',
                'max:4'
            ],
            'min_purchase_quantity' => [
                'nullable',
                'integer',
                'min:0',
            ],
            'max_purchase_quantity' => [
                'nullable',
                'integer',
                'min:0',
            ],
            'start_sales' => [
                'required',
                'date_format:Y-m-d H:i',
            ],
            'end_sales' => [
                'required',
                'date_format:Y-m-d H:i',
            ],
        ];
    }
}
