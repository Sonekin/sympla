<?php

namespace App\Http\Requests\Organizer;

use Illuminate\Foundation\Http\FormRequest;

class ParticipantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'search' => [
                'nullable',
                'string',
                'max:255'
            ],
            'status' => [
                'nullable',
                'string',
                'max:255'
            ],
            'ticket_type' => [
                'nullable',
                'string',
                'max:255'
            ],
        ];
    }
}
