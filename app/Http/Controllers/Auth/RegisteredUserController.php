<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\UserAlreadyRegistrationIncompleteException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Providers\RouteServiceProvider;
use App\Services\Consumer\ConsumerRegisterService;
use Exception;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Auth/Register');
    }

    /**
     * @param RegisterRequest $request
     * @param ConsumerRegisterService $consumerRegisterService
     *
     */
    public function store(RegisterRequest $request, ConsumerRegisterService $consumerRegisterService)
    {
        try {
            $user = $consumerRegisterService->register($request->all());
            Auth::login($user);
            return redirect(RouteServiceProvider::HOME);
        } catch(UserAlreadyRegistrationIncompleteException $ex) {
            //send e-mail to user to complete their registration
        } catch(Exception $ex) {

        }

    }
}
