<?php

namespace App\Http\Controllers;

use App\Exceptions\TicketNotExistInEventException;
use App\Exceptions\TicketSoldOutException;
use App\Http\Requests\EventRegistrationRequest;
use App\Http\Resources\EventResource;
use App\Http\Resources\TicketPassResource;
use App\Http\Resources\UserResource;
use App\Services\EventRegistration\EventRegistrationService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class EventRegistrationController extends Controller
{
    /**
     * @var EventRegistrationService
     */
    private $eventRegistrationService;

    /**
     * @param EventRegistrationService $eventRegistrationService
     */
    function __construct(EventRegistrationService $eventRegistrationService)
    {
        $this->eventRegistrationService = $eventRegistrationService;
    }

    /**
     * @param Request $request
     * @param string $slug
     *
     */
    public function create(Request $request, string $slug)
    {
        if (! $request->hasHeader('x-inertia')) {
            return Redirect::route('ticketOffice.show', ['slug' => $slug]);
        }

        $user = null;
        if ($this->isLogged()) {
            if($userAuth = $this->authConsumer()) {
                $user = UserResource::make($userAuth);
            }
        }

        return Inertia::render('TicketsRegistration', [
            'user' => $user,
        ]);
    }

    /**
     * @param Request $request
     * @param string $slug
     *
     */
    public function checkout(Request $request, string $slug)
    {
        if (! $request->hasHeader('x-inertia')) {
            return Redirect::route('ticketOffice.show', ['slug' => $slug]);
        }

        return Inertia::render('Checkout');
    }

    /**
     * @param EventRegistrationRequest $eventRegistrationRequest
     * @param string $slug
     *
     */
    public function store(EventRegistrationRequest $eventRegistrationRequest, string $slug)
    {
        try {
            $registrations = Arr::get($eventRegistrationRequest, 'registrations');
            $buyer = Arr::get($eventRegistrationRequest, 'buyer', []);
            $payment = Arr::get($eventRegistrationRequest, 'payment', []);
            $data = $this->eventRegistrationService->register(
                $registrations,
                $buyer,
                $payment,
                $slug
            );

            return Inertia::render('DebugPayment', [
                'order' => $data['order'],
                'event' => $data['event'],
            ]);
        } catch (TicketNotExistInEventException $ex) {
            dd('Os ingressos que você está tentando comprar possui incompatibilidades - (criar uma página para isso)');
        } catch (TicketSoldOutException $ex) {
            dd('Os ingressos que você está comprando estão esgotados - (criar uma página para isso)');
        } catch (Exception $ex) {
            dd($ex);
        }
    }

    /**
     * @param EventRegistrationRequest $eventRegistrationRequest
     * @param string $slug
     *
     */
    public function storePix(EventRegistrationRequest $eventRegistrationRequest, string $slug)
    {
        try {
            $registrations = Arr::get($eventRegistrationRequest, 'registrations');
            $buyer = Arr::get($eventRegistrationRequest, 'buyer', []);
            $payment = Arr::get($eventRegistrationRequest, 'payment', []);
            $data = $this->eventRegistrationService->register(
                $registrations,
                $buyer,
                $payment,
                $slug
            );

            dd($data);

            return Inertia::render('OrderCompleted', [
                'event' => EventResource::make($data->get('event')),
                'ticketPasses' => TicketPassResource::collection($data->get('ticketPasses'))
            ]);
        } catch (TicketNotExistInEventException $ex) {
            dd('Os ingressos que você está tentando comprar possui incompatibilidades - (criar uma página para isso)');
        } catch (TicketSoldOutException $ex) {
            dd('Os ingressos que você está comprando estão esgotados - (criar uma página para isso)');
        } catch (Exception $ex) {
            dd($ex);
        }
    }
}
