<?php

namespace App\Http\Controllers\WebHook;

use App\Http\Controllers\Controller;
use App\Integrations\SendGrid\SendGridIntegration;
use App\Integrations\SendGrid\Templates\TicketSubmission;
use App\Integrations\WhatsApp\MegaApiIntegration;
use App\Integrations\WhatsApp\PassaporteDigitalNotifications;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class WebHookWhatsappController extends Controller
{
    function __construct()
    {
    }

    public function test(Request $request)
    {
        try {
//            $client = new PassaporteDigitalNotifications();
//            $client->sendMessage( $request->all() );
            $client = new MegaApiIntegration( $request->all() );
            $client->sendMessage();
        } catch (Exception $ex) {
            Log::channel('whatsapp')->error( $ex->getMessage() );
        }
    }

    public function log(Request $request)
    {
        try {
            $filename   = "whatsapp-" . date("Y-m-d") . ".log";
            $content    = file_get_contents( storage_path( 'logs/'. $filename ) );
            return response($content, 200)->header('Content-Type', 'text/plain');
        } catch (Exception $ex) {
            Log::channel('whatsapp')->error( $ex->getMessage() );
            return response($ex->getMessage(), 500)->header('Content-Type', 'text/plain');
        }
    }

    public function clearLog(Request $request)
    {
        try {
            $filename   = "whatsapp-" . date("Y-m-d") . ".log";
            file_put_contents( storage_path( 'logs/'. $filename ), "" );
        } catch (Exception $ex) {
            Log::channel('whatsapp')->error( $ex->getMessage() );
        }
    }

}
