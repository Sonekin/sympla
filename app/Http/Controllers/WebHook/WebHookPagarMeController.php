<?php

namespace App\Http\Controllers\WebHook;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Services\WebHook\WebHookService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebHookPagarMeController extends Controller
{
    /**
     * @var WebHookService
     */
    private $webHookService;

    /**
     * @param WebHook $webHookService
     */
    function __construct(WebHookService $webHookService)
    {
        $this->webHookService = $webHookService;
    }

    public function __invoke(Request $request)
    {
        try {
            $this->webHookService->updateTransaction($request->data, Order::GATEWAY_PAGAR_ME);
            //Log::channel("whatsapp")->warning( json_encode( $request->data ) );
            \Log::info(json_encode($request->data));
        } catch(Exception $ex) {
            \Log::info('ERRO:'.json_encode($ex));
        }
    }
}
