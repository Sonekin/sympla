<?php

namespace App\Http\Controllers\WebHook;

use App\Http\Controllers\Controller;
use App\Integrations\WhatsApp\WhatsAppDispatcher;
use App\Models\Order;
use App\Services\WebHook\WebHookService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebHookContaPayController extends Controller
{
    /**
     * @var WebHookService
     */
    private $webHookService;

    /**
     * @param WebHookService $webHookService
     */
    function __construct(WebHookService $webHookService)
    {
        $this->webHookService = $webHookService;
    }

    public function __invoke(Request $request)
    {
        try {
            $this->webHookService->updateTransaction($request->all(), Order::GATEWAY_CONTA_PAY);
            WhatsAppDispatcher::dispatch( $request->all() );
        } catch(Exception $ex) {
            \Log::info(json_encode($ex));
        }
    }
}
