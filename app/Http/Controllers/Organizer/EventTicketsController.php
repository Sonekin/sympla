<?php

namespace App\Http\Controllers\Organizer;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests\Organizer\TicketRequest;
use App\Http\Resources\Organizer\TicketResource;
use App\Http\Controllers\Controller;
use App\Services\Ticket\TicketService;

use Exception;

class EventTicketsController extends Controller
{
    /**
     * @var TicketService
     */
    private $ticketService;

    /**
     * @param TicketService $ticketService
     */
    function __construct(TicketService $ticketService)
    {
        $this->ticketService = $ticketService;
    }

    /**
     * @param TicketRequest $request
     *
     */
    public function store(TicketRequest $request, int $eventId)
    {
        try {
            $request->merge(['event_id' => $eventId]);
            $ticket = $this->ticketService->storeTicket($request->all());

            return response()->json([
                'ticket' => TicketResource::make($ticket),
            ]);
        } catch (Exception $ex) {
            return response()->json(['error' => true, 'ticket' => null]);
        }
    }

    /**
     * @param TicketRequest $request
     *
     */
    public function update(TicketRequest $request, int $ticketId)
    {
        try {
            $ticket = $this->ticketService->updateTicket($request->all(), $ticketId);

            return response()->json([
                'ticket' => TicketResource::make($ticket),
            ]);
        } catch (NotFoundHttpException $ex) {
            return response()->json([
                'message' => 'Ingresso não pôde ser encontrado.',
                'error' => true,
                'ticket' => null
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'message' => 'Ocorreu um erro inesperado. Verifique as informações enviadas.',
                'error' => true,
                'ticket' => null
            ]);
        }
    }

    /**
     * @return [type]
     */
    public function destroy(int $ticketId)
    {
        try {
            $success = $this->ticketService->deleteTicket($ticketId);
            return response()->json(['success' => $success]);
        } catch (NotFoundHttpException $ex) {
            return response()->json([
                'message' => 'Ingresso não pôde ser encontrado.',
                'error' => true,
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'message' => 'Ocorreu um erro inesperado. Verifique as informações enviadas.',
                'error' => true,
            ]);
        }
    }
}
