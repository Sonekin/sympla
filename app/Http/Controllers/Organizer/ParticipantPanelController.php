<?php

namespace App\Http\Controllers\Organizer;

use App\Http\Resources\Organizer\ParticipantResource;
use App\Http\Resources\Organizer\EventResource;
use App\Http\Controllers\Controller;
use App\Services\Event\EventService;
use App\Services\Participant\ParticipantService;
use App\Http\Requests\Organizer\ParticipantRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Inertia\Inertia;
use Exception;

class ParticipantPanelController extends Controller
{
    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @var ParticipantService
     */
    private $participantService;

    /**
     * @param EventService $eventService
     */
    function __construct(EventService $eventService, ParticipantService $participantService)
    {
        $this->eventService = $eventService;
        $this->participantService = $participantService;
    }

    /**
     * @param int $eventId
     * 
     * @return [type]
     */
    public function index(int $eventId)
    {
        try {
            $event = $this->eventService->getEvent($eventId);
            $ticketTypes = $this->eventService->getEventTicketTypes($event);
            $participants = $this->participantService->getParticipantsByEvent($eventId);

            return Inertia::render('Organizer/Test/ParticipantPanel', [
                'event' => EventResource::make($event),
                'ticketTypes' => $ticketTypes,
                'participants' => ParticipantResource::collection($participants),
            ]);
        } catch (NotFoundHttpException $ex) {
            abort(404);
        } catch (Exception $ex) {
        }
    }

    /**
     * @param ParticipantRequest $request
     *
     * @return [type]
     */
    public function filter(ParticipantRequest $request, int $eventId)
    {
        try {
            $event = $this->eventService->getEvent($eventId);
            $participants = $this->participantService->getFilteredParticipantsByEvent($request->all(), $eventId);

            return response()->json([
                'event' => EventResource::make($event),
                'participants' => ParticipantResource::collection($participants),
            ]);
        } catch (NotFoundHttpException $ex) {
            abort(404);
        } catch (Exception $ex) {
        }
    }
}
