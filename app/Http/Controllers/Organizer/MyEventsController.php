<?php

namespace App\Http\Controllers\Organizer;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests\Organizer\MyEventsFilterRequest;
use App\Http\Requests\Organizer\EventRequest;
use App\Http\Requests\Organizer\EventAddressRequest;
use App\Http\Resources\Organizer\EventResource;
use App\Http\Resources\Organizer\EventAddressResource;
use App\Http\Controllers\Controller;
use App\Services\Event\EventService;

use Inertia\Inertia;
use Exception;

class MyEventsController extends Controller
{
    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @param EventService $eventService
     */
    function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * @return [type]
     */
    public function index()
    {
        try {
            $organizer = auth()->user()->capacityOrganizer->capacity;
            $events = $this->eventService->getEventsSoldTickets(
                $this->eventService->getEventsByCompanies($organizer->companies)
            );

            return Inertia::render('Organizer/Test/MyEvents', [
                'events' => EventResource::collection($events),
            ]);
        } catch (Exception $ex) {
        }
    }

    /**
     * @return [type]
     */
    public function create()
    {
        try {
            $organizer = auth()->user()->capacityOrganizer->capacity;
            $eventAddressList = $this->eventService->getEventAddressListByCompany($organizer->main_company->id ?? null);
            $subjectList = $this->eventService->getEventSubjectList();
            $categoryList = $this->eventService->getEventCategoryList();

            return Inertia::render('Organizer/Test/CreateEvent', [
                'eventAddressList' => EventAddressResource::collection($eventAddressList),
                'subjectList' => $subjectList,
                'categoryList' => $categoryList,
            ]);
        } catch (Exception $ex) {
        }
    }

    /**
     * @return [type]
     */
    public function edit(int $eventId)
    {
        try {
            $event = $this->eventService->getEvent($eventId);

            $organizer = auth()->user()->capacityOrganizer->capacity;
            $eventAddresses = $this->eventService->getEventAddressListByCompany($organizer->main_company->id ?? null);
            $subjectList = $this->eventService->getEventSubjectList();
            $categoryList = $this->eventService->getEventCategoryList();

            return Inertia::render('Organizer/Test/EditEvent', [
                'event' => EventResource::make($event),
                'eventAddress' => EventAddressResource::make($event->address),
                'eventAddressList' => EventAddressResource::collection($eventAddresses),
                'subjectList' => $subjectList,
                'categoryList' => $categoryList,
            ]);
        } catch (NotFoundHttpException $ex) {
            abort(404);
        } catch (Exception $ex) {
        }
    }

    /**
     * @param MyEventsFilterRequest $request
     *
     * @return [type]
     */
    public function filter(MyEventsFilterRequest $request)
    {
        try {
            $organizer = auth()->user()->capacityOrganizer->capacity;
            $events = $this->eventService->getEventsSoldTickets(
                $this->eventService->getFilteredEventsByCompanies($request->all(), $organizer->companies)
            );

            return response()->json([
                'events' => EventResource::collection($events),
            ]);
        } catch (Exception $ex) {
        }
    }

    /**
     * @param EventRequest $request
     *
     */
    public function store(EventRequest $request)
    {
        try {
            $organizer = auth()->user()->capacityOrganizer->capacity;
            $request->merge(['company_id' => $organizer->main_company->id ?? null]);
            $event = $this->eventService->storeEvent($request->all());

            return response()->json([
                'event' => EventResource::make($event),
                'eventAddress' => EventAddressResource::make($event->address)
            ]);
        } catch (Exception $ex) {
            return response()->json(['error' => true, 'event' => null]);
        }
    }

    /**
     * @param EventRequest $request
     *
     */
    public function update(EventRequest $request, int $eventId)
    {
        try {
            $organizer = auth()->user()->capacityOrganizer->capacity;
            $request->merge(['company_id' => $organizer->main_company->id ?? null]);
            $event = $this->eventService->updateEvent($request->all(), $eventId);

            return response()->json([
                'event' => EventResource::make($event),
                'eventAddress' => EventAddressResource::make($event->address)
            ]);
        } catch (NotFoundHttpException $ex) {
            return response()->json([
                'message' => 'Evento não pôde ser encontrado.',
                'error' => true,
                'event' => null
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'message' => 'Ocorreu um erro inesperado. Verifique as informações enviadas.',
                'error' => true,
                'event' => null
            ]);
        }
    }

    /**
     * @param EventAddressRequest $request
     * @param int $companyId
     *
     */
    public function storeEventAddress(EventAddressRequest $request, int $companyId)
    {
        try {
            $request->merge(['company_id' => $companyId]);
            $eventAddress = $this->eventService->storeEventAddress($request->all());
            return response()->json(['eventAddress' => EventAddressResource::make($eventAddress)]);
        } catch (Exception $ex) {
            return response()->json(['error' => true, 'eventAddress' => null]);
        }
    }

    /**
     * @return [type]
     */
    public function destroy(int $eventId)
    {
        try {
            $success = $this->eventService->deleteEvent($eventId);
            return response()->json(['success' => $success]);
        } catch (NotFoundHttpException $ex) {
            return response()->json([
                'message' => 'Evento não pôde ser encontrado.',
                'error' => true,
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'message' => 'Ocorreu um erro inesperado. Verifique as informações enviadas.',
                'error' => true,
            ]);
        }
    }
}
