<?php

namespace App\Http\Controllers\Organizer;

use App\Http\Resources\Organizer\EventResource;
use App\Http\Controllers\Controller;
use App\Services\Event\EventService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Inertia\Inertia;
use Exception;

class EventPanelController extends Controller
{
    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @param EventService $eventService
     */
    function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * @param int $eventId
     * 
     * @return [type]
     */
    public function index(int $eventId)
    {
        try {
            $event = $this->eventService->getEventSoldTickets(
                $this->eventService->getEvent($eventId)
            );

            $ticketTypes = $this->eventService->getEventTicketTypes($event);
            $soldTickets = $this->eventService->getEventSoldTicketsPerDate($event);

            return Inertia::render('Organizer/Test/EventPanel', [
                'event' => EventResource::make($event),
                'ticketTypes' => $ticketTypes,
                'soldTickets' => $soldTickets,
            ]);
        } catch (NotFoundHttpException $ex) {
            abort(404);
        } catch (Exception $ex) {
        }
    }
}
