<?php

namespace App\Http\Controllers\Organizer;

use App\Http\Controllers\Controller;
use App\Services\Checkin\CheckinService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Http\Request;

use Inertia\Inertia;
use Exception;

class CheckInController extends Controller
{
    /**
     * @var CheckInService
     */
    private $checkinService;

    /**
     * @param CheckinService $checkinService
     */
    function __construct(CheckinService $checkinService)
    {
        $this->checkinService = $checkinService;
    }

    /**
     * @param int $eventId
     * 
     * @return [type]
     */
    public function check(Request $request, $eventId)
    {
        $body = $request->all();
        $user = auth()->user();
        if(!isset($body['code'])){
            return response()->json([
                'message' => 'Missing code'
            ], 400);
        }
        if(!isset($eventId)){
            return response()->json([
                'message' => 'Missing event id'
            ], 400);
        }
        $code = $body['code'];
        try {
            $this->checkinService->storeCheckin([
                    'checker_id' =>  $user->id,
                    'code' => $code,
                    'event_id' => $eventId,
            ]);
        } catch (Exception $ex) {
           return response()->json([
                'message' => $ex->getMessage(),
           ], 400);
        }
        return response()->json(null);   
    }
    
    public function report(Request $request, int $eventId)
    {
        $user = auth()->user();
        $output = $this->checkinService->report([
                'checker_id' =>  $user->id,
                'event_id' => $eventId,
        ]);
        return response()->json($output);   
    }
}

