<?php

namespace App\Http\Controllers\Organizer;

use App\Http\Requests\Organizer\TicketFilterRequest;
use App\Http\Resources\Organizer\EventResource;
use App\Http\Resources\Organizer\TicketResource;
use App\Http\Controllers\Controller;
use App\Services\Event\EventService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Inertia\Inertia;
use Exception;

class TicketPanelController extends Controller
{
    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @param EventService $eventService
     */
    function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * @param int $eventId
     * 
     * @return [type]
     */
    public function index(int $eventId)
    {
        try {
            $event = $this->eventService->getEventSoldTickets(
                $this->eventService->getEvent($eventId)
            );

            $tickets = $this->eventService->getTicketsWithQuantityByEvent($event);
            $ticketTypes = $this->eventService->getEventTicketTypes($event);
            $soldTickets = $this->eventService->getEventSoldTicketsPerDate($event);

            return Inertia::render('Organizer/Test/TicketPanel', [
                'event' => EventResource::make($event),
                'tickets' => TicketResource::collection($tickets),
                'ticketTypes' => $ticketTypes,
                'soldTickets' => $soldTickets,
            ]);
        } catch (NotFoundHttpException $ex) {
            abort(404);
        } catch (Exception $ex) {
        }
    }
    
    /**
     * @param TicketFilterRequest $request
     *
     * @return [type]
     */
    public function filter(TicketFilterRequest $request, int $eventId)
    {
        try {
            $event = $this->eventService->getEvent($eventId);
            $soldTickets = $this->eventService->getFilteredEventSoldTicketsPerDate($request->all(), $event);

            return response()->json([
                'soldTickets' => $soldTickets,
            ]);
        } catch (NotFoundHttpException $ex) {
            abort(404);
        } catch (Exception $ex) {
        }
    }
}
