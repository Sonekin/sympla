<?php

namespace App\Http\Controllers\ConsumerArea;

use App\Http\Resources\ConsumerArea\ConsumerResource;
use App\Http\Resources\ConsumerArea\OrderResource;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Http\Requests\ConsumerArea\MyProfileRequest;
use App\Services\Consumer\ProfileService;

use Inertia\Inertia;
use Exception;

class MyProfileController extends Controller
{
    /**
     * @var ProfileService
     */
    private $profileService;

    /**
     * @param ProfileService $profileService
     */
    function __construct(ProfileService $profileService)
    {
        $this->profileService = $profileService;
    }

    /**
     * @return [type]
     */
    public function index()
    {
        try {
            $user = auth()->user();

            return Inertia::render('MyProfile', [
                'consumer' => ConsumerResource::make($user),
            ]);
        } catch (NotFoundHttpException $ex) {
            abort(404);
        } catch (Exception $ex) {
        }
    }

    /**
     * @return [type]
     */
    public function orders()
    {
        try {
            $consumer = auth()->user()->capacityConsumer->capacity;
            $orders = $this->profileService->getOrdersByConsumer($consumer);

            return Inertia::render('Orders/MyOrders', [
                'orders' => OrderResource::collection($orders),
            ]);
        } catch (NotFoundHttpException $ex) {
            abort(404);
        } catch (Exception $ex) {
        }
    }
    
    /**
     * @param MyProfileRequest $request
     *
     */
    public function update(MyProfileRequest $request)
    {
        try {
            $user = $this->profileService->updateLoggedUser($request->all());

            return response()->json([
                'consumer' => ConsumerResource::make($user),
            ]);
        } catch (NotFoundHttpException $ex) {
            return response()->json([
                'message' => 'Usuário logado não pôde ser encontrado.',
                'error' => true,
                'consumer' => null
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'message' => 'Ocorreu um erro inesperado. Verifique as informações enviadas.',
                'error' => true,
                'consumer' => null
            ]);
        }
    }
}