<?php

namespace App\Http\Controllers;

use App\Services\TicketOffice\TicketOfficeService;
use Illuminate\Http\Request;
use Inertia\Inertia;

class TicketOfficeController extends Controller
{
    /**
     * @var TicketOfficeService
     */
    private $ticketOfficeService;

    /**
     * @param TicketOfficeService $ticketOfficeService
     */
    function __construct(TicketOfficeService $ticketOfficeService)
    {
        $this->ticketOfficeService = $ticketOfficeService;
    }


    /**
     * @param Request $request
     * @param string $slug
     *
     */
    public function __invoke(Request $request, string $slug)
    {
        $event = $this->ticketOfficeService->getInfoEvent($slug);
        return Inertia::render('Event', compact('event'));
    }
}
