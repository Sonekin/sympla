<?php

namespace App\Http\Controllers\App;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Http\Controllers\Controller;
use App\Services\Ticket\TicketService;
use App\Models\Ticket;
use App\Models\TicketPass;
use App\Models\TicketPassHolder;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\Event;
use SimpleSoftwareIO\QrCode\Facades\QrCode as FacadeQrCode;

use Exception;

class UserTicketsController extends Controller
{
    /**
     * @var TicketService
     */
    private $ticketService;

    /**
     * @param TicketService $ticketService
     */
    function __construct(TicketService $ticketService)
    {
        $this->ticketService = $ticketService;
    }

    /**
     * @param TicketRequest $request
     *
     */
    public function index($eventId)
    {
        $ticket_pass = TicketPass::where('code', $eventId)->first();
        if(!$ticket_pass)
        {
            //redirect 404
            die('NAO LOCALIZADO');
        }

        $ticket = Ticket::find($ticket_pass->ticket_id);
        $ticket_holder = TicketPassHolder::where('ticket_pass_id', $ticket_pass->id)->first();
        $order = Order::find($ticket_pass->order_id);
        $transaction = Transaction::find($ticket_pass->order_id);
        $evento = Event::find($ticket->event_id);

        //$qrcode = $this->generateQrCode($eventId);
        $qrcode = FacadeQrCode::backgroundColor(0,0,0,0)->size(250)->generate($eventId)->toHtml();

        //dump($ticket_pass);die();
        //dump($ticket_holder);
        //die();

		return view('app.tickets.index', compact('ticket', 'ticket_pass', 'order', 'ticket_holder', 'transaction', 'qrcode', 'evento'));
    }
}
