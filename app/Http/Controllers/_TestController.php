<?php

namespace App\Http\Controllers;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class _TestController extends Controller
{
    private string $email;

    private string $token;

    private string $baseUrl;

    public PendingRequest $http;

    function __construct()
    {
        $accessToken = $this->generateAccessToken();
        $this->http = Http::acceptJson()->baseUrl("{$this->baseUrl}/v2")->withToken($accessToken);
    }

    /**
     * @param Request $request
     *
     */
    public function paydocument(Request $request)
    {
        dd($this->http
            ->get("/pay_document/{$request->document_number}")
            ->throw()
            ->json());
    }


    private function generateAccessToken()
    {
        $this->email = config('services.contapay.email');

        $this->token = config('services.contapay.token');

        $this->baseUrl = config('services.contapay.base_url');

        return Http::post("{$this->baseUrl}/v2/authenticate", [
            'email' => $this->email,
            'token' => $this->token,
        ])->json('access_token');
    }
}
