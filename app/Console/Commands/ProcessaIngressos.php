<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\UsersImport;
use App\Services\EventRegistration\EventRegistrationService;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Collection;
use App\Traits\Qrcode;
use App\Models\Order;
use App\Models\Ticket;
use App\Models\TicketPass;
use App\Models\TicketPassHolder;
use App\Models\User;
use App\Models\UserCapabilities;
use App\Models\Consumer;
use App\Models\Transaction;
use App\Models\Event;
use App\Models\Imports;


class ProcessaIngressos extends Command
{
    private $eventRegistrationService;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'processa-ingressos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Processa Ingressos adquiridos.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    function __construct(EventRegistrationService $eventRegistrationService)
    {
        parent::__construct();
        $this->eventRegistrationService = $eventRegistrationService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //$rows = Excel::import(new UsersImport, storage_path('excel/teste.xlsx'));
        //$rows = Excel::toArray(new \stdClass(), storage_path('excel/clientes.xlsx'));

        $rows = Excel::toCollection(new UsersImport, storage_path('excel/imports.xlsx'))[0];
        $this->processaImport($rows);

        $rows = Imports::where('status', 0)->get();
        if(sizeof($rows) <= 0) die('NENHUM REGISTRO PARA PROCESSAR');

        //dump($rows);die();
        foreach($rows as $key => $row)
        {
            $nome = $row->nome;
            $cpf = $row->cpf;
            $email = $row->email;
            $telefone = $row->telefone;
            $ingresso = $row->ingresso;
            $valor = $row->valor;
            //dump($ingresso);die();


            //DB::statement('SET FOREIGN_KEY_CHECKS=0;');

            //$_user = User::where('email', $email)->first();
            //$_user->delete();
            //$_user = Consumer::where('id', 4)->first();
            //$_user->delete();
            //$_user = UserCapabilities::where('user_id', 48)->first();
            //$_user->delete();

            //CONSULTA TICKET
            $ticket = Ticket::where('event_id', 2)->where('description', 'like', '%'.$ingresso.'%')->first();
            //$ticket = Ticket::where('event_id', 1)->where('description', '1º lote | Inteira')->first();
            \Log::info(json_encode($ticket));

            if(!$ticket) continue;

            DB::beginTransaction();
            try 
            {
                //VERIFICA SE JA EXISTE USUARIO COM EMAIL CADASTRADO
                $user = User::where('email', $email)->orWhere('phone', $telefone)->first();

                if(!$user)
                {

                    //CRIA USER
                    $user = new User;
    
                    $user->name = $nome;
                    $user->email = $email;
                    $user->phone = $telefone;
                    $user->cpf = $cpf;
                    $user->password = Hash::make('cpf');
    
                    $user->save();

                }
                \Log::info(json_encode($user));


                //CRIA CONSUMER
                $consumer = new Consumer;
                $consumer->save();
                \Log::info(json_encode($consumer));


                //$capabilities = UserCapabilities::where('user_id', $user->id)->first();
                //CRIA USER CAPABILITIES
                $capabilities = new UserCapabilities;
                $capabilities->user_id = $user->id;
                $capabilities->capacity_id = $consumer->id;
                $capabilities->capacity_type = 'App\Models\Consumer';

                $capabilities->save();
                \Log::info(json_encode($capabilities));

                //CRIA ORDER
                $order = new Order;
                $order->consumer_id = $consumer->id;
                $order->amount = $valor;
                $order->gateway_billing_id = null;
                $order->payment_method = Order::INVOICE;
                $order->installments = 1;
                $order->gateway = Order::GATEWAY_CONTA_PAY;

                $order->save();
                \Log::info(json_encode($order));
                //dd($order);

                //CRIA TRANSACAO
                $transacao = new Transaction;
                $transacao->order_id = $order->id;
                $transacao->value = $valor;
                $transacao->due_date = null;
                $transacao->payment_date = null;//isset($row->data_compra) ? date('Y-m-d', strtotime($row->data_compra)) : null;
                $transacao->gateway_transaction_id = 0;
                $transacao->status = Transaction::PAID;

                $transacao->save();
                \Log::info(json_encode($transacao));
                //dd($transacao);

                //PUXA O EVENTO
                //$evento = Event::find($ticket->event_id);
                

                //GERAR CODIGO UNICO DO INGRESSO
                $random = random_bytes(4);
                $codigo = $ticket->event_id . bin2hex($random);


                //CRIA TICKET PASS
                $ticket_pass = new TicketPass;
                $ticket_pass->ticket_id = $ticket->id;
                $ticket_pass->order_id = $order->id;
                $ticket_pass->code = $codigo;

                $ticket_pass->save();
                \Log::info(json_encode($ticket_pass));
                //dd($ticket);

                //CRIA TICKET PASS HOLDER
                $ticket_holder = new TicketPassHolder();
                $ticket_holder->ticket_pass_id = $ticket_pass->id;
                $ticket_holder->name = $nome;
                $ticket_holder->cpf = $cpf;
                $ticket_holder->email = $email;
                $ticket_holder->phone = $telefone;

                $ticket_holder->save();
                \Log::info(json_encode($ticket_holder));
                //dd($ticket_holder);

                //CONCILIA USUARIO
                $row->status = 1;
                $row->save();

                DB::commit();

            }
            catch(\Exception $e)
            {
                DB::rollback();

                dump($e->getMessage());die();
            }


            //die('OK');
        }

        //dump($rows);die();
        die('FINALIZA');
    }

    //IMPORTA DADOS DA PLANILHA SALVA NO BANCO
    private function processaImport($rows)
    {

        foreach($rows as $key => $row)
        {
            $holder = Imports::where('cpf', trim($row[1]))->first();
            if($holder) continue;

            $import = new Imports;
            $import->nome = $row[0];
            $import->cpf = $row[1];
            $import->email = $row[2];
            $import->telefone = $row[3];
            $import->data_compra = isset($row[4]) ? date('Y-m-d', strtotime(trim($row[4]))) : null;
            $import->ingresso = $row[5];
            $import->valor = (float)$row[7];

            $import->save();
        }

        die('FIM');
    }
}
