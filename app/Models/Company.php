<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'phone',
        'cnpj',
    ];

    /**
     * The organizers that belong to the company.
     */
    public function organizers()
    {
        return $this->belongsToMany(Organizer::class, 'company_collabolators');
    }

    /**
     * Get the events for the blog company.
     */
    public function events()
    {
        return $this->hasMany(Event::class);
    }
}
