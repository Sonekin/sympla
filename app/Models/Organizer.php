<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organizer extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'organizers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [];

    /**
    * Get the capacity info
    */
    public function userCapacity()
    {
        return $this->morphOne(UserCapabilities::class, 'capacity');
    }

    /**
    * The companies that belong to the organizer.
    */
    public function companies()
    {
        return $this->belongsToMany(Company::class, 'company_collabolators');
    }

    /**
    * The main company that belongs to the organizer.
    */
    public function getMainCompanyAttribute()
    {
        return $this->companies->first();
    }
}
