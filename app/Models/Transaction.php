<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    public const CHARGEDBACK = 'chargedback';
    public const CREATED = 'created';
    public const OVERPAID = 'overpaid';
    public const PARTIAL_CANCELED = 'partial_canceled';
    public const PAYMENT_FAILED = 'payment_failed';
    public const PROCESSING = 'processing';
    public const REFUNDED = 'refunded';
    public const UNDERPAID = 'underpaid';
    public const UPDATED = 'updated';
    public const PAID = 'paid';
    public const PENDING = 'pending';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'order_id',
        'value',
        'due_date',
        'payment_date',
        'gateway_transaction_id',
        'status',
    ];

    /**
     * Get the order that owns the Transaction.
     *
     * @return mixed
     */
    public function Order(): mixed
    {
        return $this->belongsTo(Order::class);
    }
}
