<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventAddress extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_addresses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'company_id',
        'name',
        'street',
        'number',
        'complement',
        'district',
        'postal_code',
        'city',
        'state',
    ];

    /**
     * Get the company that owns the event address.
     *
     * @return mixed
     */
    public function company(): mixed
    {
        return $this->belongsTo(Company::class);
    }
}
