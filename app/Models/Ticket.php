<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use SoftDeletes;

    public const ACTIVE = 1;
    public const DISABLED = 0;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tickets';

    private const AVAILABILITY_LIST = [
        1 => 'Para todo o público',
        2 => 'Convites pessoais por e-mail',
        3 => 'Link único pra compartilhar',
        4 => 'Para ser adicionado manualmente',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'event_id',
        'description',
        'detailed_description',
        'price',
        'fee',
        'quantity',
        'status',
        'sold_out',
        'availability',
        'min_purchase_quantity',
        'max_purchase_quantity',
        'start_sales',
        'end_sales',
    ];

    protected $appends = array('isExpired', 'availability');

    public static function getAvailabilityList(): array
    {
        return self::AVAILABILITY_LIST;
    }

    public static function getDefaultFee(): float
    {
        return 2.5;
    }

    public function getAvailabilityAttribute()
    {
        return !empty($this->getRawOriginal('availability')) ? self::AVAILABILITY_LIST[$this->getRawOriginal('availability')] : null;
    }

    /**
     * Get the event that owns the ticket.
     *
     * @return mixed
     */
    public function event(): mixed
    {
        return $this->belongsTo(Event::class);
    }

    public function getIsExpiredAttribute()
    {
        $now = Carbon::now();
        return ! $now->between(Carbon::createFromDate($this->start_sales), Carbon::createFromDate($this->end_sales), true);
    }
}
