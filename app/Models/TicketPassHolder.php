<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketPassHolder extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ticket_pass_holders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'ticket_pass_id',
        'name',
        'email',
        'phone',
    ];


    /**
     * Get the ticketPass.
     */
    public function ticketPass()
    {
        return $this->belongsTo(TicketPass::class);
    }
}
