<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'events';

    private const SUBJECT_LIST = [
        1 => 'Acadêmico e científico',
        2 => 'Artesanato',
        3 => 'Casa e estilo de vida',
        4 => 'Cinema, fotografia',
        5 => 'Desenvolvimento pessoal',
        6 => 'Design, métricas e produtos digitais',
        7 => 'Direito e legislação',
        8 => 'Empreendedorismo, negócios e inovação',
        9 => 'Esportes',
        10 => 'Games e geek',
        11 => 'Gastronomia, comidas e bebidas',
        12 => 'Governo e política',
        13 => 'Informática, tecnologia e programação',
        14 => 'Marketing e vendas',
        15 => 'Moda e beleza',
        16 => 'Música',
        17 => 'Outro',
        18 => 'Religião, espiritualidade',
        19 => 'Saúde, nutrição e bem-estar',
        20 => 'Sociedade e cultura',
        21 => 'Teatro, stand up e dança',
    ];

    private const CATEGORY_LIST = [
        1 => 'Competição ou torneio',
        2 => 'Corrida',
        3 => 'Curso, aula, treinamento ou workshop',
        4 => 'Drive-in',
        5 => 'Espetáculos',
        6 => 'Feira, festival ou exposição',
        7 => 'Festa, festival ou show',
        8 => 'Meetup ou evento de networking',
        9 => 'Missa ou culto',
        10 => 'Outro',
        11 => 'Palestra, congresso ou seminário',
        12 => 'Passeios, excursões ou tour',
        13 => 'Retiro ou acampamento',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'company_id',
        'address_id',
        'name',
        'slug',
        'event_status',
        'subject',
        'category',
        'city',
        'state',
        'description',
        'date',
        'end_date',
        'flyer_image',
        'visible'
    ];

    protected $appends = array('subject', 'category');

    public static function getSubjectList(): array
    {
        return self::SUBJECT_LIST;
    }

    public static function getCategoryList(): array
    {
        return self::CATEGORY_LIST;
    }

    public function getSubjectAttribute()
    {
        return !empty($this->getRawOriginal('subject')) ? self::SUBJECT_LIST[$this->getRawOriginal('subject')] : null;
    }

    public function getCategoryAttribute()
    {
        return !empty($this->getRawOriginal('category')) ? self::CATEGORY_LIST[$this->getRawOriginal('category')] : null;
    }

    /**
     * Get the address of the event.
     */
    public function address()
    {
        return $this->belongsTo(EventAddress::class, 'address_id');
    }

    /**
     * Get the company that owns the event.
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return mixed
     */
    public function tickets(): mixed
    {
        return $this->hasMany(Ticket::class);
    }
}
