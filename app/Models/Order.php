<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    public const PAYMENT_BOOK = 'payment_book';
    public const INVOICE = 'invoice';
    public const CREDIT_CARD = 'credit_card';
    public const PIX = 'pix';

    public const GATEWAY_CONTA_PAY = 'contapay';
    public const GATEWAY_PAGAR_ME = 'pagarme';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'consumer_id',
        'amount',
        'gateway_billing_id',
        'payment_method',
        'installments',
        'gateway',
    ];

    /**
     * Get the consumer that owns the order.
     *
     * @return mixed
     */
    public function consumer(): mixed
    {
        return $this->belongsTo(Consumer::class);
    }

     /**
     * Get the transactions for the order.
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
