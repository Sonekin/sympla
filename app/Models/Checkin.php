<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Checkin extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'checkin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'checker_id',
        'ticket_pass_id',
        'method',
    ];

    /**
     * Get the consumer that owns the order.
     *
     * @return mixed
     */
    public function checker(): mixed
    {
        return $this->belongsTo(User::class);
    }

    public function ticket_pass(): mixed
    {
        return $this->belongsTo(TicketPass::class);
    }
}
