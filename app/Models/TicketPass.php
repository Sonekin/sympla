<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketPass extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ticket_passes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'ticket_id',
        'order_id',
        'ticket_holder_id',
        'code',
        'qrcode',
    ];

    public function ticket()
    {
        return $this->belongsTo(ticket::class);
    }

    /**
     * Get the participant associated with the ticket.
     */
    public function holder()
    {
        return $this->hasOne(TicketPassHolder::class);
    }

    public function buyer()
    {
        return $this->belongsTo(Consumer::class, 'buyer_id', 'id');
    }
}
