<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConsumerGateway extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'consumer_gateways';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'consumer_id',
        'consumer_gateway_id',
        'gateway_identifier',
    ];

    /**
     * Get the consumer that owns the order.
     *
     * @return mixed
     */
    public function consumer(): mixed
    {
        return $this->belongsTo(Consumer::class);
    }
}
