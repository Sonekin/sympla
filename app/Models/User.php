<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'cpf',
        'password',
        'has_access',
        'zip_code',
        'street',
        'number',
        'complement',
        'district',
        'city',
        'state',
        'uf',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Gets the capabilities that the user can perform
     *
     */
    public function capabilities()
    {
        return $this->hasMany(UserCapabilities::class);
    }

    /**
     * Get the consumer capacity
     *
     */
    public function capacityConsumer()
    {
        return $this->hasOne(UserCapabilities::class)
            ->where('capacity_type', Consumer::class)
            ->with('capacity');
    }

    /**
     * Get the organizer capacity
     *
     */
    public function capacityOrganizer()
    {
        return $this->hasOne(UserCapabilities::class)
            ->where('capacity_type', Organizer::class)
            ->with('capacity');
    }
}
