<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCapabilities extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_capabilities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'capacity_id',
    ];

    /**
     * Get the parent UserCapabilities model - Consumer, Admin, Organizer, Partner.
     */
    public function capacity()
    {
        return $this->morphTo();
    }

    /**
    * Get the user that owns the capacity.
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
