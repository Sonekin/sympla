<?php

namespace App\Repositories\User\Contracts;

use App\Models\Consumer;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface UserRepositoryContract
{

    /**
     * create a new user
     *
     * @param array $data
     *
     * @return User
     */
    public function store(array $data): User;

    /**
     * Links a consumer to their user data
     *
     * @param User $user
     * @param Consumer $consumer
     *
     * @return User
     */
    public function attachConsumer(User $user, Consumer $consumer): User;

    /**
     * find a User
     *
     * @param  string  $value
     * @param  string  $by
     *
     * @return User|null
     */
    public function get(string $value, string $by = 'id'): ?User;

    /**
     * Find a User with consumer
     *
     * @param  string  $value
     * @param  string  $by
     *
     * @return User|null
     */
    public function getWithConsumer(string $value, string $by = 'id'): ?User;

    /**
     * Update the data of a user
     *
     * @param array $data
     * @param int|null $userId
     * @param User|null $user
     *
     * @return User
     */
    public function update(array $data, ?int $userId = null, ?User $user = null): User;

    /**
     * Get the user capabilities
     *
     * @param int $user_id
     *
     * @return User
     */
    public function getCapabilities(int $user_id): Collection;

    /**
     * Searchs list of orders belonging to the consumer
     *
     * @param Consumer $consumer
     *
     * @return Collection
     */
    public function getOrdersByConsumer(Consumer $consumer): Collection;
}
