<?php

namespace App\Repositories\User;

use App\Models\Consumer;
use App\Models\Order;
use App\Models\User;
use App\Models\UserCapabilities;
use App\Repositories\User\Contracts\UserRepositoryContract;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class UserRepository implements UserRepositoryContract
{
    private $model;

    /**
     * @param User $model
     */
    function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Create a new user
     *
     * @param array $data
     *
     * @return User
     */
    public function store(array $data): User
    {
        return $this->model->create($data);
    }

    /**
     * Links a consumer to their user data
     *
     * @param User $user
     * @param Consumer $consumer
     *
     * @return User
     */
    public function attachConsumer(User $user, Consumer $consumer): User
    {
        $userCapacity = new UserCapabilities([
            'user_id' => $user->id
        ]);

        $consumer->userCapacity()->save($userCapacity);
        return $user->loadMissing('capacityConsumer');
    }

    /**
     * base Query
     *
     * @param  string  $value
     * @param  string  $by
     *
     * @return Builder
     */
    private function getQuery(string $value, string $by = 'id'): Builder
    {
        return $this->model->where($by, $value);
    }

    /**
     * Find a User
     *
     * @param string $value
     * @param string $by
     *
     * @return User|null
     */
    public function get(string $value, string $by = 'id'): ?User
    {
        return $this->getQuery($value, $by)->first();
    }

    /**
     * Find a User with consumer
     *
     * @param string $value
     * @param string $by
     *
     * @return User|null
     */
    public function getWithConsumer(string $value, string $by = 'id'): ?User
    {
        return $this->getQuery($value, $by)->with('capacityConsumer')->where($by, $value)->first();
    }

    /**
     * Update the data of a user
     *
     * @param array $data
     * @param int|null $userId
     * @param User|null $user
     *
     * @return User
     */
    public function update(array $data, ?int $userId = null, ?User $user = null): User
    {
        if (! $user && $userId) {
            $user = $this->get((string) $userId);
        }

        if (! $user){
            throw new Exception('Unable to retrieve user');
        }

        $user->update($data);
        return $user;
    }

    /**
     * Get the user capabilities
     *
     * @param int $user_id
     *
     * @return User
     */
    public function getCapabilities(int $userId): Collection
    {
        return $this->model->find($userId)->capabilities()->get();
    }

    /**
     * Searchs list of orders belonging to the consumer
     *
     * @param Consumer $consumer
     *
     * @return Collection
     */
    public function getOrdersByConsumer(Consumer $consumer): Collection {
        return Order::selectRaw(
            'orders.id as id, orders.created_at as created_at, events.name as event_name, events.city as city'
        )
        ->leftJoin('ticket_passes', 'ticket_passes.order_id', 'orders.id')
        ->leftJoin('tickets', 'tickets.id', 'ticket_passes.ticket_id')
        ->leftJoin('events', 'events.id', 'tickets.event_id')
        ->where('consumer_id', $consumer->id)->get();
    }
}
