<?php

namespace App\Repositories\Consumer;

use App\Models\ConsumerGateway;
use App\Repositories\Consumer\Contracts\ConsumerGatewayRepositoryContract;

class ConsumerGatewayRepository implements ConsumerGatewayRepositoryContract
{
    /**
     * @var ConsumerGateway
     */
    private $model;

    function __construct(ConsumerGateway $model)
    {
        $this->model = $model;
    }

    /**
     * Create a new ConsumerGateway
     *
     * @param array $data
     *
     * @return ConsumerGateway
     */
    public function store(array $data): ConsumerGateway
    {
        return $this->model->create($data);
    }

    /**
     * Get a ConsumerGateway
     *
     * @param int $consumerId
     * @param string $gatewayIdentifier
     *
     * @return ConsumerGateway|null
     */
    public function get(int $consumerId, string $gatewayIdentifier): ?ConsumerGateway
    {
        return $this->model
            ->where('consumer_id', $consumerId)
            ->where('gateway_identifier', $gatewayIdentifier)
            ->first();
    }
}
