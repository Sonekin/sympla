<?php

namespace App\Repositories\Consumer;

use App\Models\Consumer;
use App\Repositories\Consumer\Contracts\ConsumerRepositoryContract;

class ConsumerRepository implements ConsumerRepositoryContract
{
    /**
     * @var Consumer
     */
    private $model;

    function __construct(Consumer $model)
    {
        $this->model = $model;
    }

    /**
     * Create a new consumer and a new user, if necessary
     *
     * @param array $data
     *
     * @return Consumer
     */
    public function store(array $data): Consumer
    {
        return $this->model->create($data);
    }

    /**
     * Get a consumer with user
     *
     * @param int $consumerId
     *
     * @return Consumer
     */
    public function get(int $consumerId): Consumer
    {
        return $this->model->with('userCapacity.user')->where('id', $consumerId)->first();
    }

    /**
     * Update the datas of a consumer
     *
     * @param int $consumerId
     * @param array $data
     *
     * @return Consumer
     */
    public function update(int $consumerId, array $data): Consumer
    {
        $consumer = $this->get($consumerId);
        $consumer->update($data);
        return $consumer;
    }
}
