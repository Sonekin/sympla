<?php

namespace App\Repositories\Consumer\Contracts;

use App\Models\Consumer;

interface ConsumerRepositoryContract
{

    /**
     * Create a new consumer and a new user, if necessary
     *
     * @param array $data
     *
     * @return Consumer
     */
    public function store(array $data): Consumer;

    /**
     * Get a consumer with user
     *
     * @param int $consumerId
     *
     * @return Consumer
     */
    public function get(int $consumerId): Consumer;

    /**
     * Update the data of a consumer
     *
     * @param int $consumerId
     * @param array $data
     *
     * @return Consumer
     */
    public function update(int $consumerId, array $data): Consumer;
}
