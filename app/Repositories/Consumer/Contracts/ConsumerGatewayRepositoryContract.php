<?php

namespace App\Repositories\Consumer\Contracts;

use App\Models\ConsumerGateway;

interface ConsumerGatewayRepositoryContract
{

    /**
     * Create a new consumerGateway
     *
     * @param array $data
     *
     * @return ConsumerGateway
     */
    public function store(array $data): ConsumerGateway;

    /**
     * Get a ConsumerGateway
     *
     * @param int $consumerId
     * @param string $gatewayIdentifier
     *
     * @return ConsumerGateway|null
     */
    public function get(int $consumerId, string $gatewayIdentifier): ?ConsumerGateway;
}
