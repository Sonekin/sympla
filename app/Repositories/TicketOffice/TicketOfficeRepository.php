<?php

namespace App\Repositories\TicketOffice;

use App\Models\Event;
use App\Models\Ticket;
use App\Repositories\TicketOffice\Contracts\TicketOfficeRepositoryContract;
use Carbon\Carbon;

class TicketOfficeRepository implements TicketOfficeRepositoryContract
{
    /**
     * Get event, organizer and ticket information by event id
     *
     * @param string $slug
     *
     * @return Event
     */
    public function getEventWithTickets(string $slug): Event
    {
        return
            Event::where('slug', $slug)
                ->with(['company', 'address', 'tickets' => function($query) {
                    $query
                        ->where('status', Ticket::ACTIVE)
                        ->where('start_sales', '<=', Carbon::now())
                        ->get();
                }])->first()
            ;
    }
}
