<?php

namespace App\Repositories\TicketOffice\Contracts;

use App\Models\Event;

interface TicketOfficeRepositoryContract
{
    /**
     * Get event, organizer and ticket information by event id
     *
     * @param string $slug
     *
     * @return Event
     */
    public function getEventWithTickets(string $slug): Event;
}
