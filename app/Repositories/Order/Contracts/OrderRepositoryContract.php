<?php

namespace App\Repositories\Order\Contracts;

use App\Models\Order;

interface OrderRepositoryContract
{
    /**
     * create a new order
     *
     * @param array $data
     *
     * @return Order
     */
    public function store(array $data): Order;

    /**
     * Find a order
     *
     * @param string $value
     * @param string $by
     *
     * @return Order|null
     */
    public function get(string $value, string $by = 'id'): ?Order;

    /**
     * Update a order by id
     *
     * @param string $orderId
     * @param array $data
     *
     * @return Order
     */
    public function update(string $orderId, array $data): Order;

    /**
     * Update a order by model object
     *
     * @param Order $order
     * @param array $data
     *
     * @return Order
     */
    public function updateByModel(Order $order, array $data): Order;
}
