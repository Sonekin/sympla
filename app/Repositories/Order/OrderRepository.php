<?php

namespace App\Repositories\Order;

use App\Models\Order;
use App\Repositories\Order\Contracts\OrderRepositoryContract;

class OrderRepository implements OrderRepositoryContract
{
    private $model;

    /**
     * @param Order $model
     */
    function __construct(Order $model)
    {
        $this->model = $model;
    }

    /**
     * Store a order
     *
     * @param array $data
     *
     * @return Order
     */
    public function store(array $data): Order
    {
        return $this->model->create($data);
    }

    /**
     * Find a order
     *
     * @param string $value
     * @param string $by
     *
     * @return Order|null
     */
    public function get(string $value, string $by = 'id'): ?Order
    {
        return $this->model->where($by, $value)->first();
    }

    /**
     * Update a order by id
     *
     * @param string $orderId
     * @param array $data
     *
     * @return Order
     */
    public function update(string $orderId, array $data): Order
    {
        $order = $this->get($orderId, 'id');
        $order->update($data);
        return $order;
    }

    /**
     * Update a order by model object
     *
     * @param Order $order
     * @param array $data
     *
     * @return Order
     */
    public function updateByModel(Order $order, array $data): Order
    {
        $order->update($data);
        return $order;
    }
}
