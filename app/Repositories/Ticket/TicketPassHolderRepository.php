<?php

namespace App\Repositories\Ticket;

use App\Models\TicketPassHolder;
use App\Repositories\Ticket\Contracts\TicketPassHolderRepositoryContract;

class TicketPassHolderRepository implements TicketPassHolderRepositoryContract
{
     /**
     * @var TicketPassHolder
     */
    private $model;

    function __construct(TicketPassHolder $model)
    {
        $this->model = $model;
    }

    /**
     * Create a new participant for a ticket
     *
     * @param array $data
     *
     * @return TicketPassHolder
     */
    public function store(array $data): TicketPassHolder
    {
        return $this->model->create($data);
    }
}
