<?php

namespace App\Repositories\Ticket\Contracts;

use App\Models\Ticket;
use Illuminate\Database\Eloquent\Collection;

interface TicketRepositoryContract
{

    /**
     * find a Ticket
     *
     * @param int $id
     *
     * @return Ticket
     */
    public function get(int $id): Ticket;

    /**
     * create a new ticket
     *
     * @param array $data
     *
     * @return Ticket
     */
    public function store(array $data): Ticket;

    /**
     * update a ticket
     *
     * @param int $id
     * @param array $data
     *
     * @return Ticket
     */
    public function update(int $id, array $data): bool;
    
    /**
     * delete the ticket passed by param
     *
     * @param int $id
     *
     * @return bool
     */
    public function delete(int $id): bool;

    /**
     * Get tickets by ids
     *
     * @param array $ticketsId
     *
     * @return Collection
     */
    public function getByIds(array $ticketsId): Collection;

    /**
     * Get tickets with quantity available, by ids
     *
     * @param array $ticketsId
     *
     * @return Collection
     */
    public function getByIdsWithQuantityAvailable(array $ticketsId): Collection;

    /**
     * Get by date sold tickets in $ticketsId
     *
     * @param array $ticketsId
     *
     * @return Collection
     */
    public function getByIdsSoldTicketsPerDate(array $ticketsId): Collection;

    /**
     * Searchs list of sold tickets that matches the array filter belonging to the event
     *
     * @param array $filter
     * @param array $ticketsId
     *
     * @return Collection
     */
    public function getFilteredEventSoldTicketsPerDate(array $filter, array $ticketsId): Collection;
}
