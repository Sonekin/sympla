<?php

namespace App\Repositories\Ticket\Contracts;

use App\Models\TicketPassHolder;

interface TicketPassHolderRepositoryContract
{
    /**
     * Create a new holder for a ticket
     *
     * @param array $data
     *
     * @return TicketPassHolder
     */
    public function store(array $data): TicketPassHolder;
}
