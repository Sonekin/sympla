<?php

namespace App\Repositories\Ticket;

use App\Models\Ticket;
use App\Models\TicketPass;
use App\Repositories\Ticket\Contracts\TicketRepositoryContract;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class TicketRepository implements TicketRepositoryContract
{

    /**
     * @var Ticket
     */
    private $model;

    function __construct(Ticket $model)
    {
        $this->model = $model;
    }

    /**
     * find a ticket
     *
     * @param int $id
     *
     * @return Ticket
     */
    public function get(int $id): Ticket
    {
        return $this->model->where('id', $id)->first();
    }

    /**
     * create a new ticket
     *
     * @param array $data
     *
     * @return Ticket
     */
    public function store(array $data): Ticket {
        if (empty($data['fee'])) {
            $data['fee'] = Ticket::getDefaultFee();
        }

        return $this->model->create($data);
    }

    /**
     * update a ticket
     *
     * @param int $id
     * @param array $data
     *
     * @return Ticket
     */
    public function update(int $id, array $data): bool
    {
        return $this->model->where('id', $id)->update($data);
    }

    /**
     * delete the ticket passed by param
     *
     * @param int $id
     *
     * @return bool
     */
    public function delete(int $id): bool {
        try {
            $ticket = $this->model->findOrFail($id);
            return $ticket->delete();
        } catch (ModelNotFoundException $ex) {
            abort(404);
        } catch (Exception $ex) {
            throw new Exception('Unexpected error.');
        } 
    }

    /**
     * Get tickets by ids
     *
     * @param array $ticketsId
     *
     * @return Collection
     */
    public function getByIds(array $ticketsId): Collection
    {
        return $this->model->whereIn('id', $ticketsId)->get();
    }

    /**
     * Get tickets with quantity available, by ids
     *
     * @param array $ticketsId
     *
     * @return Collection
     */
    public function getByIdsWithQuantityAvailable(array $ticketsId): Collection
    {
        return
            $this->model
            ->addSelect(
                [
                    'sold' => TicketPass::selectRaw('count(*)')
                        ->whereColumn('tickets.id', 'ticket_passes.ticket_id')
                        ->groupBy('ticket_passes.ticket_id')
                ]
            )
            ->whereIn('tickets.id', $ticketsId)
            ->get();
    }

    /**
     * Get by date sold tickets in $ticketsId 
     *
     * @param array $ticketsId
     *
     * @return Collection
     */
    public function getByIdsSoldTicketsPerDate(array $ticketsId): Collection
    {
        return TicketPass::selectRaw('DATE(created_at) as sale_date, COUNT(*) as sold')
            ->whereIn('ticket_id', $ticketsId)
            ->groupBy('sale_date')
            ->orderBy('sale_date')
            ->get();
    }

    /**
     * Searchs list of sold tickets that matches the array filter belonging to the event
     *
     * @param array $filter
     * @param array $ticketsId
     *
     * @return Collection
     */
    public function getFilteredEventSoldTicketsPerDate(array $filter, array $ticketsId): Collection
    {
        return TicketPass::selectRaw('DATE(ticket_passes.created_at) as sale_date, COUNT(*) as sold')
            ->join('tickets', 'tickets.id', 'ticket_passes.ticket_id')
            ->when(!is_null(Arr::get($filter, 'paid_ticket', null)), function ($query) use ($filter) {
                if ($filter['paid_ticket']) {
                    $query->where('tickets.price', '>', 0);
                } else {
                    $query->where('tickets.price', 0);
                }
            })
            ->when(!is_null(Arr::get($filter, 'ticket_type', null)), function ($query) use ($filter) {
                $query->where('tickets.description', $filter['ticket_type']);
            })
            ->when(!is_null(Arr::get($filter, 'since_days_ago', null)), function ($query) use ($filter) {
                $since = now()->subDays($filter['since_days_ago'])->toDateString();
                $query->whereRaw('DATE(ticket_passes.created_at) >= ?', [$since]);
            })
            ->whereIn('ticket_id', $ticketsId)
            ->groupBy('sale_date')
            ->orderBy('sale_date')
            ->get();
    }
}
