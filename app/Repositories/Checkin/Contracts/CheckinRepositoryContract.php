<?php

namespace App\Repositories\Checkin\Contracts;

use App\Models\Checkin;
use Illuminate\Support\Collection;

interface CheckinRepositoryContract
{

    /**
     * create a new Checkin
     *
     * @param array $data
     *
     * @return Checkin
     */
    public function store(array $data): Checkin;

    /**
     * find a Checkin
     *
     * @param  string  $value
     * @param  string  $by
     *
     * @return ?Checkin
     */
    public function get(string $value, string $by = 'ticket_pass_id'): ?Checkin;

}

