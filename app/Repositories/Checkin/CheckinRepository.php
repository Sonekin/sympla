<?php

namespace App\Repositories\Checkin;

use App\Models\Checkin;
use App\Models\CheckinAddress;
use Illuminate\Support\Collection;
use App\Repositories\Checkin\Contracts\CheckinRepositoryContract;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class CheckinRepository implements CheckinRepositoryContract
{
    /**
     * @var Checkin
     */
    private $model;

    function __construct(Checkin $model)
    {
        $this->model = $model;
    }

    /**
     * create a new Checkin
     *
     * @param array $data
     *
     * @return Checkin
     */
    public function store(array $data): Checkin
    {
        return $this->model->create($data);
    }

    /**
     * get Checkin passed by param
     *
     * @param string $ticketPassId
     *
     * @return Checkin
     */

    public function get(string $value, string $by = 'ticket_pass_id'): ?Checkin
    {
        return $this->model->where($by, intval($value))->first();
    }
}
