<?php

namespace App\Repositories\Event;

use App\Models\Event;
use App\Models\EventAddress;
use Illuminate\Support\Collection;
use App\Repositories\Event\Contracts\EventRepositoryContract;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class EventRepository implements EventRepositoryContract
{
    /**
     * @var Event
     */
    private $model;

    function __construct(Event $model)
    {
        $this->model = $model;
    }

    /**
     * find a Event
     *
     * @param  string  $value
     * @param  string  $by
     *
     * @return ?Event
     */
    public function get(string $value, string $by = 'slug'): ?Event
    {
        return $this->model->where($by, $value)->first();
    }

    /**
     * search a EventAddress collection
     *
     * @param  ?string  $value
     * @param  ?string  $by
     *
     * @return Collection
     */
    public function getEventAddressCollection(?string $value, ?string $by): Collection
    {
        return EventAddress::where($by, $value)->get();
    }

    /**
     * Get the list of event subjects
     *
     * @return array
     */
    public function getEventSubjectList(): array
    {
        return Event::getSubjectList();
    }

    /**
     * Get the list of event categories
     *
     * @return array
     */
    public function getEventCategoryList(): array
    {
        return Event::getCategoryList();
    }

    /**
     * search a Event collection
     *
     * @param  ?string  $value
     * @param  ?string  $by
     *
     * @return Collection
     */
    public function getCollection(?string $value, ?string $by): Collection
    {
        return $this->model->where($by, $value)->get();
    }

    /**
     * Searchs list of events belonging to the list of companies that matches the array filter
     *
     * @param  array $filter
     * @param  ?Collection $companies
     *
     * @return Collection
     */
    public function getFilteredEventsByCompanies(array $filter, ?Collection $companies): Collection
    {
        $companiesIds = $companies->pluck('id')->toArray();

        return $this->model
            ->when(!is_null(Arr::get($filter, 'name', null)), function ($query) use ($filter) {
                $query->where('name', 'like', "%{$filter['name']}%");
            })
            ->when(!is_null(Arr::get($filter, 'event_status', null)), function ($query) use ($filter) {
                $query->where('event_status', $filter['event_status']);
            })
            ->whereIn('company_id', $companiesIds)
            ->get();
    }

    /**
     * create a new event
     *
     * @param array $data
     *
     * @return Event
     */
    public function store(array $data): Event
    {
        DB::beginTransaction();
        try {
            if (!empty($data['save_address'])) {
                $eventAddress = $this->eventRepository->storeEventAddress($data);
                $data['address_id'] = $eventAddress->id;
            }

            if (empty($data['slug'])) {
                $data['slug'] = Str::slug($data['name'], '-');
            }

            $event = $this->model->create($data);

            DB::commit();
            return $event;
        } catch (Exception $ex) {
            DB::rollBack();
        }
    }

    /**
     * update the contents of the event passed by param
     *
     * @param array $data
     * @param int $eventId
     *
     * @return Event
     */
    public function update(array $data, int $eventId): Event
    {
        DB::beginTransaction();
        try {
            if (!empty($data['save_address'])) {
                $eventAddress = $this->eventRepository->storeEventAddress($data);
                $data['address_id'] = $eventAddress->id;
            }

            $event = $this->model->findOrFail($eventId);
            $event->update(Arr::except($data, ['slug']));

            DB::commit();
            return $event;
        } catch (ModelNotFoundException $ex) {
            DB::rollBack();
            abort(404);
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception('Unexpected error.');
        }
    }

    /**
     * delete the event passed by param
     *
     * @param int $eventId
     *
     * @return bool
     */
    public function delete(int $eventId): bool {
        try {
            $event = $this->model->findOrFail($eventId);
            return $event->delete();
        } catch (ModelNotFoundException $ex) {
            abort(404);
        } catch (Exception $ex) {
            throw new Exception('Unexpected error.');
        } 
    }

    /**
     * create a new event address
     *
     * @param array $data
     *
     * @return EventAddress
     */
    public function storeEventAddress(array $data): EventAddress
    {
        $data['name'] = Arr::get($data, 'address_name', null);
        return EventAddress::create($data);
    }
    
    public function report(string $value, string $by = 'id'): void
    {
        $eventId = intval($value);
        $result = $this->model
        ->select([
            'transactions.status AS status',
            'ticket_pass_holders.name AS name',
            'ticket_passes.code AS ticket_number',
            'tickets.description AS ticket_type',
            'users.name AS buyer_name',
            'orders.created_at AS purchase_date',
        ])
        ->join('ticket_passes', 'ticket_passes.id', 'ticket_pass_holders.ticket_pass_id')
        ->join('tickets', 'tickets.id', 'ticket_passes.ticket_id')
        ->join('orders', 'orders.id', 'ticket_passes.order_id')
        ->join('transactions', 'transactions.order_id', 'orders.id')
        ->join('user_capabilities', 'user_capabilities.capacity_id', 'orders.consumer_id')
        ->join('users', 'users.id', 'user_capabilities.user_id')
        ->where('events.id', $eventId)
        ->get();

        dump($result);die();
    }

}
