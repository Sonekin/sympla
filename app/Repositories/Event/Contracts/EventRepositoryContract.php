<?php

namespace App\Repositories\Event\Contracts;

use App\Models\Event;
use App\Models\EventAddress;
use Illuminate\Support\Collection;

interface EventRepositoryContract
{

    /**
     * find a Event
     *
     * @param  string  $value
     * @param  string  $by
     *
     * @return ?Event
     */
    public function get(string $value, string $by = 'slug'): ?Event;

    /**
     * search a Event collection
     *
     * @param  ?string  $value
     * @param  ?string  $by
     *
     * @return Collection
     */
    public function getCollection(?string $value, ?string $by): Collection;

    /**
     * search a EventAddress collection
     *
     * @param  ?string  $value
     * @param  ?string  $by
     *
     * @return Collection
     */
    public function getEventAddressCollection(?string $value, ?string $by): Collection;

    /**
     * Get the list of event subjects
     *
     * @return array
     */
    public function getEventSubjectList(): array;

    /**
     * Get the list of event categories
     *
     * @return array
     */
    public function getEventCategoryList(): array;

    /**
     * Searchs list of events belonging to the list of companies that matches the array filter
     *
     * @param  array $filter
     * @param  ?Collection $companies
     *
     * @return Collection
     */
    public function getFilteredEventsByCompanies(array $filter, ?Collection $companies): Collection;

    /**
     * create a new event
     *
     * @param array $data
     *
     * @return Event
     */
    public function store(array $data): Event;

    /**
     * update the contents of the event passed by param
     *
     * @param array $data
     * @param int $eventId
     *
     * @return Event
     */
    public function update(array $data, int $eventId): Event;

    /**
     * delete the event passed by param
     *
     * @param int $eventId
     *
     * @return bool
     */
    public function delete(int $eventId): bool;

    /**
     * create a new event address
     *
     * @param array $data
     *
     * @return EventAddress
     */
    public function storeEventAddress(array $data): EventAddress;
}
