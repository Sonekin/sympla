<?php

namespace App\Repositories\Transaction;

use App\Models\Transaction;
use App\Repositories\Transaction\Contracts\TransactionRepositoryContract;

class TransactionRepository implements TransactionRepositoryContract
{
    private $model;

    /**
     * @param Transaction $model
     */
    function __construct(Transaction $model)
    {
        $this->model = $model;
    }

    /**
     * Store a transaction
     *
     * @param array $data
     *
     * @return Transaction
     */
    public function store(array $data): Transaction
    {
        return $this->model->create($data);
    }

    /**
     * Find a Transaction
     *
     * @param string $value
     * @param string $by
     *
     * @return Transaction|null
     */
    public function get(string $value, string $by = 'id'): ?Transaction
    {
        return $this->model->where($by, $value)->firstOrFail();
    }

    /**
     * update a transaction
     *
     * @param string $transactionId
     * @param array $data
     *
     * @return Transaction
     */
    public function update(string $transactionId, array $data): Transaction
    {
        $transaction = $this->get($transactionId, 'gateway_transaction_id');
        $transaction->update($data);
        return $transaction;
    }
}
