<?php

namespace App\Repositories\Transaction\Contracts;

use App\Models\Transaction;

interface TransactionRepositoryContract
{
    /**
     * create a new Transaction
     *
     * @param array $data
     *
     * @return Transaction
     */
    public function store(array $data): Transaction;

    /**
     * Find a Transaction
     *
     * @param string $value
     * @param string $by
     *
     * @return Transaction|null
     */
    public function get(string $value, string $by = 'id'): ?Transaction;

    /**
     * update a Transaction
     *
     * @param string $transactionId
     * @param array $data
     *
     * @return Transaction
     */
    public function update(string $transactionId, array $data): Transaction;
}
