<?php

namespace App\Repositories\TicketPass;

use App\Models\TicketPass;
use App\Repositories\TicketPass\Contracts\TicketPassRepositoryContract;

class TicketPassRepository implements TicketPassRepositoryContract
{
    /**
     * @var TicketPass
     */
    private $model;

    function __construct(TicketPass $model)
    {
        $this->model = $model;
    }

    /**
     * Register a consumer at an event
     *
     * @param  array  $data
     *
     * @return TicketPass
     */
    public function register(array $data): TicketPass
    {
        return $this->model->create($data);
    }


    /**
     * find a ticket Pass
     *
     * @param string $value
     * @param string $by
     *
     * @return TicketPass|null
     */
    public function get(string $value, $by = 'id'): ?TicketPass
    {
        return $this->model->where($by, $value)->first();
    }
}
