<?php

namespace App\Repositories\TicketPass\Contracts;

use App\Models\TicketPass;

interface TicketPassRepositoryContract
{
     /**
     * Register a consumer at an event
     *
     * @param array $data
     *
     * @return TicketPass
     */
    public function register(array $data): TicketPass;

    /**
     * find a Ticket Pass
     *
     * @param string $value
     * @param string $by
     *
     * @return TicketPass|null
     */
    public function get(string $value, $by = 'id'): ?TicketPass;
}
