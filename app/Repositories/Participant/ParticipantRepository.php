<?php

namespace App\Repositories\Participant;

use App\Repositories\Participant\Contracts\ParticipantRepositoryContract;
use App\Models\TicketPassHolder;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class ParticipantRepository implements ParticipantRepositoryContract
{
    /**
     * @var TicketPassHolder
     */
    private $model;

    function __construct(TicketPassHolder $model)
    {
        $this->model = $model;
    }

    /**
     * Get a participant by id
     *
     * @param int $participantId
     *
     * @return TicketPassHolder
     */
    public function get(int $participantId): TicketPassHolder
    {
        return $this->model->find($participantId);
    }

    /**
     * Searchs a participant collection
     *
     * @param  ?string  $value
     * @param  ?string  $by
     *
     * @return Collection
     */
    public function getCollection(?string $value, ?string $by): Collection
    {
        return $this->model->where($by, $value)->get();
    }

    /**
     * Mounts basic participants query inner joins
     *
     * @return Builder
     */
    private function mountParticipantsQuery(): Builder
    {
        return
            $this->model
            ->select([
                'transactions.status AS status',
                'ticket_pass_holders.name AS name',
                'ticket_passes.code AS ticket_number',
                'tickets.description AS ticket_type',
                'users.name AS buyer_name',
                'orders.created_at AS purchase_date',
            ])
            ->join('ticket_passes', 'ticket_passes.id', 'ticket_pass_holders.ticket_pass_id')
            ->join('tickets', 'tickets.id', 'ticket_passes.ticket_id')
            ->join('orders', 'orders.id', 'ticket_passes.order_id')
            ->join('transactions', 'transactions.order_id', 'orders.id')
            ->join('user_capabilities', 'user_capabilities.capacity_id', 'orders.consumer_id')
            ->join('users', 'users.id', 'user_capabilities.user_id');
    }

    /**
     * Searchs all participants by event
     *
     * @param int $eventId
     *
     * @return Collection
     */
    public function getParticipantsByEvent(int $eventId): Collection
    {
        return
            $this->mountParticipantsQuery()
            ->where('tickets.event_id', $eventId)
            ->where('user_capabilities.capacity_type', 'App\Models\Consumer')
            ->get();
    }

    /**
     * Searchs filtered participants by event
     *
     * @param array $filter
     * @param int $eventId
     *
     * @return Collection
     */
    public function getFilteredParticipantsByEvent(array $filter, int $eventId): Collection
    {
        return
            $this->mountParticipantsQuery()
            ->where('tickets.event_id', $eventId)
            ->where('user_capabilities.capacity_type', 'App\Models\Consumer')
            ->when(!is_null(Arr::get($filter, 'search', null)), function ($query) use ($filter) {
                $query->where(function ($query) use ($filter) {
                    $query->where('ticket_pass_holders.name', 'like', "%{$filter['search']}%")
                          ->orWhere('ticket_pass_holders.email', 'like', "%{$filter['search']}%")
                          ->orWhere('ticket_passes.code', 'like', "%{$filter['search']}%");
                });
            })
            ->when(!is_null(Arr::get($filter, 'status', null)), function ($query) use ($filter) {
                $query->where('transactions.status', $filter['status']);
            })
            ->when(!is_null(Arr::get($filter, 'ticket_type', null)), function ($query) use ($filter) {
                $query->where('tickets.description', $filter['ticket_type']);
            })
            ->get();
    }
    
    /**
     * Filter all participants by event
     *
     * @param int $eventId
     *
     * @return Collection
     */
    public function getParticipantsReportByEvent(int $eventId): Collection
    {
        return
        $this->model
        ->select([
            'transactions.status AS status',
            'ticket_pass_holders.name AS name',
            'ticket_passes.code AS ticket_number',
            'tickets.description AS ticket_type',
            'users.name AS buyer_name',
            'orders.created_at AS purchase_date',
            'checkin.created_at AS checkin_date',
        ])
        ->join('ticket_passes', 'ticket_passes.id', 'ticket_pass_holders.ticket_pass_id')
        ->leftJoin('checkin', 'checkin.ticket_pass_id', 'ticket_passes.id')
        ->join('tickets', 'tickets.id', 'ticket_passes.ticket_id')
        ->join('orders', 'orders.id', 'ticket_passes.order_id')
        ->join('transactions', 'transactions.order_id', 'orders.id')
        ->join('user_capabilities', 'user_capabilities.capacity_id', 'orders.consumer_id')
        ->join('users', 'users.id', 'user_capabilities.user_id')
        ->where('tickets.event_id', $eventId)
        ->where('user_capabilities.capacity_type', 'App\Models\Consumer')
        ->get();
    }

}
