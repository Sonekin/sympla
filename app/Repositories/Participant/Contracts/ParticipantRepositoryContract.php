<?php

namespace App\Repositories\Participant\Contracts;

use App\Models\TicketPassHolder;
use Illuminate\Support\Collection;

interface ParticipantRepositoryContract
{
    /**
     * Get a participant by id
     *
     * @param int $participantId
     *
     * @return TicketPassHolder
     */
    public function get(int $participantId): TicketPassHolder;

    /**
     * Searchs a participant collection
     *
     * @param  ?string  $value
     * @param  ?string  $by
     *
     * @return Collection
     */
    public function getCollection(?string $value, ?string $by): Collection;

    /**
     * Searchs all participants by event
     *
     * @param int $eventId
     *
     * @return Collection
     */
    public function getParticipantsByEvent(int $eventId): Collection;

    /**
     * Searchs filtered participants by event
     *
     * @param array $filter
     * @param int $eventId
     *
     * @return Collection
     */
    public function getFilteredParticipantsByEvent(array $filter, int $eventId): Collection;

    
    /**
     * Searchs filtered participants report by event
     *
     * @param array $filter
     * @param int $eventId
     *
     * @return Collection
     */
    public function getParticipantsReportByEvent(int $eventId): Collection;

}
