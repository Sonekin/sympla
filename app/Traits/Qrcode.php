<?php

namespace App\Traits;

use SimpleSoftwareIO\QrCode\Facades\QrCode as FacadeQrCode;

Trait Qrcode
{
    /**
     * Generate a QrCode
     *
     * @param string $code
     *
     * @return string
     */
    private function generateQrCode(string $code): string
    {
        /** if it is necessary to define specific styles for the qr code, customize this method  */
        return FacadeQrCode::backgroundColor(0,0,0,0)->size(80)->generate($code)->toHtml();
    }
}
