<?php

namespace App\Traits;

use App\Models\Order;
use Exception;

Trait GatewayPayment
{
    /**
     * Get the gateway responsible for the payment
     *
     * @param string $paymentMethod
     *
     * @return string
     */
    private function getGatewayByPaymentMethod(string $paymentMethod): string
    {
        switch ($paymentMethod) {
            case Order::PAYMENT_BOOK:
            case Order::INVOICE:
            case Order::PIX:
                return Order::GATEWAY_CONTA_PAY;
            case Order::CREDIT_CARD:
                return Order::GATEWAY_PAGAR_ME;
        }
    }
}
