<?php
namespace App\Traits;

use App\Exceptions\AuthorizationCapabilitiesException;
use App\Models\User;

Trait AuthCapabilities
{
    public function isLogged(): bool
    {
        return auth()->check();
    }

    public function authConsumer(): User
    {
        /** @var User $user */
        $user = auth()->user();
        $user->loadMissing('capacityConsumer');

        if (! $user->capacityConsumer) {
            throw new AuthorizationCapabilitiesException("You are not allowed to perform this action.");
        }

        return $user;
    }

    public function authOrganizer(): User
    {
        //todo
        return (new User());
    }

    public function authAdmin(): User
    {
        //todo
        return (new User());
    }

    public function authPartner(): User
    {
        //todo
        return (new User());
    }
}
