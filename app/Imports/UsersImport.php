<?php

namespace App\Imports;

use App\Models\Imports;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\Hash;


class UsersImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        //dump($row);die();
        return new Imports([
            'nome'     => $row[0],
            'cpf'     => $row[1],
            'email'    => $row[2],
            'telefone'    => $row[3],
            'ingresso'    => $row[4],
            //'valor'    => (float)$row[5]
        ]);
    }

    public function collection($row)
    {
        return $row;
    }
}
