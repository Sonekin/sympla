<?php

namespace App\Providers;

use App\Repositories\Consumer\ConsumerGatewayRepository;
use App\Repositories\Consumer\ConsumerRepository;
use App\Repositories\Consumer\Contracts\ConsumerGatewayRepositoryContract;
use App\Repositories\Consumer\Contracts\ConsumerRepositoryContract;
use App\Repositories\Event\Contracts\EventRepositoryContract;
use App\Repositories\Event\EventRepository;
use App\Repositories\Ticket\Contracts\TicketPassHolderRepositoryContract;
use App\Repositories\Order\Contracts\OrderRepositoryContract;
use App\Repositories\Order\OrderRepository;
use App\Repositories\Participant\Contracts\ParticipantRepositoryContract;
use App\Repositories\Participant\ParticipantRepository;
use App\Repositories\Ticket\Contracts\TicketRepositoryContract;
use App\Repositories\Ticket\TicketPassHolderRepository;
use App\Repositories\Ticket\TicketRepository;
use App\Repositories\TicketOffice\Contracts\TicketOfficeRepositoryContract;
use App\Repositories\TicketOffice\TicketOfficeRepository;
use App\Repositories\TicketPass\Contracts\TicketPassRepositoryContract;
use App\Repositories\TicketPass\TicketPassRepository;
use App\Repositories\Transaction\Contracts\TransactionRepositoryContract;
use App\Repositories\Transaction\TransactionRepository;
use App\Repositories\User\Contracts\UserRepositoryContract;
use App\Repositories\User\UserRepository;
use App\Repositories\Checkin\Contracts\CheckinRepositoryContract;
use App\Repositories\Checkin\CheckinRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            UserRepositoryContract::class,
            UserRepository::class
        );

        $this->app->bind(
            ConsumerRepositoryContract::class,
            ConsumerRepository::class
        );

        $this->app->bind(
            EventRepositoryContract::class,
            EventRepository::class
        );

        $this->app->bind(
            TicketRepositoryContract::class,
            TicketRepository::class
        );

        $this->app->bind(
            TicketOfficeRepositoryContract::class,
            TicketOfficeRepository::class
        );

        $this->app->bind(
            TicketPassRepositoryContract::class,
            TicketPassRepository::class
        );

        $this->app->bind(
            TicketPassHolderRepositoryContract::class,
            TicketPassHolderRepository::class
        );

        $this->app->bind(
            OrderRepositoryContract::class,
            OrderRepository::class
        );

        $this->app->bind(
            ParticipantRepositoryContract::class,
            ParticipantRepository::class
        );

        $this->app->bind(
           TransactionRepositoryContract::class,
           TransactionRepository::class
        );

        $this->app->bind(
            ConsumerGatewayRepositoryContract::class,
            ConsumerGatewayRepository::class
        );
        
        $this->app->bind(
            ConsumerGatewayRepositoryContract::class,
            ConsumerGatewayRepository::class
        );

        $this->app->bind(
            CheckinRepositoryContract::class,
            CheckinRepository::class
        );

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
