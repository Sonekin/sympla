<?php

namespace App\Services\Ticket;

use App\Models\Ticket;
use App\Repositories\Ticket\Contracts\TicketRepositoryContract;

class TicketService
{
    /**
     * @var TicketRepositoryContract
     */
    private $ticketRepository;

    function __construct(
        TicketRepositoryContract $ticketRepository
    ) {
        $this->ticketRepository = $ticketRepository;
    }

    /**
     * create a new ticket
     *
     * @param array $data
     *
     * @return Ticket
     */
    public function storeTicket(array $data): Ticket {
        return $this->ticketRepository->store($data);
    }

    /**
     * update the contents of the ticket passed by param
     *
     * @param array $data
     * @param int $ticketId
     *
     * @return Ticket
     */
    public function updateTicket(array $data, int $ticketId): Ticket {
        $success = $this->ticketRepository->update($ticketId, $data);
        if(!$success) {
            abort(404);
        }

        return $this->ticketRepository->get($ticketId);
    }

    /**
     * delete the the ticket passed by param
     *
     * @param int $ticketId
     *
     * @return bool
     */
    public function deleteTicket(int $ticketId): bool {
        return $this->ticketRepository->delete($ticketId);
    }
}
