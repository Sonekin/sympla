<?php

namespace App\Services\Payment\ContaPay;

use App\Integrations\ContaPay\ContaPayIntegration;
use App\Services\Payment\Contracts\GatewayClientContract;
use Illuminate\Support\Arr;

class Client implements GatewayClientContract
{
    private $contaPay;

    public function __construct()
    {
        $this->contaPay = new ContaPayIntegration();
    }

    /**
     * Register a client in contapay gateway
     *
     * @param array $data
     *
     * @return array
     */
    public function store(array $data): array
    {
        $client = [
            'name' => Arr::get($data, 'name'),
            'phone' => Arr::get($data, 'phone'),
            'email' => Arr::get($data, 'email'),
            'person_id' => Arr::get($data, 'cpf'),
            'address' => [
                'zip_code' => Arr::get($data, 'zip_code'),
                'street' => Arr::get($data, 'street'),
                'number' => Arr::get($data, 'number'),
                'complement' => Arr::get($data, 'complement'),
                'district' => Arr::get($data, 'district'),
                'city' => Arr::get($data, 'city'),
                'uf' => Arr::get($data, 'uf')
            ]
        ];
        return $this->contaPay->clients()->create($client);
    }

    /**
     * Get a client in contapay gateway
     *
     * @param string $identifier
     *
     * @return array
     */
    public function get(string $identifier): array
    {
        return $this->contaPay->clients()->getById($identifier);
    }
}
