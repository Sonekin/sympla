<?php

namespace App\Services\Payment\ContaPay;

use App\Integrations\ContaPay\ContaPayIntegration;
use App\Services\Payment\Contracts\GatewayTransactionContract;
use Illuminate\Support\Arr;

class PixTransaction implements GatewayTransactionContract
{
    private $contaPay;

    public function __construct()
    {
        $this->contaPay = new ContaPayIntegration();
    }

    /**
     * Register a order of type pix in contapay gateway
     *
     * @param array $data
     *
     * @return array
     */
    public function store(array $data): array
    {
        $pix = [
            'internal_id' =>  Arr::get($data, 'order.id'),
            'description' => 'Cobranca passaporte digital',
            'client_id' => Arr::get($data, 'client_id_gateway'),
            'amount' => Arr::get($data, 'order.amount'),
            'due_date' => Arr::get($data, 'due_date'),
            'assessment' => "0",
            'interest' => "0",
            'discount' => "0",
            'days_discount' => "0"
        ];
        return $this->contaPay->pix()->create($pix);
    }

    /**
     * Get a order of type pix in contapay gateway
     *
     * @param string $identifier
     *
     * @return array
     */
    public function get(string $identifier): array
    {
        return $this->contaPay->billings()->getById($identifier);
    }
}
