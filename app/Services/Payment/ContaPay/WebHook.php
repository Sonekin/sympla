<?php

namespace App\Services\Payment\ContaPay;

use App\Models\Transaction;
use App\Services\Payment\Contracts\GatewayWebHookContract;
use Illuminate\Support\Arr;

class WebHook implements GatewayWebHookContract
{
    /**
     * Serialize contapay gateway webhook to transaction format
     *
     * @param array $data
     *
     * @return array
     */
    public function serialize(array $data): array
    {
        $invoice = Arr::get($data, 'data.invoices.0');
        return [
            'id' => Arr::get($invoice, 'invoice_id'),
            'billing_id' => Arr::get($invoice, 'billing_id'),
            'payment_date' => Arr::get($invoice, 'payment_date'),
            'status' => $this->getStatus(Arr::get($invoice, 'status')),
        ];
    }

    /**
     * translates contapay gateway status to transaction format
     *
     * @param string $status
     *
     * @return string
     */
    private function getStatus(string $status): string
    {
        switch ($status) {
            case 'PENDING':
                return Transaction::PENDING;
            case 'PAID':
                return Transaction::PAID;
        }
    }
}
