<?php

namespace App\Services\Payment\ContaPay;

use App\Integrations\ContaPay\ContaPayIntegration;
use App\Services\Payment\Contracts\GatewayTransactionContract;
use Illuminate\Support\Arr;

class InvoiceTransaction implements GatewayTransactionContract
{
    private $contaPay;

    public function __construct()
    {
        $this->contaPay = new ContaPayIntegration();
    }

    /**
     * Register a order of type invoice in contapay gateway
     *
     * @param array $data
     *
     * @return array
     */
    public function store(array $data): array
    {
        $invs = Arr::get($data, 'invoices');
        $invoices = [];

        foreach ($invs as $invoice) {
            array_push($invoices, [
                'due_date' => Arr::get($invoice, 'due_date'),
                'value' => Arr::get($invoice, 'value'),
                'assessment' => "0",
                'interest' => "0",
                'discount' => "0",
                'days_discount' => "0",
                'demonstrative' => []
            ]);
        }

        $billings = [
            'description' => 'Cobranca passaporte digital',
            'internal_code' => Arr::get($data, 'order.id'),
            'client_id' => Arr::get($data, 'client_id_gateway'),
            'payment_type' => 'BANK_SLIP',
            'installment' => count($invs),
            'amount' => Arr::get($data, 'order.amount'),
            'invoices' => $invoices
        ];
        return $this->contaPay->billings()->create($billings);
    }

    /**
     * Get a order of type invoice in contapay gateway
     *
     * @param string $identifier
     *
     * @return array
     */
    public function get(string $identifier): array
    {
        return $this->contaPay->billings()->getById($identifier);
    }
}
