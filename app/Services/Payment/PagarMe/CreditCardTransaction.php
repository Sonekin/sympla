<?php

namespace App\Services\Payment\PagarMe;

use App\Integrations\PagarMe\PagarMeIntegration;
use App\Services\Payment\Contracts\GatewayTransactionContract;
use Illuminate\Support\Arr;
use PagarmeCoreApiLib\Models\CreateAddressRequest;
use PagarmeCoreApiLib\Models\CreateCardRequest;
use PagarmeCoreApiLib\Models\CreateCreditCardPaymentRequest;
use PagarmeCoreApiLib\Models\CreateOrderItemRequest;
use PagarmeCoreApiLib\Models\CreateOrderRequest;
use PagarmeCoreApiLib\Models\CreatePaymentRequest;
use PagarmeCoreApiLib\Models\GetOrderResponse;

class CreditCardTransaction implements GatewayTransactionContract
{
    private $pagarMe;

    public function __construct()
    {
        $this->pagarMe = PagarMeIntegration::init();
    }

    /**
     * Register a order of type credit card in pagarme gateway
     *
     * @param array $data
     *
     * @return array
     */
    public function store(array $data): array
    {
        list($expMonth, $expYear) = explode('/', Arr::get($data, 'card.expiration_date'));

        $creditCard = new CreateCreditCardPaymentRequest();
        $creditCard->capture = true;
        $creditCard->installments = Arr::get($data, 'installments');

        //credit card
        $creditCard->card = new CreateCardRequest();
        $creditCard->card->number = Arr::get($data, 'card.number');
        $creditCard->card->holderName = Arr::get($data, 'card.holder_name');
        $creditCard->card->holderDocument = str_replace(['.', '-'], '', Arr::get($data, 'user.cpf'));
        $creditCard->card->expMonth = $expMonth;
        $creditCard->card->expYear = $expYear;
        $creditCard->card->cvv = Arr::get($data, 'card.cvv');

        // Billing Address;
        $creditCard->card->billingAddress = new CreateAddressRequest();
        $creditCard->card->billingAddress->line1 = Arr::get($data, 'user.number', '') . ', ' . Arr::get($data, 'user.street', '') . ', ' . Arr::get($data, 'user.district', '');
        $creditCard->card->billingAddress->line2 = Arr::get($data, 'user.complement', '');
        $creditCard->card->billingAddress->zipCode = str_replace('-', '', Arr::get($data, 'user.zip_code', ''));
        $creditCard->card->billingAddress->city = Arr::get($data, 'user.city', '');
        $creditCard->card->billingAddress->state = Arr::get($data, 'user.uf', '');
        $creditCard->card->billingAddress->country = "BR";
        // // Card Options: Verify OneDollarAuth;
        // $creditCard->card->options = new \PagarmeCoreApiLib\Models\CreateCardOptionsRequest();
        // $creditCard->card->options->verifyCard = true;

        $request = new CreateOrderRequest();

        $request->items = [new CreateOrderItemRequest()];
        $request->items[0]->code = 01;
        $request->items[0]->description = "Compra passaporte digital";
        $request->items[0]->quantity = 1;
        $request->items[0]->amount = Arr::get($data, 'order.amount') * 100; // this value should be in cents

        $request->payments = [new CreatePaymentRequest()];
        $request->payments[0]->paymentMethod = "credit_card";
        $request->payments[0]->creditCard = $creditCard;
        $request->customerId = Arr::get($data, 'client_id_gateway');

        $result = $this->pagarMe->getOrders()->createOrder($request);
        return $this->responseToArray($result);
    }

    /**
     * @param GetOrderResponse $response
     *
     * @return array
     */
    private function responseToArray(GetOrderResponse $response): array
    {
        return [
            'billing_id' => $response->id,
            'invoices' => [
                [
                    'invoice_id' => $response->charges[0]->id,
                    'value' => ($response->charges[0]->amount/100),
                    'due_date' => null,
                ],
            ]
        ];
    }

    /**
     * Get a order of type credit card in pagarme gateway
     *
     * @param string $identifier
     *
     * @return array
     */
    public function get(string $identifier): array
    {
        return [];
    }
}
