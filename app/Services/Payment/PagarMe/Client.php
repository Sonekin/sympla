<?php

namespace App\Services\Payment\PagarMe;

use App\Integrations\PagarMe\PagarMeIntegration;
use App\Services\Payment\Contracts\GatewayClientContract;
use Exception;
use Illuminate\Support\Arr;
use PagarmeCoreApiLib\Models\CreateCustomerRequest;
use PagarmeCoreApiLib\Models\CreatePhoneRequest;
use PagarmeCoreApiLib\Models\CreatePhonesRequest;

class Client implements GatewayClientContract
{
    private $pagarMe;

    public function __construct()
    {
        $this->pagarMe = PagarMeIntegration::init();
    }

    /**
     * Register a client in contapay gateway
     *
     * @param array $data
     *
     * @return array
     */
    public function store(array $data): array
    {
        $consumer = $this->pagarMe->getCustomers();
        $consumer = new CreateCustomerRequest();
        $consumer->code = Arr::get($data, 'capacityConsumer.capacity_id');
        $consumer->name = Arr::get($data, 'name');
        $consumer->email = Arr::get($data, 'email');
        $consumer->type = "individual";
        $consumer->document_type = "CPF";
        $consumer->document = str_replace(['.', '-'], '', Arr::get($data, 'cpf'));

        $phone = $this->extractPhoneNumber(Arr::get($data, 'phone'));
        $consumer->phones = new CreatePhonesRequest();
        $consumer->phones->mobilePhone = new CreatePhoneRequest();
        $consumer->phones->mobilePhone->areaCode = $phone['code'];
        $consumer->phones->mobilePhone->countryCode = "55"; //BR
        $consumer->phones->mobilePhone->number = $phone['number'];

        $response = $this->pagarMe->getCustomers()->createCustomer($consumer);

        return [
            'client_id' => $response->id
        ];
    }

    /**
     * @param string $phone
     *
     * @return array
     */
    private function extractPhoneNumber(string $phone): array
    {
        $mimSizePhone = 14; //(00) 0011-3344 or (00) 90011-3344
        if (strlen($phone)  < $mimSizePhone) {
            throw new Exception('Phone incorrect');
        }

        return [
            'code' => str_replace(['(', ')'], '', substr($phone, 0, 4)),
            'number' => str_replace('-', '', substr($phone, 5)),
        ];
    }

    /**
     * Get a client in contapay gateway
     *
     * @param string $identifier
     *
     * @return array
     */
    public function get(string $identifier): array
    {
        return [];
    }
}
