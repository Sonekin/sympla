<?php

namespace App\Services\Payment\PagarMe;

use App\Models\Transaction;
use App\Services\Payment\Contracts\GatewayWebHookContract;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class WebHook implements GatewayWebHookContract
{
    /**
     * Serialize pagarme gateway webhook to transaction format
     *
     * @param array $data
     *
     * @return array
     */
    public function serialize(array $data): array
    {
        return [
            'id' => Arr::get($data, 'id'),
            'billing_id' => Arr::get($data, 'order.id'),
            'payment_date' => Arr::get($data, 'paid_at') ? Carbon::create(Arr::get($data, 'paid_at'))->timezone('-3') : null,
            'status' => $this->getStatus(Arr::get($data, 'status')),
        ];
    }

    /**
     * translates pagarme gateway status to transaction format
     *
     * @param string $status
     *
     * @return string
     */
    private function getStatus(string $status): string
    {
        switch ($status) {
            case 'chargedback':
                return Transaction::CHARGEDBACK;
            case 'created':
                return Transaction::CREATED;
            case 'overpaid':
                return Transaction::OVERPAID;
            case 'partial_canceled':
                return Transaction::PARTIAL_CANCELED;
            case 'payment_failed':
                return Transaction::PAYMENT_FAILED;
            case 'processing':
                return Transaction::PROCESSING;
            case 'refunded':
                return Transaction::REFUNDED;
            case 'underpaid':
                return Transaction::UNDERPAID;
            case 'updated':
                return Transaction::UPDATED;
            case 'paid':
                return Transaction::PAID;
            case 'PENDING':
                return Transaction::PENDING;
            default:
                return '';
        }
    }
}
