<?php

namespace App\Services\Payment\Contracts;

interface GatewayWebHookContract
{
    public function serialize(array $data): array;
}
