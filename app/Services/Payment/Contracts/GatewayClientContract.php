<?php

namespace App\Services\Payment\Contracts;

interface GatewayClientContract
{
    public function store(array $data): array;

    public function get(string $identifier): array;
}
