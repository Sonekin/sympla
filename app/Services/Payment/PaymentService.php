<?php

namespace App\Services\Payment;

use App\Exceptions\GatewayNotFoundException;
use App\Exceptions\PaymentInstanceNotFoundException;
use App\Models\Order;
use App\Services\Payment\Contracts\GatewayClientContract;
use App\Services\Payment\Contracts\GatewayTransactionContract;
use App\Services\Payment\Contracts\GatewayWebHookContract;

class PaymentService
{
    private const RESOURCE_CLIENT = 'client';
    private const RESOURCE_PAYMENT_BOOK = 'payment_book';
    private const RESOURCE_CREDIT_CARD = 'credit_card';
    private const RESOURCE_INVOICE = 'invoice';
    private const RESOURCE_PIX = 'pix';
    private const RESOURCE_WEBHOOK = 'webhook';

    private $nameGateway;

    /**
     * Set gateway chosen to payment
     *
     * @param string $name
     *
     * @return PaymentService
     */
    public function gateway(string $name): PaymentService
    {
        $this->nameGateway = $name;
        return $this;
    }

    /**
     * Get client instance of gateway
     *
     * @return GatewayClientContract
     */
    public function client(): GatewayClientContract
    {
        return $this->resolve(self::RESOURCE_CLIENT);
    }

    /**
     * Get paymentBook instance of gateway
     *
     * @return GatewayTransactionContract
     */
    public function paymentBookTransaction(): GatewayTransactionContract
    {
        return $this->resolve(self::RESOURCE_PAYMENT_BOOK);
    }

    /**
     * Get creditCard instance of gateway
     *
     * @return GatewayTransactionContract
     */
    public function creditCardTransaction(): GatewayTransactionContract
    {
        return $this->resolve(self::RESOURCE_CREDIT_CARD);
    }

    /**
     * Get invoice instance of gateway
     *
     * @return GatewayTransactionContract
     */
    public function invoiceTransaction(): GatewayTransactionContract
    {
        //invoice uses the same as the payment book
        return $this->resolve(self::RESOURCE_INVOICE);
    }

    /**
     * Get pix instance of gateway
     *
     * @return GatewayTransactionContract
     */
    public function PixTransaction(): GatewayTransactionContract
    {
        return $this->resolve(self::RESOURCE_PIX);
    }

    /**
     * Get webhook instance of gateway
     *
     * @return GatewayWebHookContract
     */
    public function webHook(): GatewayWebHookContract
    {
        return $this->resolve(self::RESOURCE_WEBHOOK);
    }

    /**
     * Resolve instance based on gateway and resource
     *
     * @param string $type
     *
     * @return mixed
     */
    private function resolve(string $type)
    {
        $classSumary = $this->listGateways();

        if (! isset($classSumary[$this->nameGateway])) {
            throw new GatewayNotFoundException('Payment gateway not found, choose a new gateway.');
        }

        if (! isset($classSumary[$this->nameGateway][$type])) {
            throw new PaymentInstanceNotFoundException('Payment instance not found in the gateway, choose another.');
        }

        return $classSumary[$this->nameGateway][$type];
    }

    /**
     * Summary of gateway instances and resources
     *
     * @return array
     */
    private function listGateways(): array
    {
        return [
            Order::GATEWAY_CONTA_PAY => [
                self::RESOURCE_CLIENT => (new \App\Services\Payment\ContaPay\Client),
                self::RESOURCE_PAYMENT_BOOK => (new \App\Services\Payment\ContaPay\PaymentBookTransaction),
                self::RESOURCE_INVOICE => (new \App\Services\Payment\ContaPay\InvoiceTransaction),
                self::RESOURCE_PIX => (new \App\Services\Payment\ContaPay\PixTransaction),
                self::RESOURCE_WEBHOOK => (new \App\Services\Payment\ContaPay\WebHook),
            ],
            Order::GATEWAY_PAGAR_ME => [
                self::RESOURCE_CLIENT => (new \App\Services\Payment\PagarMe\Client),
                self::RESOURCE_CREDIT_CARD => (new \App\Services\Payment\PagarMe\CreditCardTransaction),
                self::RESOURCE_WEBHOOK => (new \App\Services\Payment\PagarMe\WebHook),
            ],
        ];
    }
}
