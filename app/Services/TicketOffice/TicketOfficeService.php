<?php

namespace App\Services\TicketOffice;

use App\Models\Event;
use App\Repositories\TicketOffice\Contracts\TicketOfficeRepositoryContract;

class TicketOfficeService
{
    /**
     * @var TicketOfficeRepositoryContract
     */
    private $ticketOfficeRepository;

    function __construct(TicketOfficeRepositoryContract $ticketOfficeRepository)
    {
        $this->ticketOfficeRepository = $ticketOfficeRepository;
    }

    /**
     * Get event, organizer and ticket information by event id
     *
     * @param string $slug
     *
     * @return Event
     */
    public function getInfoEvent(string $slug): Event
    {
        return $this->ticketOfficeRepository->getEventWithTickets($slug);
    }
}
