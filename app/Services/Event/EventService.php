<?php

namespace App\Services\Event;

use App\Models\Event;
use App\Models\EventAddress;
use Illuminate\Support\Collection;
use App\Repositories\Event\Contracts\EventRepositoryContract;
use App\Repositories\Ticket\Contracts\TicketRepositoryContract;

class EventService
{
    /**
     * @var EventRepositoryContract
     */
    private $eventRepository;

    /**
     * @var TicketRepositoryContract
     */
    private $ticketRepository;

    function __construct(
        TicketRepositoryContract $ticketRepository,
        EventRepositoryContract $eventRepository
    ) {
        $this->ticketRepository = $ticketRepository;
        $this->eventRepository = $eventRepository;
    }

    /**
     * Search event model by id
     *
     * @param int $eventId
     *
     * @return Event
     */
    public function getEvent(int $eventId): Event
    {
        $event = $this->eventRepository->get($eventId, 'id');
        if (is_null($event)) {
            abort(404);
        }

        return $event;
    }

    /**
     * Search event list by company
     *
     * @param int $companyId
     *
     * @return Collection
     */
    public function getEventsByCompany(int $companyId): Collection
    {
        $events = $this->eventRepository->getCollection($companyId, 'company_id');
        return $events;
    }

    /**
     * Searchs a list of event addresses by company
     *
     * @param int $companyId
     *
     * @return Collection
     */
    public function getEventAddressListByCompany(int $companyId): Collection
    {
        return $this->eventRepository->getEventAddressCollection($companyId, 'company_id');
    }

    /**
     * Get the list of event subjects
     *
     * @return array
     */
    public function getEventSubjectList(): array
    {
        return $this->eventRepository->getEventSubjectList();
    }

    /**
     * Get the list of event categories
     *
     * @return array
     */
    public function getEventCategoryList(): array
    {
        return $this->eventRepository->getEventCategoryList();
    }

    /**
     * Searchs list of tickets types belonging to the event
     *
     * @param Event $event
     *
     * @return Collection
     */
    public function getEventTicketTypes(Event $event): Collection
    {
        return $event->tickets->pluck('description');
    }

    /**
     * Returns event with total and sold tickets
     *
     * @param Event $event
     *
     * @return Event
     */
    public function getEventSoldTickets(Event $event): Event
    {
        $soldTickets = $this->getTicketsWithQuantityByEvent($event);

        $event->total_tickets = 0;
        $event->sold_tickets = 0;
        $event->sold_value = 0.0;

        $soldTickets->each(function ($ticket) use (&$event) {
            $event->total_tickets += $ticket->quantity;
            $event->sold_tickets += $ticket->sold;
            $event->sold_value += $ticket->sold * $ticket->price;
        });

        return $event;
    }

    /**
     * Returns a collection of tickets of the event
     *
     * @param Event $event
     *
     * @return Collection
     */
    public function getTicketsWithQuantityByEvent(Event $event): Collection 
    {
        $ticketsIds = $event->tickets->pluck('id')->toArray();
        $tickets = $this->ticketRepository->getByIdsWithQuantityAvailable($ticketsIds);

        return $tickets;
    }

    /**
     * Get by date sold tickets that belongs to $eventId
     *
     * @param Event $event
     *
     * @return Collection
     */
    public function getEventSoldTicketsPerDate(Event $event): Collection
    {
        $ticketsIds = $event->tickets->pluck('id')->toArray();
        return $this->ticketRepository->getByIdsSoldTicketsPerDate($ticketsIds)->values();
    }

    /**
     * Search list of events total and sold tickets
     *
     * @param Collection $events
     *
     * @return Collection
     */
    public function getEventsSoldTickets(Collection $events): Collection
    {
        $events->each(function ($event) {
            $event = $this->getEventSoldTickets($event);
        });

        return $events;
    }

    /**
     * Searchs list of events belonging to the list of companies
     *
     * @param Collection $companies
     *
     * @return Collection
     */
    public function getEventsByCompanies(Collection $companies): Collection
    {
        $events = collect([]);
        $companies->each(function ($company) use (&$events) {
            $companyEvents = $this->getEventsByCompany($company->id);
            $events = $events->merge($companyEvents);
        });

        return $events;
    }

    /**
     * Searchs list of events belonging to the list of companies that matches the array filter
     *
     * @param array $filter
     * @param Collection $companies
     *
     * @return Collection
     */
    public function getFilteredEventsByCompanies(array $filter, Collection $companies): Collection
    {
        return $this->eventRepository->getFilteredEventsByCompanies($filter, $companies);
    }

    /**
     * Searchs list of sold tickets that matches the array filter belonging to the event
     *
     * @param array $filter
     * @param Event $event
     *
     * @return Collection
     */
    public function getFilteredEventSoldTicketsPerDate(array $filter, Event $event): Collection
    {
        $ticketsIds = $event->tickets->pluck('id')->toArray();
        return $this->ticketRepository->getFilteredEventSoldTicketsPerDate($filter, $ticketsIds)->values();
    }

    /**
     * create a new event address
     *
     * @param array $data
     *
     * @return EventAddress
     */
    public function storeEventAddress(array $data): EventAddress {
        return $this->eventRepository->storeEventAddress($data);
    }

    /**
     * create a new event
     *
     * @param array $data
     *
     * @return Event
     */
    public function storeEvent(array $data): Event {
        return $this->eventRepository->store($data);
    }

    /**
     * update the contents of the event passed by param
     *
     * @param array $data
     * @param int $eventId
     *
     * @return Event
     */
    public function updateEvent(array $data, int $eventId): Event {
        return $this->eventRepository->update($data, $eventId);
    }

    /**
     * delete the the event passed by param
     *
     * @param int $eventId
     *
     * @return bool
     */
    public function deleteEvent(int $eventId): bool {
        return $this->eventRepository->delete($eventId);
    }
}
