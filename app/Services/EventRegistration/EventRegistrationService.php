<?php

namespace App\Services\EventRegistration;

use App\Models\Order;
use App\Models\TicketPass;
use App\Models\TicketPassHolder;
use App\Models\User;
use App\Repositories\Consumer\Contracts\ConsumerRepositoryContract;
use App\Repositories\Event\Contracts\EventRepositoryContract;
use App\Repositories\Ticket\Contracts\TicketPassHolderRepositoryContract;
use App\Repositories\TicketPass\Contracts\TicketPassRepositoryContract;
use App\Repositories\User\Contracts\UserRepositoryContract;
use App\Services\Checkout\CheckoutService;
use App\Traits\Qrcode;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class EventRegistrationService
{
    use Qrcode;

    /**
     * @var TicketPassRepositoryContract
     */
    private $ticketPassRepository;

    /**
     * @var UserRepositoryContract
     */
    private $userRepository;

    /**
     * @var ConsumerRepositoryContract
     */
    private $consumerRepository;

     /**
     * @var EventRepositoryContract
     */
    private $eventRepository;

    /**
     * @var CheckoutService
     */
    private $checkoutService;

    /**
     * @var TicketPassHolderRepositoryContract
     */
    private $ticketPassHolderRepository;

    /**
     * @param TicketPassRepositoryContract $ticketPassRepository
     * @param UserRepositoryContract $userRepository
     * @param ConsumerRepositoryContract $consumerRepository
     * @param EventRepositoryContract $eventRepository
     * @param TicketPassHolderRepositoryContract $ticketPassHolderRepository
     * @param CheckoutService $checkoutService
     */
    function __construct(
        TicketPassRepositoryContract $ticketPassRepository,
        UserRepositoryContract $userRepository,
        ConsumerRepositoryContract $consumerRepository,
        EventRepositoryContract $eventRepository,
        TicketPassHolderRepositoryContract $ticketPassHolderRepository,
        CheckoutService $checkoutService
    ) {
        $this->ticketPassRepository = $ticketPassRepository;
        $this->userRepository = $userRepository;
        $this->consumerRepository = $consumerRepository;
        $this->eventRepository = $eventRepository;
        $this->ticketPassHolderRepository = $ticketPassHolderRepository;
        $this->checkoutService = $checkoutService;
    }

    /**
     * Store a group of new registrations that linked to sold tickets
     *
     * @param array $registrations
     * @param array $buyer
     * @param array $payment
     * @param string $slugEvent
     *
     * @return Collection
     */
    public function register(array $registrations, array $buyer, array $payment, string $slugEvent): Collection
    {
        $registrations = collect($registrations);
        $event = $this->eventRepository->get($slugEvent);
        $buyer = $this->getConsumerLogged() ?? $this->registrationTicketBuyer($buyer);
        $orderItems = $this->orderItems($registrations);
        $order = $this->checkoutService->register($buyer, $event, $orderItems, $payment);
        $ticketPasses = $this->registrationTicketPassHolders($registrations, $order);

        return collect(
            [
                'event' => $event,
                'ticketPasses' => $ticketPasses,
                'buyer' => $buyer,
                'order' => $order
            ]
        );
    }

    /**
     * Organizes the order items in cart format
     *
     * @param  Collection  $registrations
     *
     * @return Collection
     */
    private function orderItems(Collection $registrations): Collection
    {
        $orderItems = collect([]);
        $registrations->each(function($register) use (&$orderItems) {
            $idxItem = $orderItems->search(function($item) use ($register) {
                return $item['ticket_id'] == $register['ticket_id'];
            });

            if ($idxItem === false) {
                //add new item
                $orderItems->push([
                    'ticket_id' => $register['ticket_id'],
                    'quant' => 1
                ]);
                return;
            }

            //update item
            $orderItems->put($idxItem, [
                'ticket_id' => $orderItems->get($idxItem)['ticket_id'],
                'quant' => $orderItems->get($idxItem)['quant'] + 1
            ]);
        });

        return $orderItems;
    }

    /**
     * Register ticket holders
     *
     * @param Collection $registrations
     * @param Order $order
     *
     * @return Collection
     */
    private function registrationTicketPassHolders(
        Collection $registrations,
        Order $order
    ): Collection {
        $ticketPasses = collect([]);
        $registrations->each(function($register) use (&$ticketPasses, $order) {
            $ticketPass = $this->registrationHolderInEvent($order, $register['ticket_id']);
            $ticketPassHolder = $this->registrationHolder($register, $ticketPass);
            $ticketPass->qrcode = $this->generateQrCode($ticketPass->code);
            $ticketPass->setRelation('holder', $ticketPassHolder);
            $ticketPasses->push($ticketPass);
        });
        return $ticketPasses;
    }

    /**
     * Get the consumer logged
     *
     * @return User|null
     */
    private function getConsumerLogged(): ?User
    {
        if (! Auth::check()) {
            return null;
        }

        $user = Auth::user();
        return $user->capacityConsumer ? $user : null;
    }

    /**
     * Register a buyer tickets
     *
     * @param array $buyer
     *
     * @return User
     */
    private function registrationTicketBuyer(array $buyer): User
    {
        return $this->registrationUser($buyer);
    }

    /**
     * Register a holder tickets
     *
     * @param array $registration
     * @param TicketPass $ticketPass
     *
     * @return TicketPassHolder
     */
    private function registrationHolder(array $registration, TicketPass $ticketPass): TicketPassHolder
    {
        $data = [
            'ticket_pass_id' => $ticketPass->id,
            'name' => Arr::get($registration, 'name', ''),
            'email' => Arr::get($registration, 'email', ''),
            'phone' => Arr::get($registration, 'phone', '')
        ];
        return $this->ticketPassHolderRepository->store($data);
    }

    /**
     * Register or return a existing consumer
     *
     * @param array $register
     *
     * @return User
     */
    private function registrationUser(array $register): User
    {
        $user = $this->userRepository->getWithConsumer($register['cpf'], 'cpf');

        /**
         * if there is no user, create a new one
         */
        if (! $user) {
            return $this->createUser($register);
        }

        /**
         * if there is a user and it is of type consumer
         */
        if ($user->capacityConsumer) {

            /**
             * if the user does not yet have access to the system, it means that
             * he is a consumer who has only one ticket linked, so his information
             * can be updated.
             */
            if (! $user->has_access) {
                return $this->updateUser($user, $register);
            }

            /**
             * retrieve the user
             */
            return $user;
        }

        /**
         * if there is a user and it not is of type consumer, attach a new consumer a this user
         */
        return $this->attachConsumer($user);
    }

    /**
     * Register a holder tickets in event
     *
     * @param Order $order
     * @param int $ticketId
     *
     * @return TicketPass
     */
    private function registrationHolderInEvent(Order $order, int $ticketId): TicketPass
    {
        $data = [
            'ticket_id' => $ticketId,
            'order_id' => $order->id,
            'code' => $this->generateTicketCode(),
        ];
        return $this->ticketPassRepository->register($data);
    }



    /**
     * Generate a unique and random code for the ticket
     *
     * @param  int  $size
     *
     * @return string
     */
    public function generateTicketCode(int $size = 10): string
    {
        do {
            $code = Str::random($size);
        } while($this->ticketPassRepository->get($code, 'code'));
        return $code;
    }

    /**
     * Create a new user and consumer
     *
     * @param array $registration
     *
     * @return User
     */
    private function createUser(array $registration): User
    {
        $data = [
            'cpf' => Arr::get($registration, 'cpf', ''),
            'name' => Arr::get($registration, 'name', ''),
            'email' => Arr::get($registration, 'email', ''),
            'phone' => Arr::get($registration, 'phone', ''),
            'zip_code' => Arr::get($registration, 'zip_code', '39401-509'),
            'street' => Arr::get($registration, 'street', 'Rua Tupinambas'),
            'number' => Arr::get($registration, 'number', '13'),
            'complement' => Arr::get($registration, 'complement', 'Sala 902'),
            'district' => Arr::get($registration, 'district', 'Melo'),
            'city' => Arr::get($registration, 'city', 'Montes Claros'),
            'state' => Arr::get($registration, 'state', 'Minas Gerais'),
            'uf' => Arr::get($registration, 'uf', 'MG'),
            'password' => Hash::make(Str::random(30)),
            'zip_code' => Arr::get($registration, 'zip_code', '39401-509'),
            'street' => Arr::get($registration, 'street', 'Rua Tupinambas'),
            'number' => Arr::get($registration, 'number', '13'),
            'complement' => Arr::get($registration, 'complement', 'Sala 902'),
            'district' => Arr::get($registration, 'district', 'Melo'),
            'city' => Arr::get($registration, 'city', 'Montes Claros'),
            'state' => Arr::get($registration, 'state', 'Minas Gerais'),
            'uf' => Arr::get($registration, 'uf', 'MG')
        ];

        $user = $this->userRepository->store($data);
        $consumer = $this->consumerRepository->store([]);
        return $this->userRepository->attachConsumer($user, $consumer);
    }

    /**
     * Update the consumer and user info
     *
     * @param User $user
     * @param array $registration
     *
     * @return User
     */
    private function updateUser(User $user, array $registration): User
    {
        $dataUser = [
            'cpf' => Arr::get($registration, 'cpf', ''),
            'name' => Arr::get($registration, 'name', ''),
            'email' => Arr::get($registration, 'email', ''),
            'phone' => Arr::get($registration, 'phone', ''),
            'zip_code' => Arr::get($registration, 'zip_code', '39401-509'),
            'street' => Arr::get($registration, 'street', 'Rua Tupinambas'),
            'number' => Arr::get($registration, 'number', '13'),
            'complement' => Arr::get($registration, 'complement', 'Sala 902'),
            'district' => Arr::get($registration, 'district', 'Melo'),
            'city' => Arr::get($registration, 'city', 'Montes Claros'),
            'state' => Arr::get($registration, 'state', 'Minas Gerais'),
            'uf' => Arr::get($registration, 'uf', 'MG'),
            //'password' => Hash::make(Str::random(30)),
        ];

        $dataConsumer = [];
        $user = $this->userRepository->update($dataUser, null, $user);
        /** The consumer table has no data yet */
        // $consumer = $this->consumerRepository->update($user->capacityConsumer->capacity_id, $dataConsumer);
        return $user;
    }

    /**
     * Links a consumer the a user
     *
     * @param User $user
     *
     * @return User
     */
    private function attachConsumer(User $user): User
    {
        $consumer =  $this->consumerRepository->store([]);
        return $this->userRepository->attachConsumer($user, $consumer);
    }
}
