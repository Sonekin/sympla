<?php

namespace App\Services\Consumer;

use App\Models\Consumer;
use App\Models\User;
use App\Repositories\User\Contracts\UserRepositoryContract;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;

class ProfileService
{
    /**
     * @var UserRepositoryContract
     */
    private $userRepository;

    function __construct(
        UserRepositoryContract $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * Update logged user information
     *
     * @param array $data
     *
     * @return User
     */
    public function updateLoggedUser(array $data): User
    {
        $password = Arr::get($data, 'password', null);
        if(!empty($password)) {
            $data['password'] = Hash::make($password);
        } else {
            unset($data['password']);
        }

        return $this->userRepository->update($data, auth()->user()->id);
    }

    /**
     * Searchs list of orders belonging to the consumer
     *
     * @param Consumer $consumer
     *
     * @return Collection
     */
    public function getOrdersByConsumer(Consumer $consumer): Collection
    {
        return $this->userRepository->getOrdersByConsumer($consumer);
    }
}
