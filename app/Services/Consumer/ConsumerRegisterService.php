<?php

namespace App\Services\Consumer;

use App\Exceptions\UserAlreadyRegistrationIncompleteException;
use App\Models\User;
use App\Repositories\Consumer\Contracts\ConsumerRepositoryContract;
use App\Repositories\User\Contracts\UserRepositoryContract;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class ConsumerRegisterService
{
    /**
     * @var UserRepositoryContract
     */
    private $userRepository;

    /**
     * @var ConsumerRepositoryContract
     */
    private $consumerRepository;

    function __construct(
        UserRepositoryContract $userRepository,
        ConsumerRepositoryContract $consumerRepository
    ) {
        $this->userRepository = $userRepository;
        $this->consumerRepository = $consumerRepository;
    }

    /**
     * Store a new consumer
     *
     * @param array $data
     *
     * @return User
     */
    public function register(array $data): User
    {
        $user = $this->userRepository->getWithConsumer($data['cpf'], 'cpf');

        /**
         * if there is user, throw a exception
         */
        if ($user) {
            throw new UserAlreadyRegistrationIncompleteException('User already has an incomplete registration');
        }

        return $this->createUser($data);
    }

    /**
     * Create a new user and consumer
     *
     * @param array $userData
     *
     * @return User
     */
    private function createUser(array $userData): User
    {
        $data = [
            'cpf' => Arr::get($userData, 'cpf', ''),
            'name' => Arr::get($userData, 'name', ''),
            'email' => Arr::get($userData, 'email', ''),
            'phone' => Arr::get($userData, 'phone', ''),
            'zip_code' => Arr::get($userData, 'zip_code', '39401-509'),
            'street' => Arr::get($userData, 'street', 'Rua Tupinambas'),
            'number' => Arr::get($userData, 'number', '13'),
            'complement' => Arr::get($userData, 'complement', 'Sala 902'),
            'district' => Arr::get($userData, 'district', 'Melo'),
            'city' => Arr::get($userData, 'city', 'Montes Claros'),
            'state' => Arr::get($userData, 'state', 'Minas Gerais'),
            'uf' => Arr::get($userData, 'uf', 'MG'),
            'password' => Hash::make(Arr::get($userData, 'password', '')),
            'has_access' => true,
        ];

        $user = $this->userRepository->store($data);
        $consumer = $this->consumerRepository->store([]);
        return $this->userRepository->attachConsumer($user, $consumer);
    }
}
