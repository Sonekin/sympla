<?php

namespace App\Services\Checkout;

use App\Exceptions\TicketNotExistInEventException;
use App\Exceptions\TicketPaymentException;
use App\Exceptions\TicketSoldOutException;
use App\Models\ConsumerGateway;
use App\Models\Event;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\User;
use App\Repositories\Consumer\Contracts\ConsumerGatewayRepositoryContract;
use App\Repositories\Order\Contracts\OrderRepositoryContract;
use App\Repositories\Ticket\Contracts\TicketRepositoryContract;
use App\Repositories\Transaction\Contracts\TransactionRepositoryContract;
use App\Services\Payment\PaymentService;
use App\Traits\GatewayPayment;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class CheckoutService
{
    use GatewayPayment;

    /**
     * @var OrderRepositoryContract $orderRepository
     */
    private $orderRepository;

    /**
     * @var TransactionRepositoryContract $TransactionRepository
     */
    private $TransactionRepository;

    /**
     * @var TicketRepositoryContract
     */
    private $ticketRepository;

    /**
     * @var PaymentService
     */
    private $paymentService;

    /**
     * @var ConsumerGatewayRepositoryContract
     */
    private $consumerGatewayRepository;


    /**
     * @param  OrderRepositoryContract  $orderRepository
     * @param  TransactionRepositoryContract  $TransactionRepository
     * @param  TicketRepositoryContract  $ticketRepository
     * @param  PaymentService  $paymentService
     * @param  ConsumerGatewayRepositoryContract  $consumerGatewayRepository
     */
    public function __construct(
        OrderRepositoryContract $orderRepository,
        TransactionRepositoryContract $TransactionRepository,
        TicketRepositoryContract $ticketRepository,
        PaymentService $paymentService,
        ConsumerGatewayRepositoryContract $consumerGatewayRepository
    ) {
        $this->orderRepository = $orderRepository;
        $this->TransactionRepository = $TransactionRepository;
        $this->ticketRepository = $ticketRepository;
        $this->paymentService = $paymentService;
        $this->consumerGatewayRepository = $consumerGatewayRepository;
    }

    /**
     * Register a payment
     *
     * @param User $user
     * @param Event $event
     * @param Collection $orderItems
     * @param array $payment
     *
     * @return Order
     */
    public function register(User $user, Event $event, Collection $orderItems, array $payment): Order
    {
        $ticketsIds = $orderItems->pluck('ticket_id')->all();
        $tickets = $this->ticketRepository->getByIdsWithQuantityAvailable($ticketsIds);

        /**
         * if the ticket does not belong to the event, an exception is thrown
         */
        if (! $this->ticketsExistsInEvent($tickets, $event)) {
            throw new TicketNotExistInEventException('The tickets entered are invalid');
        }

        /**
         * if any tickets are sold out, an exception is thrown
         */
        if (! $this->haveTicketForSale($tickets, $orderItems)) {
            throw new TicketSoldOutException('Some of the tickets you are purchasing are not available');
        }

        $paymentMethod = Arr::get($payment, 'payment_method');
        $amountPayment = Arr::get($payment, 'amount');
        $numInstallments = Arr::get($payment, 'installments');

        /**
         * if the amount received does not match the value of the tickets, an exception is thrown
         */
        if (! $this->ticketsAmountIsCorrect($orderItems, $tickets, $amountPayment)) {
            throw new TicketPaymentException('Payment information is not valid');
        }

        $orderData = [
            'consumer_id' => $user->capacityConsumer->capacity_id,
            'amount' => $amountPayment,
            'payment_method' => $paymentMethod,
            'gateway' => $this->getGatewayByPaymentMethod($paymentMethod),
            'installments' => $numInstallments,
        ];

        $order = $this->createOrder($orderData);

        if ($order) {
            $registerTransaction = $this->registerTransactions($order, $user, $payment, $amountPayment, $paymentMethod);
            $order->setRelation('transactions', $registerTransaction['transactions']);

            if ($infoPix = Arr::get($registerTransaction, 'info_pix')) {
                $order->infoPix = $infoPix;
            }
        }

        /**
         * if ticket sold out, update this information in the table
         */
        $this->registerSoldOut($tickets, $orderItems);

        return $order;
    }

    /**
     * Store a new order
     *
     * @param array $data
     *
     * @return Order
     */
    private function createOrder(array $data): Order
    {
        return $this->orderRepository->store($data);
    }

    /**
     * Update order billingId
     *
     * @param string $billingId
     *
     * @return Order
     */
    private function addBillingIdOrder(Order $order, string $billingId): Order
    {
        return $this->orderRepository->updateByModel($order, [
            'gateway_billing_id' => $billingId
        ]);
    }

     /**
     * Checks if the tickets belong to the event
     *
     * @param Collection $tickets
     * @param Event $event
     *
     * @return bool
     */
    private function ticketsExistsInEvent(Collection $tickets, Event $event): bool
    {
        return (! $tickets->contains(function ($ticket) use ($event) {
            return $ticket->event_id != $event->id;
        }));
    }

    /**
     * checks if all tickets are available
     *
     * @param Collection $tickets
     * @param Collection $orderItems
     *
     * @return bool
     */
    private function haveTicketForSale(Collection $tickets, Collection $orderItems): bool
    {
        $haveTicket = true;
        $tickets->each(function($ticket) use ($orderItems, &$haveTicket) {
            $ticketsAvailable = $ticket->quantity - $ticket->sold;

            $idxItem = $orderItems->search(function($item) use ($ticket) {
                return $item['ticket_id'] == $ticket->id;
            });

            if ($ticketsAvailable < $orderItems->get($idxItem)['quant']) {
                $haveTicket = false;
                return false;
            }
        });
        return $haveTicket;
    }

    /**
     * checks if the order total received is correct
     *
     * @param Collection $orderItems
     * @param Collection $tickets
     * @param float $amountPayment
     *
     * @return bool
     */
    private function ticketsAmountIsCorrect(Collection $orderItems, Collection $tickets, float $amountPayment): bool
    {
        $realTicketValue = 0;
        $tickets->each(function($ticket) use ($orderItems, &$realTicketValue) {
            $idxItem = $orderItems->search(function($item) use ($ticket) {
                return $item['ticket_id'] == $ticket->id;
            });

            $realTicketValue += ($ticket->price + $ticket->fee) * $orderItems->get($idxItem)['quant'];
        });

        return $realTicketValue == $amountPayment;
    }

    /**
     * @param array $invoices
     *
     * @return float
     */
    private function amountInvoices(array $invoices): float
    {
        $amount = 0;
        foreach ($invoices as $invoice) {
            $amount += $invoice['value'];
        }
        return $amount;
    }

    /**
     * Build invoices for a payment
     *
     * @param float $amount
     * @param int $numInstallments
     *
     * @return array
     *
     * todo: later extract this logic for new methods or classes,
     *      since the installments can have several business rules,
     *      such as: limit the installments with the proximity of
     *      the event, avoid end-of-week dates, avoid that invoices
     *      are due too close to the event
     */
    private function buildInvoicesPayment(float $amount, int $numInstallments): array
    {
        $invoices = [];

        $valueInstallment = (float) number_format($amount / $numInstallments, 2);

        //The first installment has a different amount from the others, if necessary
        $invoices[] = [
            'value' => $amount - ($valueInstallment * ($numInstallments - 1)),
            'due_date' => Carbon::now()->addDays(3),
        ];

        //Other installments
        for($i = 2; $i <= $numInstallments; $i++) {
            array_push($invoices, [
                'value' => $valueInstallment,
                'due_date' => Carbon::now()->addDays(30 * $i),
            ]);
        }
        return $invoices;
    }

    /**
     * updates the ticket to sold out, if this happened
     *
     * @param Collection $tickets
     * @param Collection $orderItems
     *
     * @return void
     */
    private function registerSoldOut(Collection $tickets, Collection $orderItems): void
    {
        $tickets->each(function($ticket) use ($orderItems) {

            $idxItem = $orderItems->search(function($item) use ($ticket) {
                return $item['ticket_id'] == $ticket->id;
            });

            $ticketsAvailable = $ticket->quantity - $ticket->sold - $orderItems->get($idxItem)['quant'];

            if ($ticketsAvailable == 0) {
                $data = [
                    'sold_out' => true
                ];
                $this->ticketRepository->update($ticket->id, $data);
            }
        });
    }

    /**
     * Register payment transactions
     *
     * @param Order $order
     * @param User $user
     * @param array $payment
     * @param float $amount
     * @param string $paymentMethod
     *
     * @return array
     */
    private function registerTransactions(Order $order, User $user, array $payment, float $amount, string $paymentMethod): array
    {
        if ($paymentMethod === Order::PAYMENT_BOOK) {
            return $this->paymentByPaymentBook($order, $user, $payment, $amount, $paymentMethod);
        }

        if ($paymentMethod === Order::INVOICE) {
            return $this->paymentByInvoice($order, $user, $amount, $paymentMethod);
        }

        if ($paymentMethod === Order::CREDIT_CARD) {
            return $this->paymentByCreditCard($order, $user, $payment, $amount, $paymentMethod);
        }

        if ($paymentMethod === Order::PIX) {
            return $this->paymentByPix($order, $user, $paymentMethod);
        }
    }

    /**
     * Register payment book type payment
     *
     * @param Order $order
     * @param User $user
     * @param array $payment
     * @param float $amount
     * @param string $paymentMethod
     *
     * @return array
     */
    private function paymentByPaymentBook(Order $order, User $user, array $payment, float $amount, string $paymentMethod): array
    {
        $numInstallments = Arr::get($payment, 'installments');

        $invoices = $this->buildInvoicesPayment($amount, $numInstallments);

        $transactionGateway = $this->registerPaymentBookGateway(
            $order,
            $user,
            $invoices,
            $this->getGatewayByPaymentMethod($paymentMethod)
        );

        $billingId = Arr::get($transactionGateway, 'billing_id');

        //update order for add billingId
        if ($billingId) {
            $this->addBillingIdOrder($order, $billingId);
        }

        $invoicesGateway = Arr::get($transactionGateway, 'invoices');

        return [
            'transactions' => $this->createInvoicesTransactions($order, $invoicesGateway)
        ];
    }

    /**
     * Register credit card type payment
     *
     * @param Order $order
     * @param User $user
     * @param array $payment
     * @param float $amount
     * @param string $paymentMethod
     *
     * @return array
     */
    private function paymentByCreditCard(Order $order, User $user, array $payment, float $amount, string $paymentMethod): array
    {
        $numInstallments = Arr::get($payment, 'installments');

       //$invoices = $this->buildInvoicesPayment($amount, $numInstallments);

        $card = Arr::get($payment, 'card');

        $transactionGateway = $this->registerCreditCardPaymentGateway(
            $order,
            $user,
            $numInstallments,
            $card,
            $this->getGatewayByPaymentMethod($paymentMethod)
        );

        $billingId = Arr::get($transactionGateway, 'billing_id');

        //update order for add billingId
        if ($billingId) {
            $this->addBillingIdOrder($order, $billingId);
        }

        $invoicesGateway = Arr::get($transactionGateway, 'invoices');

        return [
            'transactions' => $this->createInvoicesTransactions($order, $invoicesGateway)
        ];
    }

    /**
     * Register invoice type payment
     *
     * @param Order $order
     * @param User $user
     * @param float $amount
     * @param string $paymentMethod
     *
     * @return array
     */
    private function paymentByInvoice(Order $order, User $user, float $amount, string $paymentMethod): array
    {
        $numInstallments = 1;

        $invoices = $this->buildInvoicesPayment($amount, $numInstallments);

        $transactionGateway = $this->registerInvoiceGateway(
            $order,
            $user,
            $invoices,
            $this->getGatewayByPaymentMethod($paymentMethod)
        );

        $billingId = Arr::get($transactionGateway, 'billing_id');

        //update order for add billingId
        if ($billingId) {
            $this->addBillingIdOrder($order, $billingId);
        }

        $invoicesGateway = Arr::get($transactionGateway, 'invoices');

        return [
            'transactions' => $this->createInvoicesTransactions($order, $invoicesGateway)
        ];
    }

    /**
     * Register pix type payment
     *
     * @param Order $order
     * @param User $user
     * @param string $paymentMethod
     *
     * @return array
     */
    private function paymentByPix(Order $order, User $user, string $paymentMethod): array
    {
        $dueDate = Carbon::now()->addDay();
        $invoicePixGateway = $this->registerPixGateway(
            $order,
            $user,
            $dueDate,
            $this->getGatewayByPaymentMethod($paymentMethod)
        );

        $data = [
            'due_date' => $dueDate,
            'value' => $order->amount
        ];

        return [
            'transactions' => $this->registerPixTransaction($order, $data, $invoicePixGateway),
            'info_pix' => $invoicePixGateway
        ];
    }

    /**
     * Register payment book installments at the gateway
     *
     * @param Order $order
     * @param User $user
     * @param array $invoices
     * @param string $gateway
     *
     * @return array
     */
    private function registerPaymentBookGateway(Order $order, User $user, array $invoices, string $gateway): array
    {
        $consumerGateway = $this->getConsumerGateway($user, $gateway);
        $transactionsGateway = $this->paymentService
            ->gateway($gateway)
            ->paymentBookTransaction()
            ->store([
                'order' => $order->toArray(),
                'invoices' => $invoices,
                'client_id_gateway' => $consumerGateway->consumer_gateway_id
            ]);

        return $transactionsGateway;
    }

    /**
     * Register payment credit card installments at the gateway
     *
     * @param Order $order
     * @param User $user
     * @param array $invoices
     * @param string $gateway
     *
     * @return array
     */
    private function registerCreditCardPaymentGateway(Order $order, User $user, int $numInstallments, array $card, string $gateway): array
    {
        $consumerGateway = $this->getConsumerGateway($user, $gateway);
        $transactionsGateway = $this->paymentService
            ->gateway($gateway)
            ->creditCardTransaction()
            ->store([
                'order' => $order->toArray(),
                'installments' => $numInstallments,
                'client_id_gateway' => $consumerGateway->consumer_gateway_id,
                'user' => $user->toArray(),
                'card' => $card,
            ]);

        return $transactionsGateway;
    }

    /**
     * Register invoice payment gateway
     *
     * @param Order $order
     * @param User $user
     * @param array $invoices
     * @param string $gateway
     *
     * @return array
     */
    private function registerInvoiceGateway(Order $order, User $user, array $invoices, string $gateway): array
    {
        $consumerGateway = $this->getConsumerGateway($user, $gateway);
        $transactionsGateway = $this->paymentService
            ->gateway($gateway)
            ->invoiceTransaction()
            ->store([
                'order' => $order->toArray(),
                'invoices' => $invoices,
                'client_id_gateway' => $consumerGateway->consumer_gateway_id
            ]);

        return $transactionsGateway;
    }

    /**
     * Register pix payments at the gateway
     *
     * @param Order $order
     * @param User $user
     * @param Carbon $dueDate
     * @param string $gateway
     *
     * @return array
     */
    private function registerPixGateway(Order $order, User $user, Carbon $dueDate, string $gateway): array
    {
        $consumerGateway = $this->getConsumerGateway($user, $gateway);
        $transactionsGateway = $this->paymentService
            ->gateway($gateway)
            ->PixTransaction()
            ->store([
                'order' => $order->toArray(),
                'due_date' => $dueDate->format('Y-m-d'),
                'client_id_gateway' => $consumerGateway->consumer_gateway_id
            ]);

        return $transactionsGateway;
    }

    /**
     * Get, if it exists, the client_id on the gateway,
     * otherwise create a new client on the gateway
     *
     * @param User $user
     * @param string $gateway
     *
     * @return ConsumerGateway
     */
    private function getConsumerGateway(User $user, string $gateway): ConsumerGateway
    {
        $consumerGateway = $this->consumerGatewayRepository->get($user->capacityConsumer->capacity_id, $gateway);
        if (! $consumerGateway) {
            $clientGateway = $this->paymentService
                ->gateway($gateway)
                ->client()
                ->store($user->toArray());
            $clientGatewayId = Arr::get($clientGateway, 'client_id');

            $consumerGateway = $this->createConsumerGateway($user, $clientGatewayId, $gateway);
        }

        return $consumerGateway;
    }

    /**
     * stores the gateway client_id for a consumer
     *
     * @param User $user
     * @param string $clientGatewayId
     * @param string $gateway
     *
     * @return ConsumerGateway
     */
    private function createConsumerGateway(User $user, string $clientGatewayId, string $gateway): ConsumerGateway
    {
        $data = [
            'consumer_id' => $user->capacityConsumer->capacity_id,
            'consumer_gateway_id' => $clientGatewayId,
            'gateway_identifier' => $gateway,
        ];
        return $this->consumerGatewayRepository->store($data);
    }

    /**
     * stores invoices for a order from consumer
     *
     * @param  Order  $order
     * @param  array  $invoices
     *
     * @return Collection
     */
    private function createInvoicesTransactions(Order $order, array $invoices): Collection
    {
        $transactions = collect([]);
        foreach ($invoices as $invoice) {
            $data = [
                'order_id' => $order->id,
                'value' => $invoice['value'],
                'due_date' => $invoice['due_date'],
                'gateway_transaction_id' => $invoice['invoice_id'],
                'status' => Transaction::CREATED,
            ];
            $transaction = $this->TransactionRepository->store($data);
            $transaction->pdf = Arr::get($invoice, 'pdf');
            $transaction->barcode = Arr::get($invoice, 'barcode');
            $transactions->push($transaction);
        }
        return $transactions;
    }

    /**
     * stores pix for a order from consumer
     *
     * @param Order $order
     * @param array $payment
     * @param array $invoice
     *
     * @return Collection
     */
    private function registerPixTransaction(Order $order, array $payment, array $invoice): Collection
    {
        $data = [
            'order_id' => $order->id,
            'value' => $payment['value'],
            'due_date' => $payment['due_date'],
            'gateway_transaction_id' => $invoice['transaction_id'],
            'status' => Transaction::PENDING,
        ];
        $transactions = Collection::make()->push($this->TransactionRepository->store($data));
        return $transactions;
    }
}
