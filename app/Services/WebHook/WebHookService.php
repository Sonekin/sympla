<?php

namespace App\Services\WebHook;

use App\Integrations\WhatsApp\MegaApiIntegration;
use App\Integrations\WhatsApp\PassaporteDigitalNotifications;
use App\Models\Transaction;
use App\Repositories\Transaction\Contracts\TransactionRepositoryContract;
use App\Services\Payment\PaymentService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class WebHookService
{
    /**
     * @var PaymentService
     */
    private $paymentService;

    /**
     * @var TransactionRepositoryContract
     */
    private $transactionRepository;

    /**
     * @param PaymentService $paymentService
     * @param TransactionRepositoryContract $transactionRepository
     */
    function __construct(
        PaymentService $paymentService,
        TransactionRepositoryContract $transactionRepository
    ) {
        $this->paymentService = $paymentService;
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * Update a transaction
     *
     * @param array $webHookData
     * @param string $gateway
     *
     * @return Transaction
     */
    public function updateTransaction(array $webHookData, string $gateway): Transaction
    {
        $transaction = $this->paymentService->gateway($gateway)->webHook()->serialize($webHookData);
        //\Log::info(json_encode($transaction));

        $transactionId = Arr::get($transaction, 'id');

        $data = [
            'payment_date' => Arr::get($transaction, 'payment_date'),
        ];

        $status = Arr::get($transaction, 'status');

        if (! empty($status)) {
            $data['status'] = $status;
        }

        return $this->transactionRepository->update($transactionId, $data);
    }
}
