<?php

namespace App\Services\Participant;

use Illuminate\Support\Collection;
use App\Repositories\Participant\Contracts\ParticipantRepositoryContract;

class ParticipantService
{
    /**
     * @var ParticipantRepositoryContract
     */
    private $participantRepository;

    function __construct(
        ParticipantRepositoryContract $participantRepository
    ) {
        $this->participantRepository = $participantRepository;
    }

    /**
     * Searchs all participants by event
     *
     * @param int $eventId
     *
     * @return Collection
     */
    public function getParticipantsByEvent(int $eventId): Collection
    {
        return $this->participantRepository->getParticipantsByEvent($eventId);
    }

    /**
     * Searchs filtered participants by event
     *
     * @param array $filter
     * @param int $eventId
     *
     * @return Collection
     */
    public function getFilteredParticipantsByEvent(array $filter, int $eventId): Collection
    {
        return $this->participantRepository->getFilteredParticipantsByEvent($filter, $eventId);
    }
}
