<?php

namespace App\Services\Checkin;
use App\Repositories\Event\Contracts\EventRepositoryContract;
use App\Repositories\TicketPass\Contracts\TicketPassRepositoryContract;
use App\Repositories\Checkin\Contracts\CheckinRepositoryContract;
use App\Repositories\Participant\Contracts\ParticipantRepositoryContract;
use App\Repositories\Ticket\Contracts\TicketRepositoryContract;
use Exception;



class CheckinService
{
    /**
     * @var CheckinRepositoryContract
     */
    private $checkinRepository;

    /**
     * @var EventRepositoryContract
     */
    private $eventRepository;

    /**
     * @var ParticipantRepositoryContract
     */
    private $participantRepository;

    /**
     * @var TicketPassRepositoryContract
     */
    private $ticketPassRepository;

    /**
     * @var TicketRepositoryContract
     */
    private $ticketRepository;

    function __construct(
        CheckinRepositoryContract $checkinRepository,
        TicketPassRepositoryContract $ticketPassRepository,
        EventRepositoryContract $eventRepository,
        ParticipantRepositoryContract $participantRepository,
        TicketRepositoryContract $ticketRepository
       ) {
        $this->ticketPassRepository = $ticketPassRepository;
        $this->eventRepository = $eventRepository;
        $this->checkinRepository = $checkinRepository;
        $this->participantRepository = $participantRepository;
        $this->ticketRepository = $ticketRepository;
    }

    /**
     * create a new event
     *
     * @param array $data
     *
     * @return void
     */
    public function storeCheckin(array $data): void {
        $event = $this->eventRepository->get(intval($data['event_id']), 'id');
        if(is_null($event)) throw new Exception('Event not found');
        $date = date('d-m-y h:i:s');
        $nowTimestamp = strtotime($date);
        $eventDateTimestamp = strtotime($event->date);
        // if ($nowTimestamp < $eventDateTimestamp) throw new Exception('Invalid checkin date');
        $ticketPass = $this->ticketPassRepository->get($data['code'], 'code');
        $ticket = $this->ticketRepository->get($ticketPass->ticket_id, 'id');
        if(is_null($ticket)) throw new Exception('Ticket pass not found');
        if(is_null($ticketPass)) throw new Exception('Ticket pass not found');
        if($ticket->event_id !== $event->id) throw new Exception('Ticket pass not found');
        if($ticketPass->code !== $data['code']) throw new Exception('Invalid code');
        $checkin = $this->checkinRepository->get($ticketPass->id);
        $exists = !is_null($checkin);
        if($exists) throw new Exception('Ticket pass already read');
        $data['ticket_pass_id'] = $ticketPass->id;
        $this->checkinRepository->store($data);
    }

    public function report(array $data): mixed {
        $event = $this->participantRepository->getParticipantsReportByEvent(intval($data['event_id']));
        return $event;
    }
}
