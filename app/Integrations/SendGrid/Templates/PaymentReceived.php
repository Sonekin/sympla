<?php

namespace App\Integrations\SendGrid\Templates;

use App\Integrations\SendGrid\Enum\PaymentConfirmedGateway;
use App\Integrations\SendGrid\Interfaces\IEventVariables;
use App\Integrations\SendGrid\Interfaces\IPaymentVariables;

class PaymentReceived extends DynamicTemplate implements IEventVariables, IPaymentVariables
{
    public function __construct( PaymentConfirmedGateway $gateway )
    {
        $templateId = match ($gateway) {
            PaymentConfirmedGateway::TICKET => getenv("PEDIDO_RECEBIDO_BOLETO_TEMPLATE_ID"),
            PaymentConfirmedGateway::PIX => getenv("PEDIDO_RECEBIDO_PIX_TEMPLATE_ID"),
        };
        parent::__construct( $templateId );
    }

    public function setEventVariables(
        string $event_name,
        string $event_date,
        string $event_time,
        string $event_city,
        string $event_state,
        string $user_myaccount_url,
        string $event_banner_url
    ): void
    {
        $this->variables = array_merge( $this->variables, [
            'event_name'           => $event_name,
            'event_city'            => $event_city,
            'event_date'            => $event_date,
            'event_time'            => $event_time,
            'event_state'           => $event_state,
            'user_myaccount_url'    => $user_myaccount_url,
            'event_banner_url'      => $event_banner_url
        ] );
    }

    public function setPaymentVariables(
        string $order_value,
        string $expiration_date,
        string $barcode,
        string $image_pdf_url
    ): void
    {
        $this->variables = array_merge( $this->variables, [
            'order_value'       => $order_value,
            'expiration_date'   => $expiration_date,
            'barcode'           => $barcode,
            'image_pdf_url'     => $image_pdf_url
        ] );
    }
}
