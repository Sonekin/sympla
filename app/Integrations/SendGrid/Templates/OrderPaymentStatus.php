<?php

namespace App\Integrations\SendGrid\Templates;

use App\Integrations\SendGrid\Enum\OrderPaymentStatuses;
use App\Integrations\SendGrid\Interfaces\IEventVariables;

class OrderPaymentStatus extends DynamicTemplate implements IEventVariables
{
    public function __construct( OrderPaymentStatuses $status )
    {
        $templateId = match ($status) {
            OrderPaymentStatuses::PAYMENT_DECLINED => getenv("PAGAMENTO_RECUSADO_TEMPLATE_ID"),
            OrderPaymentStatuses::PAYMENT_CONFIRMED => getenv("PAGAMENTO_CONFIRMADO_TEMPLATE_ID"),
            OrderPaymentStatuses::ORDER_CANCELLED => getenv("PEDIDO_CANCELADO_TEMPLATE_ID"),
            OrderPaymentStatuses::CARD_HOLDING => getenv( "PEDIDO_RECEBIDO_CARTAO_TEMPLATE_ID" )
        };
        parent::__construct( $templateId );
    }

    /**
     * @param string $event_name Nome do evento
     * @param string $event_date Data do evento formatada como no exemplo: 01 de outubro
     * @param string $event_time Horário do evento (24h)
     * @param string $event_city Cidade do evento
     * @param string $event_state Estado do evento
     * @param string $user_myaccount_url Endereço URL da painel de usuário
     * @param string $event_banner_url URL da imagem do evento
     * @return void
     */
    public function setEventVariables(
        string $event_name,
        string $event_date,
        string $event_time,
        string $event_city,
        string $event_state,
        string $user_myaccount_url,
        string $event_banner_url
    ): void
    {
        $this->variables = array_merge( $this->variables, [
            'event_name'           => $event_name,
            'event_city'            => $event_city,
            'event_date'            => $event_date,
            'event_time'            => $event_time,
            'event_state'           => $event_state,
            'user_myaccount_url'    => $user_myaccount_url,
            'event_banner_url'      => $event_banner_url
        ] );
    }
}
