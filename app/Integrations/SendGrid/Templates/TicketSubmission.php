<?php

namespace App\Integrations\SendGrid\Templates;

class TicketSubmission extends DynamicTemplate
{
    /**
     * @param string $name
     * @param string $ticket_url
     */
    public function __construct(string $name, string $ticket_url )
    {
        parent::__construct( getenv("ENVIO_DO_INGRESSO_TEMPLATE_ID") );
        $this->variables = [
            'name'          => $name,
            'ticket_url'    => $ticket_url
        ];
    }
}
