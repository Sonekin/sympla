<?php

namespace App\Integrations\SendGrid\Templates;

use App\Integrations\SendGrid\Interfaces\IDynamicTemplate;

class DynamicTemplate implements IDynamicTemplate
{
    protected string $id;
    protected array $variables;

    public function __construct( $id )
    {
        $this->id           = $id;
        $this->variables    = [];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getVariables(): array
    {
        return $this->variables;
    }
}
