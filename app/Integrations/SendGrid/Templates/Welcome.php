<?php

namespace App\Integrations\SendGrid\Templates;

class Welcome extends DynamicTemplate
{
    public function __construct()
    {
        parent::__construct( getenv( "BOAS_VINDAS_TEMPLATE_ID" ) );
    }
}
