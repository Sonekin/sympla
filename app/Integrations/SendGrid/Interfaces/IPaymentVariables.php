<?php

namespace App\Integrations\SendGrid\Interfaces;

interface IPaymentVariables
{
    public function setPaymentVariables(
        string $order_value,
        string $expiration_date,
        string $barcode,
        string $image_pdf_url
    ): void;
}
