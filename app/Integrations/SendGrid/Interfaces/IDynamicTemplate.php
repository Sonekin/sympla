<?php

namespace App\Integrations\SendGrid\Interfaces;

interface IDynamicTemplate
{
    public function getId();

    public function getVariables();
}
