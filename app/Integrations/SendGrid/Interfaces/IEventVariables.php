<?php

namespace App\Integrations\SendGrid\Interfaces;

interface IEventVariables
{
    public function setEventVariables(
        string $event_name,
        string $event_city,
        string $event_date,
        string $event_time,
        string $event_state,
        string $user_myaccount_url,
        string $event_banner_url
    ): void;
}
