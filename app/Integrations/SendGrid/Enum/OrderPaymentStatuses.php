<?php

namespace App\Integrations\SendGrid\Enum;

enum OrderPaymentStatuses
{
    case ORDER_CANCELLED;
    case PAYMENT_DECLINED;
    case PAYMENT_CONFIRMED;
    case CARD_HOLDING;
}
