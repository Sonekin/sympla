<?php

namespace App\Integrations\SendGrid\Enum;

enum PaymentConfirmedGateway
{
    case PIX;
    case TICKET;
}
