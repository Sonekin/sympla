<?php

namespace App\Integrations\SendGrid;

use App\Integrations\SendGrid\Templates\DynamicTemplate;
use Exception;
use SendGrid;
use SendGrid\Mail\Mail;
use SendGrid\Mail\TypeException;

class SendGridIntegration
{

    private string $subject;
    private string $mailTo;
    private string $nameTo;
    private DynamicTemplate $template;

    /**
     * @param string $subject Assunto do e-mail
     * @param string $mailTo E-mail do destinatário
     * @param string $nameTo Nome do destinatário
     * @param DynamicTemplate $template Template dinâmico SendGrid e ser enviado por e-mail.
     */
    public function __construct(string $subject, string $mailTo, string $nameTo, DynamicTemplate $template )
    {
        $this->subject      = $subject;
        $this->mailTo       = $mailTo;
        $this->nameTo       = $nameTo;
        $this->template     = $template;
    }

    /**
     * @throws TypeException
     */
    public function sendMail(): void
    {
        $email = new Mail();
        $email->setFrom(getenv( "SENDGRID_AUTH_EMAIL" ), "Passaporte Digital");
        $email->setSubject( $this->subject );
        $email->addTo( $this->mailTo, $this->nameTo );
        $email->setTemplateId( $this->template->getId() );
        $email->addDynamicTemplateDatas( $this->template->getVariables() );
        $sendgrid = new SendGrid(getenv('SENDGRID_API_KEY'));
        try {
            $response = $sendgrid->send($email);
            print $response->statusCode() . "\n";
            print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
    }
}
