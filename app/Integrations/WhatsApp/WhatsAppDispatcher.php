<?php

namespace App\Integrations\WhatsApp;

class WhatsAppDispatcher
{
    public static function dispatch( $webhookData ): void
    {
        // send whatsapp payment message
        $pdnClient = new PassaporteDigitalNotifications();
        $pdnClient->sendMessage( $webhookData );
        // send whatsapp ticket message
        sleep(5);
        $maiClient = new MegaApiIntegration( $webhookData );
        $maiClient->sendMessage();
    }
}
