<?php

namespace App\Integrations\WhatsApp;

use App\Models\Order;
use App\Models\Ticket;
use App\Models\TicketPass;
use App\Models\Transaction;
use App\Models\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class MegaApiIntegration
{
    const MEGA_API_TOKEN = "MPdmsOoEkkovqZZIzT1sRIgGmj";
    private Client $client;
    private array $data;

    public function __construct( $webhookData )
    {
        $this->client = new Client([
            'base_uri' => "https://api2.megaapi.com.br/rest/sendMessage/megaapi-" . self::MEGA_API_TOKEN . "/text",
        ]);
        $this->data = $webhookData;
    }

    public function sendMessage(): void
    {
        if ($this->isPaid()) {
            try {
                $params = json_encode([
                    'messageData'   => [
                        'to'        => $this->getConsumerPhone() . "@s.whatsapp.net",
                        'text'      => $this->getTemplate()
                    ]
                ]);
                $response = $this->client->request('POST', '', [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . self::MEGA_API_TOKEN,
                    ],
                    'body' => $params
                ]);
                $content = $response->getBody()->getContents();
                Log::channel("whatsapp")->info( "Retorno Envio Ingresso: " . $content );
            } catch (GuzzleException $e) {
                Log::channel("whatsapp")->info("Erro Envio Ingresso: " . $e->getTraceAsString());
            } catch (\Exception $e) {
                Log::channel("whatsapp")->info("Erro Prepraro Envio Ingresso: " . $e->getMessage());
            }
        }
    }

    private function isPaid(): bool
    {
        $order          = $this->getOrder();
        $transactions   = Transaction::where( 'order_id', $order->id )->get();
        foreach ($transactions as $transaction) {
            if ($transaction->status === 'created') {
                return false;
            }
        }
        return true;
    }

    private function getTicketCode(): string
    {
        $order = $this->getOrder();
        $ticket = TicketPass::where('order_id', $order->id)->first();
        return $ticket->code;
    }

    private function getOrder()
    {
        $invoice        = Arr::get($this->data, 'data.invoices.0');
        $billingId      = Arr::get($invoice, 'billing_id');
        return Order::where('gateway_billing_id', $billingId)->first();
    }

    private function getUser()
    {
        $order          = $this->getOrder();
        $consumer       = $order->consumer;
        return User::find($consumer->userCapacity->user_id);
    }

    private function getConsumerPhone(): string
    {
        $user   = $this->getUser();
        return "55" . preg_replace("/\D+/", "", $user->phone);
    }

    private function getConsumerName(): string
    {
        $user   = $this->getUser();
        return $user->name;
    }

    private function getEventName(): string
    {
        $order      = $this->getOrder();
        $ticketPass = TicketPass::where('order_id', $order->id)->first();
        $ticket     = Ticket::where('id', $ticketPass->ticket_id )->first();
        return $ticket->event->name;
    }

    private function getTemplate(): string
    {
        return "Olá, *" . $this->getConsumerName() . "*" . PHP_EOL . PHP_EOL .
        "Obrigada por comprar com a Passaporte Digital 💙" . PHP_EOL . PHP_EOL .
        "Recebemos a confirmação do pagamento referente ao ingresso do evento: *" . $this->getEventName() . "*" . PHP_EOL . PHP_EOL .
        "Segue abaixo o link com o seu ingresso. Apresente o QR Code na portaria do evento e divirta-se !" . PHP_EOL . PHP_EOL .
        "Ingresso: https://app.passaportedigitalplus.com.br/ticket/" . $this->getTicketCode() . PHP_EOL . PHP_EOL .
        "Ah, pra saber sobre a sua compra a qualquer hora, acesse:" . PHP_EOL .
        "https://www.passaportedigitalplus.com.br/meuingresso";
    }
}
