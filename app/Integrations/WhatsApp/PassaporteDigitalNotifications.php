<?php

namespace App\Integrations\WhatsApp;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class PassaporteDigitalNotifications
{

    private Client $client;

    public function __construct()
    {
        $this->client       = new Client([
            'base_uri'      => "https://passaportedigitalnotifications.cloudns.ph/",
        ]);
    }

    public function sendMessage( $webhookData ): void
    {
        try {
            $params = json_encode( [
                'data'      => Arr::get( $webhookData, 'data' )
            ] );
            Log::channel("whatsapp")->info( "Parâmetros para envio: " . $params );
            $response = $this->client->request('POST', 'webhook', [
                'headers' => [
                    'Accept'            => 'application/json',
                    'Content-Type'      => 'application/json',
                ],
                'body'  => $params
            ]);
            $content = $response->getBody()->getContents();
            Log::channel("whatsapp")->info( "Retorno da API: " . $content );
        } catch (GuzzleException $e) {
            Log::channel("whatsapp")->info( "Erro Retorno da API: " . $e->getMessage() );
        }
    }
}
