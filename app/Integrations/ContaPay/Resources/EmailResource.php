<?php

namespace App\Integrations\ContaPay\Resources;

use App\Integrations\ContaPay\ContaPayIntegration;

class EmailResource
{
    private ContaPayIntegration $integration;

    /**
     * EmailResource constructor.
     */
    public function __construct(ContaPayIntegration $integration)
    {
        $this->integration = $integration;
    }

    /**
     * Resend a specific ticket to the Client.
     */
    public function sendInvoice(string $invoiceId): array
    {
        return $this->integration->http
            ->put("/emails/{$invoiceId}")
            ->throw()
            ->json();
    }
}
