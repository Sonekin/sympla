<?php

namespace App\Integrations\ContaPay\Resources;

use App\Integrations\ContaPay\ContaPayIntegration;

class PixResource
{
    private ContaPayIntegration $integration;

    /**
     * PixResource constructor.
     */
    public function __construct(ContaPayIntegration $integration)
    {
        $this->integration = $integration;
    }

    /**
     * Create new Billing.
     */
    public function create(array $data): array
    {
        return $this->integration->http
            ->post('/pix/cobranca', $data)
            ->throw()
            ->json();
    }
}
