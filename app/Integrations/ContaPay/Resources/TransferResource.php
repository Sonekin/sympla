<?php

namespace App\Integrations\ContaPay\Resources;

use App\Integrations\ContaPay\ContaPayIntegration;

class TransferResource
{
    private ContaPayIntegration $integration;

    /**
     * TransferResource constructor.
     */
    public function __construct(ContaPayIntegration $integration)
    {
        $this->integration = $integration;
    }

    /**
     * Withdrawal Request.
     */
    public function withdraw(array $data): array
    {
        return $this->integration->http
            ->post('/transfer', $data)
            ->throw()
            ->json();
    }
}
