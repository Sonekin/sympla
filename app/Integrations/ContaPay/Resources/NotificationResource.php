<?php

namespace App\Integrations\ContaPay\Resources;

use App\Integrations\ContaPay\ContaPayIntegration;

class NotificationResource
{
    private ContaPayIntegration $integration;

    /**
     * NotificationResource constructor.
     */
    public function __construct(ContaPayIntegration $integration)
    {
        $this->integration = $integration;
    }

    /**
     * Get invoice notifications by ID.
     */
    public function getAll(string $invoiceId): array
    {
        return $this->integration->http
            ->get("/notifications/{$invoiceId}")
            ->throw()
            ->json();
    }
}
