<?php

namespace App\Integrations\ContaPay\Resources;

use App\Integrations\ContaPay\ContaPayIntegration;

class ClientResource
{
    private ContaPayIntegration $integration;

    /**
     * ClientResource constructor.
     */
    public function __construct(ContaPayIntegration $integration)
    {
        $this->integration = $integration;
    }

    /**
     * Get All Clients.
     */
    public function getAll(): array
    {
        return $this->integration->http
            ->get('/clients')
            ->throw()
            ->json();
    }

    /**
     * Get the Client by ID.
     */
    public function getById(string $clientId): array
    {
        return $this->integration->http
            ->get("/clients/{$clientId}")
            ->throw()
            ->json();
    }

    /**
     * Create new Client.
     */
    public function create(array $data): array
    {
        return $this->integration->http
            ->post('/clients', $data)
            ->throw()
            ->json();
    }

    /**
     * Update the Client by ID.
     */
    public function update(string $clientId, array $data): array
    {
        return $this->integration->http
            ->put("/clients/{$clientId}", $data)
            ->throw()
            ->json();
    }

    /**
     * Delete the Client by ID.
     */
    public function delete(string $clientId): array
    {
        return $this->integration->http
            ->delete("/clients/{$clientId}")
            ->throw()
            ->json();
    }
}
