<?php

namespace App\Integrations\ContaPay\Resources;

use App\Integrations\ContaPay\ContaPayIntegration;
use Carbon\Carbon;

class ExtractResource
{
    private ContaPayIntegration $integration;

    /**
     * ExtractResource constructor.
     */
    public function __construct(ContaPayIntegration $integration)
    {
        $this->integration = $integration;
    }

    /**
     * Get the extract in a certain period.
     */
    public function getAll(Carbon $startDate, Carbon $endDate): array
    {
        return $this->integration->http
            ->get('/extract', [
                'data_inicial' => $startDate->format('Y-m-d'),
                'data_final'   => $endDate->format('Y-m-d'),
            ])
            ->throw()
            ->json();
    }

    /**
     * Get the balance.
     */
    public function balance(): array
    {
        return $this->integration->http
            ->get('/balance')
            ->throw()
            ->json();
    }
}
