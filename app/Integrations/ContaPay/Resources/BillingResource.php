<?php

namespace App\Integrations\ContaPay\Resources;

use App\Integrations\ContaPay\ContaPayIntegration;

class BillingResource
{
    private ContaPayIntegration $integration;

    /**
     * BillingResource constructor.
     */
    public function __construct(ContaPayIntegration $integration)
    {
        $this->integration = $integration;
    }

    /**
     * Get All Billings.
     */
    public function getAll(): array
    {
        return $this->integration->http
            ->get('/billings')
            ->throw()
            ->json();
    }

    /**
     * Get the Billing by ID.
     */
    public function getById(string $billingId): array
    {
        return $this->integration->http
            ->get("/billings/{$billingId}")
            ->throw()
            ->json();
    }

    /**
     * Create new Billing.
     */
    public function create(array $data): array
    {
        return $this->integration->http
            ->post('/billings', $data)
            ->throw()
            ->json();
    }

    /**
     * Delete the Billing by ID.
     */
    public function deleteInvoice(string $billingId): array
    {
        return $this->integration->http
            ->delete("/billings/{$billingId}")
            ->throw()
            ->json();
    }
}
