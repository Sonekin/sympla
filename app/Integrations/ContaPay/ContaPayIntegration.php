<?php

namespace App\Integrations\ContaPay;

use App\Integrations\ContaPay\Resources\BillingResource;
use App\Integrations\ContaPay\Resources\ClientResource;
use App\Integrations\ContaPay\Resources\EmailResource;
use App\Integrations\ContaPay\Resources\ExtractResource;
use App\Integrations\ContaPay\Resources\NotificationResource;
use App\Integrations\ContaPay\Resources\PixResource;
use App\Integrations\ContaPay\Resources\TransferResource;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;

class ContaPayIntegration
{
    private string $email;

    private string $token;

    private string $baseUrl;

    public PendingRequest $http;

    /**
     * ContaPayIntegration constructor.
     */
    public function __construct()
    {
        $accessToken = $this->generateAccessToken();

        $this->http = Http::acceptJson()->baseUrl("{$this->baseUrl}/v2")->withToken($accessToken);
    }

    /**
     * BillingResource
     */
    public function billings(): BillingResource
    {
        return new BillingResource($this);
    }

    /**
     * BillingResource
     */
    public function pix(): PixResource
    {
        return new PixResource($this);
    }

    /**
     * ClientResource
     */
    public function clients(): ClientResource
    {
        return new ClientResource($this);
    }

    /**
     * EmailResource
     */
    public function emails(): EmailResource
    {
        return new EmailResource($this);
    }

    /**
     * TransferResource
     */
    public function transfer(): TransferResource
    {
        return new TransferResource($this);
    }

    /**
     * ExtractResource
     */
    public function extract(): ExtractResource
    {
        return new ExtractResource($this);
    }

    /**
     * NotificationResource
     */
    public function notifications(): NotificationResource
    {
        return new NotificationResource($this);
    }

    /**
     * Generating the Access Token for Authentication.
     */
    private function generateAccessToken()
    {
        $this->email = config('services.contapay.email');

        $this->token = config('services.contapay.token');

        $this->baseUrl = config('services.contapay.base_url');

        return Http::post("{$this->baseUrl}/v2/authenticate", [
            'email' => $this->email,
            'token' => $this->token,
        ])->json('access_token');
    }
}
