<?php

namespace App\Integrations\PagarMe;

use PagarmeCoreApiLib\PagarmeCoreApiClient;

class PagarMeIntegration
{
    /**
     * @return Client
     */
    public static function init(): PagarmeCoreApiClient
    {
        $apiKey = config('services.pagarme.api_key');

        return new PagarmeCoreApiClient($apiKey);
    }
}
