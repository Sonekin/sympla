<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->smallInteger('availability')->default(1)->after('sold_out');
            $table->integer('min_purchase_quantity')->default(1)->after('availability');
            $table->integer('max_purchase_quantity')->nullable()->after('min_purchase_quantity');
            $table->text('detailed_description')->nullable('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn(
                ['availability', 'min_purchase_quantity', 'max_purchase_quantity', 'detailed_description']
            );
        });
    }
};
