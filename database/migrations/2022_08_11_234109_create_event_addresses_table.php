<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_addresses', function (Blueprint $table) {
            $table->id()->unsigned();
            $table->foreignId('company_id')->constrained('companies');

            $table->string('name', 255);
            $table->string('street', 255);
            $table->string('number', 45)->nullable();
            $table->string('complement', 255)->nullable();
            $table->string('district', 255);
            $table->string('postal_code', 255);
            $table->string('city', 255);
            $table->string('state', 45);

            $table->timestamps();
            $table->softDeletes();

            $table->unique(['company_id', 'name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_addresses');
    }
};
