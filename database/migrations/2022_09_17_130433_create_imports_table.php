<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imports', function (Blueprint $table) {
            $table->id();
            $table->string('nome', 255)->nullable(true);
            $table->string('cpf', 50)->nullable(true);
            $table->string('email', 255)->nullable(true);
            $table->string('telefone', 255)->nullable(true);
            $table->string('ingresso', 255)->nullable(true);
            $table->decimal('valor', 10,2)->nullable(true);
            $table->integer('status')->default(0)->nullable(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imports');
    }
};
