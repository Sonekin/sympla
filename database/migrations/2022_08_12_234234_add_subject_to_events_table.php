<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->foreignId('address_id')->unsigned()->after('company_id')->nullable()->constrained('event_addresses');

            $table->unsignedInteger('subject')->after('event_status')->nullable();
            $table->unsignedInteger('category')->after('subject')->nullable();
            $table->string('city', 255)->after('category')->nullable();
            $table->string('state', 45)->after('city')->nullable();

            $table->dateTime('end_date')->after('date')->nullable();

            $table->boolean('visible')->after('flyer_image')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropForeign(['address_id']);
            $table->dropColumn('address_id');

            $table->dropColumn('subject');
            $table->dropColumn('category');
            $table->dropColumn('city');
            $table->dropColumn('state');
            $table->dropColumn('end_date');
            $table->dropColumn('visible');
        });
    }
};
