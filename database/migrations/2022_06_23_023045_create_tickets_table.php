<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id()->unsigned();
            $table->foreignId('event_id')->constrained('events');
            $table->string('description', 255);
            $table->float('price', 8, 2);
            $table->float('fee', 8, 2);
            $table->unsignedInteger('quantity');
            $table->boolean('status')->default(0);
            $table->boolean('sold_out')->default(0);
            $table->dateTime('start_sales');
            $table->dateTime('end_sales');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
};
