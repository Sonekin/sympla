<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Event;
use App\Models\Organizer;
use App\Models\Ticket;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class EventSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $company = $this->getCompany(1);
        $this->createEvent($company);
    }

    /**
     * Create a new event
     *
     * @return Event
     */
    private function createEvent(Company $company): Event
    {
        return Event::create([
            'company_id' => $company->id,
            'name' => 'A festa do ano',
            'slug' => 'a-festa-do-ano',
            'description' => 'Teremos 50 atrações',
            'date' => Carbon::now()->addDays(20),
            'flyer_image' => 'uma imagem',
        ]);
    }

    private function getCompany(int $company_id)
    {
        return Company::find($company_id);
    }
}
