<?php

namespace Database\Seeders;

use App\Models\ConsumerGateway;
use App\Models\Order;
use Illuminate\Database\Seeder;

class ConsumerGatewaySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /**
         * just to avoid creating a gateway client
         */
        ConsumerGateway::create([
            'consumer_id' => '1',
            'consumer_gateway_id' => '4c8e39ebdbe1e08a0b9272b327cd26213ad5c856',
            'gateway_identifier' => Order::GATEWAY_CONTA_PAY,
        ]);

        /**
         * just to avoid creating a gateway client
         */
        ConsumerGateway::create([
            'consumer_id' => '1',
            'consumer_gateway_id' => 'cus_GoK5QpzI2YhJZ3XO',
            'gateway_identifier' => Order::GATEWAY_PAGAR_ME,
        ]);
    }
}
