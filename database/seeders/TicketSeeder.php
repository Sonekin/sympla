<?php

namespace Database\Seeders;

use App\Models\Event;
use App\Models\Ticket;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TicketSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $event = $this->getEvent(1);
        $this->createTicket($event);
    }

    private function getEvent(int $event_id)
    {
        return Event::find($event_id);
    }


    /**
     * Create a new ticket
     *
     * @return void
     */
    private function createTicket(Event $event): void
    {
        Ticket::insert([
            $this->ticketPromotionalExpirad($event),
            $this->ticketFirstBatchHalfPriceSoldOut($event),
            $this->ticketSecondBatchHalfPrice($event),
            $this->ticketFirstBatchValid($event),
            $this->ticketSegundBatchNotShow($event),
        ]);
    }

    private function ticketPromotionalExpirad(Event $event): array
    {
        $now = Carbon::now()->subDay(1);
        return [
            'event_id' => $event->id,
            'description' => 'Promocional',
            'price' => 15,
            'fee' => 1.5,
            'quantity' => 100,
            'sold_out' => false,
            'status' => true,
            'start_sales' => $now->copy()->subDays(15),
            'end_sales' => $now,
        ];
    }

    private function ticketFirstBatchHalfPriceSoldOut(Event $event): array
    {
        $now = Carbon::now()->subDays(1);
        return [
            'event_id' => $event->id,
            'description' => '1º lote | Meia entrada',
            'price' => 25,
            'fee' => 2.5,
            'quantity' => 100,
            'sold_out' => true,
            'status' => true,
            'start_sales' => $now,
            'end_sales' => $now->copy()->addDays(15),
        ];
    }

    private function ticketSecondBatchHalfPrice(Event $event): array
    {
        $now = Carbon::now()->subDays(1);
        return [
            'event_id' => $event->id,
            'description' => '2º lote | Meia entrada',
            'price' => 30,
            'fee' => 3,
            'quantity' => 100,
            'sold_out' => false,
            'status' => true,
            'start_sales' => $now,
            'end_sales' => $now->copy()->addDays(15),
        ];
    }

    private function ticketFirstBatchValid(Event $event): array
    {
        $now = Carbon::now()->subDays(1);
        return [
            'event_id' => $event->id,
            'description' => '1º lote | Inteira',
            'price' => 50,
            'fee' => 5,
            'quantity' => 100,
            'sold_out' => false,
            'status' => true,
            'start_sales' => $now,
            'end_sales' => $now->copy()->addDays(15),
        ];
    }

    private function ticketSegundBatchNotShow(Event $event): array
    {
        $now = Carbon::now()->addDays(5);
        return [
            'event_id' => $event->id,
            'description' => '2º lote | Inteira',
            'price' => 60,
            'fee' => 6,
            'quantity' => 100,
            'sold_out' => false,
            'status' => true,
            'start_sales' => $now,
            'end_sales' => $now->copy()->addDays(15),
        ];
    }
}
