<?php

namespace Database\Seeders;

use App\Models\Organizer;
use App\Models\User;
use App\Models\UserCapabilities;
use Illuminate\Database\Seeder;

class OrganizerSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $organizer = $this->createOrganizer();
        $user = $this->createUser();
        $userCapacity = $this->builderUserCapacity($user);
        $organizer->userCapacity()->save($userCapacity);
    }

    /**
     * Build a object for attach user and consumer
     *
     * @param User $user
     *
     * @return UserCapabilities
     */
    private function builderUserCapacity(User $user): UserCapabilities
    {
        return new UserCapabilities([
            'user_id' => $user->id
        ]);
    }

    /**
     * Create a new organizer
     *
     * @return Organizer
     */
    private function createOrganizer(): Organizer
    {
        return Organizer::create([]);
    }

    /**
     * create a new user
     *
     * @return User
     */
    private function createUser(): User
    {
        return User::create([
            'name' => 'Maria Pereira',
            'email' => 'mp@emai.com',
            'phone' => '(38) 95585-4485',
            'cpf' => '097.100.250-90',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password,
        ]);
    }
}
