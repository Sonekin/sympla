<?php

namespace Database\Seeders;

use App\Models\Consumer;
use App\Models\User;
use App\Models\UserCapabilities;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class ConsumerSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $consumer = $this->createConsumer();
        $user = $this->createUser();
        $userCapacity = $this->builderUserCapacity($user);
        $consumer->userCapacity()->save($userCapacity);
    }

    private function builderUserCapacity(User $user): UserCapabilities
    {
        return new UserCapabilities([
            'user_id' => $user->id
        ]);
    }

    private function createConsumer(): Consumer
    {
        return Consumer::create([]);
    }

    private function createUser(): User
    {
        return User::create([
            'name' => 'Consumer Teste',
            'email' => 'ct@email.com',
            'phone' => '(38) 95585-4521',
            'cpf' => '853.507.320-56',
            'password' => Hash::make('password'), // password,
            'has_access' => true,
            'zip_code' => '39400-001',
            'street' => 'Rua Dr Santos',
            'number' => '123',
            'complement' => '',
            'district' => 'Centro',
            'city' => 'Montes Claros',
            'state' => 'Minas Gerais',
            'uf' => 'MG',
        ]);
    }
}
