<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Organizer;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $company = Company::create([
            'name' => 'ABC events',
            'description' => 'Shows e festas comemorativas',
        ]);

        $this->addCollabolators($company);
    }

    private function addCollabolators(Company $company)
    {
        $organizers = [1];
        $company->organizers()->attach($organizers);
    }
}
