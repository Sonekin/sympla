# Passaporte digital

## Stack utilizada

- [Laravel 9.x](https://laravel.com/docs/9.x).
- [Vue 3](https://vuejs.org/).
- [Inertia](https://inertiajs.com/).

## Instalar o projeto

- Requisitos: Ter o [docker](https://www.docker.com/get-started/) instalado;
- Clonar o projeto: [Passaporte digital](https://bitbucket.org/bankfy/passaporte-digital/);
- Configurar arquivo .env
- Executar o script na raiz: ``` ./install.sh ```;
- Executar o comando ``` docker-compose exec laravel.test bash ``` para acessar o container;
- Executar o seguinte comando para instalar o banco de dados: ``` php artisan migrate:fresh --seed ```
