const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .vue({
      "extractStyles": true,
    })
    .override(config => {
      config.module.rules.find(rule => rule.test.test('.svg')).exclude = /\.svg$/;
      config.module.rules.push({
        test: /\.svg$/,
        use: [{loader:'html-loader'}]
      });
    })
    .postCss('resources/css/app.css', 'public/css', [require('tailwindcss'), require('autoprefixer')])
    .postCss('resources/css/tailwind.css', 'public/css')
    .sass('resources/sass/app.scss', 'public/css')
    .webpackConfig({
      module: {
        rules: [
          {
            test: /\.(postcss)$/,
            use: [
              'vue-style-loader',
              { loader: 'css-loader', options: { importLoaders: 1 } },
              'postcss-loader'
            ]
          }
        ],
      },
    })
    .alias({
        '@': 'resources/js',
    });

if (mix.inProduction()) {
    mix.version();
}
